#ifndef ADAPTERTRAITS_HH
#define ADAPTERTRAITS_HH

#include <array>
#include <type_traits>

#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/geometry/type.hh>

#include <dune/randomfield/randomfield.hh>

#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/finiteelementmap/pkqkfem.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/constraints/common/constraintsparameters.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include <dune/pdelab/localoperator/convectiondiffusionccfv.hh>
#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/pdelab/function/discretegridviewfunction.hh>

#include <dune/grfsampler/traits/switches.hh>
#include <dune/grfsampler/traits/gridtraits.hh>


namespace GRFS
{
namespace Traits
{
  template <typename RealNumber, int dimension = 2>
  struct CEGridTraits
  {
  private:

    using GT = GridTraits<Switches::GridLowerLeft::NULL_GLL, RealNumber>;


  public:

    enum {dim = dimension};

    using RangeField  = RealNumber;
    using Scalar      = Dune::FieldVector<RangeField, 1>;
    using DomainField = typename GT::DomainField;
    using Domain      = typename GT::DomainVector;
  };


  template<typename RangeField>
  struct CERandomFieldTraits
  {
    using Seed = unsigned int;

    using Configuration = Dune::ParameterTree;

    using CirculantEmbeddingGridTraits = CEGridTraits<RangeField>;

    using RandomField =
      Dune::RandomField::RandomField<CEGridTraits<RangeField> >;
  };




  template <class GridView, typename RealNumber,
            int dimension, Switches::Discretization d>
  struct PDELabTraits
  {
    static constexpr int dim = dimension;

    static_assert(d == Switches::Discretization::PQ1FEM or
                  d == Switches::Discretization::CIRCULANTEMBEDDING or
                  d == Switches::Discretization::CCFV);

    static constexpr bool isspdemethod =
      d != Switches::Discretization::CIRCULANTEMBEDDING;

    static constexpr Switches::Discretization discretization = d;

    static constexpr Dune::GeometryType gt{Traits::GridTraits<
        Traits::Switches::GridLowerLeft::NULL_GLL>::geometrytype };

    using RangeField = RealNumber;

    using Constraints = Dune::PDELab::ConformingDirichletConstraints;

    using VectorBackend = Dune::PDELab::ISTL::VectorBackend<>;

    // order = 1 for pq1fem and circulant embedding and order = 0 for ccfv
    static constexpr int order =
      []
      {
        if constexpr
          (discretization == Switches::Discretization::PQ1FEM or
           discretization == Switches::Discretization::CIRCULANTEMBEDDING)
          return 1;

        if constexpr (discretization == Switches::Discretization::CCFV)
          return 0;
      }();

    using FiniteVolumeMap =
      Dune::PDELab::P0LocalFiniteElementMap<
        RangeField, RangeField, dimension>;

    using PiecewiseLinearMap =
      Dune::PDELab::PkQkLocalFiniteElementMap<
        RangeField, RangeField, dimension, 1>;

    using FiniteElementMap = typename
      std::conditional<order == 1, PiecewiseLinearMap, FiniteVolumeMap>::type;

    using GridFunctionSpace =
      Dune::PDELab::GridFunctionSpace<
        GridView, FiniteElementMap, Constraints, VectorBackend>;

    using ConstraintsContainer =
      typename GridFunctionSpace::template
        ConstraintsContainer<RangeField>::Type;

    using CoefficientVector =
      Dune::PDELab::Backend::Vector<GridFunctionSpace, RangeField>;

    using GridViewFunction =
      Dune::PDELab::DiscreteGridViewFunction<
        GridFunctionSpace, CoefficientVector>;
    // ------------------------------------------------------------------------

    template <class GridFunction>
    using VTKAdapter = Dune::PDELab::VTKGridFunctionAdapter<GridFunction>;
  };



  template <class ModelProblem, class PDELabTraits>
  struct PDELabAssemblerTraits
  {
  private:

    using RF = typename PDELabTraits::RangeField;

    // we need this, because as of c++17, we cannot use a lambda-expression
    // in a template argument to deduce the type in std::conditional
    static constexpr bool usepwlinear{
      PDELabTraits::discretization == Switches::Discretization::PQ1FEM or
      PDELabTraits::discretization ==
        Switches::Discretization::CIRCULANTEMBEDDING };


  public:

    using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;

    using LocalOperatorFEM =
      Dune::PDELab::ConvectionDiffusionFEM<
        ModelProblem, typename PDELabTraits::FiniteElementMap>;

    using LocalOperatorCCFV =
      Dune::PDELab::ConvectionDiffusionCCFV<ModelProblem>;

    using LocalOperator = typename
      std::conditional<usepwlinear, LocalOperatorFEM, LocalOperatorCCFV>::type;

    using BoundaryConditionAdapter =
      Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<ModelProblem>;

    using DirichletAdapter =
      Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<
        ModelProblem>;

    using GridOperator =
      Dune::PDELab::GridOperator<typename PDELabTraits::GridFunctionSpace,
                                 typename PDELabTraits::GridFunctionSpace,
                                 LocalOperator, MatrixBackend, RF, RF, RF,
                                 typename PDELabTraits::ConstraintsContainer,
                                 typename PDELabTraits::ConstraintsContainer>;

    using JacobianMatrix = typename GridOperator::Jacobian;

    using NativeMatrix = Dune::PDELab::Backend::Native<JacobianMatrix>;
  };
} // namespace Traits
} // namespace GRFS

#endif

