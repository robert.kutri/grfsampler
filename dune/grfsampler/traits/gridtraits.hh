#ifndef GRIDTRAITS_HH
#define GRIDTRAITS_HH

#include <array>
#include <type_traits>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/common/capabilities.hh>

#include <dune/geometry/type.hh>

#include <dune/grfsampler/traits/switches.hh>


namespace GRFS
{
namespace Traits
{
  template <Switches::GridLowerLeft gp, typename DF = double>
  struct GridTraits
  {
    static constexpr unsigned int dimension{ 2 };

    using DomainField = DF;

    using DomainVector      = Dune::FieldVector<DomainField, dimension>;
    using CellLayout        = std::array<int, dimension>;
    using RectangularDomain = std::array<DomainVector, 2>;

    // The position of the lower left corner of the cuboid domain
    static_assert(gp == Switches::GridLowerLeft::DEFAULT or
                  gp == Switches::GridLowerLeft::GENERIC or
                  gp == Switches::GridLowerLeft::NULL_GLL);
    static constexpr Switches::GridLowerLeft gridposition = gp;

    using OriginCoordinates =
      Dune::EquidistantCoordinates<DomainField, dimension>;
    using OffsetCoordinates =
      Dune::EquidistantOffsetCoordinates<DomainField, dimension>;

    using OriginYaspG = Dune::YaspGrid<dimension, OriginCoordinates>;
    using OffsetYaspG = Dune::YaspGrid<dimension, OffsetCoordinates>;

    using Grid = typename
      std::conditional<gridposition == Switches::GridLowerLeft::DEFAULT,
                       OriginYaspG, OffsetYaspG>::type;

    using SingleGeometryType = Dune::Capabilities::hasSingleGeometryType<Grid>;

    // Mixed grids are not yet supported, so we assert whether the grid uses
    // a single geometry type.
    // FIXME: For support of mixed grids, the definitoin of the quadrature
    //        rules and the factorization of the matrix have to be done
    //        elementwise
    static_assert(SingleGeometryType::v,
                  "Mixed grids are not supported. Grid must only use a "
                  "single type of reference element.");

    // FIXME: Infer this from the Grid type
    static constexpr Dune::GeometryType
      geometrytype{ Dune::GeometryTypes::cube(dimension) };

    using MultiDomainGrid =
      Dune::MultiDomainGrid<Grid,
                            Dune::mdgrid::FewSubDomainsTraits<dimension, 1> >;
  };
} // namespace Traits
} // namespace GRFS

#endif

