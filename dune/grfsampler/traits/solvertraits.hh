#ifndef SOLVERTRAITS_HH
#define SOLVERTRAITS_HH

#include <dune/istl/umfpack.hh>
#include <dune/istl/ldl.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/paamg/amg.hh>
#include <dune/istl/paamg/parameters.hh>
#include <dune/istl/solvers.hh>

#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/stationary/linearproblem.hh>

namespace GRFS
{
namespace Traits
{
  template <class Matrix, class Vector>
  using LinearOperator = Dune::MatrixAdapter<Matrix, Vector, Vector>;

  template <class Matrix, class Vector>
  struct AMGTraits
  {
    using Smoothing = Dune::SeqILU<Matrix, Vector, Vector>;

    using SmoothingParameters =
      typename Dune::Amg::SmootherTraits<Smoothing>::Arguments;

    using CoarseningCriterion =
      Dune::Amg::CoarsenCriterion<Dune::Amg::SymmetricCriterion<Matrix,
                                  Dune::Amg::FirstDiagonal> >;

    using Preconditioner =
      Dune::Amg::AMG<LinearOperator<Matrix, Vector> , Vector, Smoothing>;

    using PreconditionerParameters = Dune::Amg::Parameters;

    using Solver = Dune::CGSolver<Vector>;

    using SolutionStatistics = Dune::InverseOperatorResult;
  };


namespace Preconditioner
{
  template <class Matrix, class Vector>
  using ILU0 = Dune::SeqILU<Matrix, Vector, Vector>;

  template <class Matrix, class Vector>
  using ILDL = Dune::SeqILDL<Matrix, Vector, Vector>;
} // namespace Preconditioner


namespace Solver
{
  template <class Vector>
  using CGSolver = Dune::CGSolver<Vector>;


  template <class Matrix>
  struct DirectSparseSolvers
  {
    using UMFPack = Dune::UMFPack<Matrix>;

    using LDL = Dune::LDL<Matrix>;

    using SolutionStatistics = Dune::InverseOperatorResult;
  };


  template <class Vector, class PDELabAssemblerTraits>
  struct PDELabSolverTraits
  {
  private:

    using GO = typename PDELabAssemblerTraits::GridOperator;


  public:

    using CG_Solver_AMG_Prec = Dune::PDELab::ISTLBackend_SEQ_CG_AMG_SSOR<GO>;
    using CG_Solver_ILU0_Prec = Dune::PDELab::ISTLBackend_SEQ_CG_ILU0;

    template <class SolverBackend>
    using Solver =
      Dune::PDELab::StationaryLinearProblemSolver<GO, SolverBackend, Vector>;

    using SolutionStatistics = Dune::InverseOperatorResult;
  };
} // namespace Solvers
} // namespace Traits
} // namespace GRFS

#endif

