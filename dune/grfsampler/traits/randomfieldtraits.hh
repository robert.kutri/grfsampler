#ifndef RANDOMFIELDTRAITS_HH
#define RANDOMFIELDTRAITS_HH

#include <type_traits>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/pdelab/finiteelementmap/p0fem.hh>

#include <dune/grfsampler/traits/switches.hh>
#include <dune/grfsampler/traits/gridtraits.hh>


namespace GRFS
{
namespace Traits
{
  template <Switches::Discretization d>
  struct RandomFieldTraits
  {
  private:

    using DomainVector =
      typename GridTraits<Switches::GridLowerLeft::GENERIC>::DomainVector;

    static constexpr unsigned int griddimension{
      GridTraits<Switches::GridLowerLeft::GENERIC>::dimension };

    // we need this, because as of c++17, we cannot use a lambda-expression
    // in a template argument to deduce the type in std::conditional
    static constexpr bool ispq1fem{ d == Switches::Discretization::PQ1FEM };


  public:

    // the dimension of the domain on which the random field is defined
    static constexpr unsigned int dimension{ griddimension };

    // the method used to discretize the sampling SPDE problem
    static_assert(d == Switches::Discretization::PQ1FEM or
                  d == Switches::Discretization::CCFV);
    static constexpr Switches::Discretization discretization = d;

    // the order of the piecewise polynomial basis. 1 for PQ1FEM, 0 for CCFV
    // implemented by immediately evaluating a lambda (should be constexpr)
    static constexpr unsigned int order =
      []
      {
          if constexpr (discretization == Switches::Discretization::PQ1FEM)
            return 1;
          else
            return 0;
      }();

    using RealNumber = double;
    using Seed = unsigned int;

    using ScalarVectorEntry = Dune::FieldVector<RealNumber, 1>;
    using ScalarMatrixEntry = Dune::FieldMatrix<RealNumber, 1, 1>;
    using FlatDofVector = Dune::BlockVector<ScalarVectorEntry>;
    using FlatSparseMatrix = Dune::BCRSMatrix<ScalarMatrixEntry>;

    template <class GridView>
    using PQ1Basis =
      Dune::Functions::LagrangeBasis<GridView, 1, RealNumber>;

    template <class GridView>
    using FiniteVolumeBasis =
      Dune::PDELab::P0LocalFiniteElementMap<DomainVector, RealNumber,
                                            dimension>;

    template <class GridView>
    using Basis = typename
      std::conditional<ispq1fem,
                       PQ1Basis<GridView>, FiniteVolumeBasis<GridView>
      >::type;

    template <class GridView>
    using DiscreteGlobalBasisFunction =
      Dune::Functions::DiscreteGlobalBasisFunction<PQ1Basis<GridView>,
                                                   FlatDofVector>;
  };
} // namespace Traits
} // namespace GRFS

#endif

