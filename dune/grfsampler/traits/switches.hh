#ifndef SWITCHES_HH
#define SWITCHES_HH


namespace GRFS
{
namespace Traits
{
  namespace Switches
  {
    enum class GridLowerLeft{ NULL_GLL, DEFAULT, GENERIC };

    enum class Discretization{ CIRCULANTEMBEDDING, PQ1FEM, CCFV };
  } // namespace Switches
} // namespace Traits
} // namespace GRFS

#endif

