#ifndef TEST_HH
#define TEST_HH

#include <cstddef>
#include <algorithm>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/assembly/quadrature.hh>
#include <dune/grfsampler/assembly/assemblyutility.hh>


using GlobalTypes::FlatDofVector;


namespace Utility
{
  template <class FEBasis>
  GlobalTypes::SparseMatrix assemble_mass_matrix(
    const FEBasis& febasis,
    const GRFS::Assembly::QuadratureInformation& quadinfo
    )
  {
    using Index = typename GlobalTypes::SparseMatrix::size_type;

    const auto basissize = febasis.size();
    const auto buildmode = GlobalTypes::SparseMatrix::row_wise;

    auto massmatrix = GlobalTypes::SparseMatrix(basissize, basissize,
                                                quadinfo.nnz, buildmode);

    GRFS::Assembly::set_up_sparsity_pattern(massmatrix, quadinfo.pattern);

    const auto qorder = GlobalTypes::DIM * GlobalTypes::ORD;
    const auto quadrature =
      Dune::QuadratureRules<GlobalTypes::RealNumber, GlobalTypes::DIM>::rule(
        GlobalTypes::ElementType, qorder);

    auto gridview = febasis.gridView();
    auto localview = febasis.localView();

    auto locmatrix = GRFS::Assembly::LocalMatrix{};
    bool locmatrixassembled{ false };

    for (const auto& element : elements(gridview))
    {
      localview.bind(element);

      const auto geometry = element.geometry();
      const auto& locbasis = localview.tree().finiteElement().localBasis();
      const auto locdim = locbasis.size();

      if (!locmatrixassembled)
        GRFS::Assembly::assemble_local_mass_matrix(locmatrix, locbasis,
                                                   quadrature);

      const auto jdet = geometry.integrationElement(quadrature[0].position());

      for (Index ilocal{0}; ilocal < locdim; ++ilocal)
      {
        const auto iglobal = localview.index(ilocal);

        for (Index jlocal{0}; jlocal < locdim; ++jlocal)
        {
          const auto jglobal = localview.index(jlocal);

          massmatrix[iglobal][jglobal] += jdet * locmatrix[ilocal][jlocal];
        }
      }
    }

    return massmatrix;
  }


  // signature f(GlobalTypes::DomainVector)
  template<class AnalyticFunction, class FEBasis>
  FlatDofVector assemble_rhs_from_analytic(
    const AnalyticFunction& analyticfunction,
    const FEBasis& febasis)
  {
    auto rhs = FlatDofVector(febasis.size());
    rhs = 0.0;

    auto gridview = febasis.gridView();

    const int qord{ GlobalTypes::DIM * GlobalTypes::ORD };
    const auto quadparams =
      GRFS::Assembly::get_quadrature_parameters(GlobalTypes::ElementType, qord);

    const auto& qp = quadparams.points;
    const auto& qw = quadparams.weights;

    auto localview = febasis.localView();

    for (const auto& element : elements(gridview))
    {
      auto geometry = element.geometry();

      localview.bind(element);
      const auto& locbasis = localview.tree().finiteElement().localBasis();

      const auto jacdet = geometry.integrationElement(qp[0]);

      // loop over all quadrature points
      const std::size_t Q{ qp.size() };
      for (std::size_t q{0}; q < Q; ++q)
      {
        // evaluate the analytic right-hand side at the quadrature point in
        // global coordinates, i.e. compute f(\mu_t(\xi_q))
        const auto qglobal = geometry.global(qp[q]);
        auto feval = analyticfunction(qglobal);

        // evaluate the shape function basis at the quadrature point
        std::vector<GRFS::Assembly::ShapeFunctionValue> sfbasisevals;
        locbasis.evaluateFunction(qp[q], sfbasisevals);

        // distribute local contributions
        const auto sfbasissize = sfbasisevals.size();
        for (std::size_t k{0}; k < sfbasissize; ++k)
        {
          const auto kglobal = localview.index(k);
          rhs[kglobal] += jacdet * qw[q] * feval * sfbasisevals[k];
        }
      }
    }

    return rhs;
  }


  template <class TargetGV, class RFS>
  std::vector<typename RFS::RealNumber> cell_center_values(
      const TargetGV& targetgv,
      const RFS& rfs)
  {
    auto cellcentervals = std::vector<typename RFS::RealNumber>{};
    for (const auto& element : elements(targetgv))
    {
      const auto center = element.geometry().center();
      cellcentervals.push_back(rfs.evaluate(element, center));
    }

    return cellcentervals;
  }



  template <class GridFunction>
  FlatDofVector compute_cell_center_values(const GridFunction& rfgridfcn)
  {
    auto gridview = rfgridfcn.basis().gridView();
    auto localfunction = localFunction(rfgridfcn);

    auto ccvalues = FlatDofVector(gridview.size(0));
    auto ccvaluesentry = ccvalues.begin();

    for (const auto& element : elements(gridview))
    {
      localfunction.bind(element);
      auto geometry = element.geometry();

      *ccvaluesentry++ = localfunction(geometry.center());
    }

    return ccvalues;
  }
} // namespace Test

#endif

