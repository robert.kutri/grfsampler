#ifndef INIFILEPARSING_HH
#define INIFILEPARSING_HH

#include <memory>
#include <random>
#include <string>
#include <sstream>
#include <vector>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>


namespace Utility
{
  // requires a [YASPGRID] section and corresponding options in the ini file
  std::unique_ptr<GlobalTypes::Grid>
  set_up_grid_unique_ptr(const Dune::ParameterTree& ptree)
  {
    using GlobalTypes::RealNumber;

    // parse the parameters
    const auto ncellsx = ptree.get<int>("YASPGRID.ncellsx");
    const auto ncellsy = ptree.get<int>("YASPGRID.ncellsy");

    const auto urx = ptree.get<RealNumber>("YASPGRID.upperrightx", 1.0);
    const auto ury = ptree.get<RealNumber>("YASPGRID.upperrighty", 1.0);

    // set grid parameters
    const auto upperright = GlobalTypes::DomainVector{ urx, ury };
    const auto cells = GlobalTypes::CellLayout{ ncellsx, ncellsy };

    // allocate the grid
    return std::make_unique<GlobalTypes::Grid>(upperright, cells);
  }


  template <typename ParameterValue = double>
  Statistics::MaternParameters<ParameterValue>
    parse_matern_parameters(const Dune::ParameterTree& ptree,
                            const std::string& section)
  {
    const auto variance =
      ptree.get<ParameterValue>(section + ".variance", 0.0);

    const auto corrlength =
      ptree.get<ParameterValue>(section + ".corrlength", 0.0);

    const auto smoothness =
      ptree.get<ParameterValue>(section + ".smoothness", 0.0);

    return Statistics::MaternParameters<ParameterValue>{ variance,
                                                         corrlength,
                                                         smoothness };
  }


  GlobalTypes::Seed parse_seed_value(const std::string& section,
                                     const Dune::ParameterTree& ptree)
  {
    auto userandomseed = ptree.get<bool>(section + ".randomseed");

    auto seed = GlobalTypes::Seed{};
    if (userandomseed)
    {
      auto rd = std::random_device{};
      seed = rd();
    }
    else
    {
      seed = ptree.get<GlobalTypes::Seed>(section + ".seed");
    }

    return seed;
  }


  std::vector<std::string> get_option_split(const Dune::ParameterTree& ptree,
                                            const std::string& optionName,
                                            const std::string& defaultOption)
  {
    auto iss =
      std::istringstream(ptree.get<std::string>(optionName, defaultOption));

    return std::vector<std::string>(std::istream_iterator<std::string>{iss},
                                    std::istream_iterator<std::string>());
  }
} // namespace Utility

#endif

