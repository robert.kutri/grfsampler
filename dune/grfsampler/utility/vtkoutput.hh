#ifndef VTKOUTPUT_HH
#define VTKOUTPUT_HH

#include <string>

#include <dune/common/unused.hh>

#include <dune/grid/io/file/vtk/function.hh>

namespace Utility
{
  template <class TargetGridView, class RandomFieldSample>
  class RandomFieldSampleVTKFunction
    : public Dune::VTKFunction<TargetGridView>
  {
    using Base = Dune::VTKFunction<TargetGridView>;


  public:

    using Base::dim;
    using Entity = typename Base::Entity;
    using ctype  = typename Base::ctype;


    RandomFieldSampleVTKFunction(const RandomFieldSample& rfs,
                                 const std::string& filename)
    : rfs_{ rfs }
    , filename_{ filename }
    {}


    virtual int ncomps() const override
    {
      return 1;
    }


    virtual double evaluate(int comp,
                            const Entity& e,
                            const Dune::FieldVector<ctype, dim>& xi)
      const override
    {
      DUNE_UNUSED_PARAMETER(comp);
      return static_cast<double>(rfs_.evaluate(e, xi));
    }


    virtual std::string name() const override
    {
      return filename_;
    }

  protected:

    const RandomFieldSample& rfs_;
    std::string filename_;
  };
} // namespace Utility
