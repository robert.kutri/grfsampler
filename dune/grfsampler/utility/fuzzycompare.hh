#ifndef FUZZYCOMPARE_HH
#define FUZZYCOMPARE_HH

#include <cmath>
#include <dune/common/float_cmp.hh>
#include <dune/common/fvector.hh>

#include <dune/grfsampler/globaltypes.hh>


namespace Utility
{
  /** is_equal
   *  \brief Check two floating point numbers for equality.
   *
   *  \param[in] x1
   *  \param[in] x2
   *  \param aEps absolute precision. Numbers closer than aEps to one another
   *              are considered equal.
   *  \param rEps relative precision. Numbers are considered equal, if they
   *              are within rEps of one another.
   */
  template <typename T>
  struct DefaultTolerance
  {
    static constexpr T abs{ 1e-12 };
    static constexpr T rel{ 1e-8 };
  };


  template <>
  struct DefaultTolerance<float>
  {
    static constexpr float abs{ 1e-6 };
    static constexpr float rel{ 1e-6 };
  };


  template <>
  struct DefaultTolerance<long double>
  {
    static constexpr long double abs{ 1e-15 };
    static constexpr long double rel{ 1e-12 };
  };


  template <typename T>
  bool is_equal(const T& x1,
                const T& x2,
                T aEps = DefaultTolerance<T>::abs,
                T rEps = DefaultTolerance<T>::rel)
  {
    if (std::abs(x1 - x2) <= aEps)
      return true;

    return Dune::FloatCmp::eq(x1, x2, rEps);
  }


  template <typename T>
  inline bool is_equal(const Dune::FieldVector<T, 1>& fv1,
                       const Dune::FieldVector<T, 1>& fv2,
                       T aEps = DefaultTolerance<T>::abs,
                       T rEps = DefaultTolerance<T>::rel)
  {
    return is_equal(fv1[0], fv2[0], aEps, rEps);
  }


  template <typename T>
  inline bool is_equal(const Dune::FieldMatrix<T, 1, 1>& fm1,
                       const Dune::FieldMatrix<T, 1, 1>& fm2,
                       T aEps = DefaultTolerance<T>::abs,
                       T rEps = DefaultTolerance<T>::rel)
  {
    return is_equal(fm1[0][0], fm2[0][0], aEps, rEps);
  }


  template <typename T>
  inline bool is_equal(const Dune::FieldVector<T, 1>& fv,
                       const T& x,
                       T aEps = DefaultTolerance<T>::abs,
                       T rEps = DefaultTolerance<T>::rel)
  {
    return is_equal(fv[0], x, aEps, rEps);
  }


  /** is_symmetric
   *  \brief Check whether a matrix is symmetric.
   *  \note For a sparse matrix, the sparsity pattern must also be symmetric.
   *
   *  \param[in] mat either dense matrix, or matrix with block size 1.
   *  \exception Dune::ISTLError sparsity pattern is not symmetric.
   */
  bool is_symmetric(const GlobalTypes::SparseMatrix& mat)
  {
    // iterate through the lower triangle (excluding diagonal)
    for (auto row = mat.begin(); row != mat.end(); ++row)
    {
      auto ri = row.index();

      // check whether mat[i][j] == mat[j][i] for all i,j
      for (auto col = row->begin(); col.index() < ri; ++col)
        if(!is_equal(*col, mat[col.index()][row.index()]))
          return false;
    }

    return true;
  }


  /** ranges_equal
   *  \brief Check two ranges, potentially of different types, for equality.
   *
   *  \note This method works, as long as both types can be iterated through
   *        using a single iterator (i.e. one dimensional containers).
   *
   *  \note The absolute and relative precisions refer to equality of the
   *        entries.
   *
   *  \param[in] r1
   *  \param[in] r2
   *  \param aEps Absolute precision. Entries closer than aEps to one another
   *              are considered equal.
   *  \param rEps Relative precision. Entries are considered equal, if they
   *              are within rEps of one another.
   */
  template<class Range1, class Range2>
  bool ranges_equal(const Range1& r1, const Range2& r2,
                    double aEps=1e-12, double rEps=1e-8)
  {
    auto entries_equal = [aEps, rEps](const auto& e1, const auto& e2)
                         {
                           return is_equal(e1, e2, aEps, rEps);
                         };

    auto unequalpos = std::mismatch(r1.begin(), r1.end(),
                                    r2.begin(), r2.end(),
                                    entries_equal);

    return (unequalpos.first == r1.end() and unequalpos.second == r2.end());
  }


  /** block_vectors_equal
   *  \brief Check two block vectors for equality.
   *
   *  \note The absolute and relative precisions refer to equality of the
   *        entries.
   *
   *  \param[in] bv1
   *  \param[in] bv2
   *  \param aEps Absolute precision. Entries closer than aEps to one another
   *              are considered equal.
   *  \param rEps Relative precision. Entries are considered equal, if they
   *              are within rEps of one another.
   */
  bool block_vectors_equal(const GlobalTypes::FlatDofVector& bv1,
                           const GlobalTypes::FlatDofVector& bv2,
                           double aEps=1e-12, double rEps=1e-8)
  {
    auto blocks_equal = [aEps, rEps](const auto& block1, const auto& block2)
                        {
                          return ranges_equal(block1, block2, aEps, rEps);
                        };

    auto unequalpos = std::mismatch(bv1.begin(), bv1.end(),
                                    bv2.begin(), bv2.end(),
                                    blocks_equal);

    return (unequalpos.first == bv1.end() and unequalpos.second == bv2.end());
  }
} // namespace Utility

#endif

