#ifndef RATEESTIMATION_HH
#define RATEESTIMATION_HH

#include <vector>
#include <array>
#include <cmath>
#include <string>
#include <sstream>
#include <cctype>
#include <algorithm>
#include <fstream>

#include <dune/common/parametertree.hh>
#include <dune/common/exceptions.hh>

#include <dune/grfsampler/traits/gridtraits.hh>
#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>


namespace Utility
{
  template <class Scalar>
  class RateEstimationSetup
  {
  protected:

    using GridTraits =
      GRFS::Traits::GridTraits<GRFS::Traits::Switches::GridLowerLeft::GENERIC>;


  public:

    using RealNumber = Scalar;
    using DomainVector = typename GridTraits::DomainVector;
    using CellLayout = typename GridTraits::CellLayout;
    using RectangularDomain = typename GridTraits::RectangularDomain;


    RateEstimationSetup(const Dune::ParameterTree& ptree)
    : numrefs_{ ptree.get<int>("GRID.refinements") }
    , numsamples_{ ptree.get<std::size_t>("MC.numsamples") }
    , targetdomain_{ parse_target_domain("GRID", ptree) }
    , numcellsinit_{ parse_initial_cell_layout("GRID", ptree) }
    , qoidomain_{ parse_qoi_domain(
                    ptree.get<int>("MC.qoidomainhalfwidth", -1)) }
    , qoidomhalfwidth_{ ptree.get<int>("MC.qoidomainhalfwidth", -1) }
    , usecereference_{ ptree.get<bool>("MC.usecereference", false) }
    , rfininame_{ "" }
    , outputconfigfilename_{ parse_output_config_filename("FILES", ptree) }
    , meandatasignature_{ parse_mean_data_signature("FILES", ptree) }
    , vardatasignature_{ parse_variance_data_signature("FILES", ptree) }
    {
      if (numrefs_ < 0)
        DUNE_THROW(Dune::Exception, "Error: Invalid number of runs.");

      if (numsamples_ < 2)
	DUNE_THROW(Dune::Exception, "Error: Invalid number of samples.");

      if (numcellsinit_.empty())
	DUNE_THROW(Dune::Exception, "Error: Invalid initial number of cells.");

    }


    // constructor used for mock objects in testing
    RateEstimationSetup(const RectangularDomain& targetdomain,
                        const CellLayout& layout)
    : numrefs_{ 0 }
    , numsamples_{ 0 }
    , targetdomain_{ targetdomain }
    , numcellsinit_{ layout }
    , qoidomain_{}
    , qoidomhalfwidth_{}
    , usecereference_{}
    , rfininame_{}
    , outputconfigfilename_{}
    , meandatasignature_{}
    {}


    RateEstimationSetup(const DomainVector& L, const CellLayout& layout)
      : RateEstimationSetup(RectangularDomain{ { {0., 0.}, {L[0], L[1]} } },
                            layout)
    {}


    int number_of_refinements() const
    {
      return numrefs_;
    }


    std::size_t number_of_samples() const
    {
      return numsamples_;
    }


    RectangularDomain target_domain() const
    {
      return targetdomain_;
    }


    CellLayout initial_cell_layout() const
    {
      return numcellsinit_;
    }


    RectangularDomain qoi_domain() const
    {
      return qoidomain_;
    }


    bool use_ce_reference() const
    {
      return usecereference_;
    }


    std::string random_field_ini_filename() const
    {
      return rfininame_;
    }


    std::string output_config_filename() const
    {
      return outputconfigfilename_;
    }


    std::string mean_data_signature() const
    {
      return meandatasignature_;
    }


    std::string variance_data_signature() const
    {
      return vardatasignature_;
    }


    RealNumber mesh_width(const CellLayout& layout) const
    {
      static_assert(GridTraits::dimension == 2);

      const auto meshwidthx = (targetdomain_[1][0] - targetdomain_[0][0]) /
        static_cast<RealNumber>(layout[0]);

      const auto meshwidthy = (targetdomain_[1][1] - targetdomain_[0][1]) /
        static_cast<RealNumber>(layout[1]);

      return std::fmax(meshwidthx, meshwidthy);
    }


    template <class MPIHelper>
    void write_csv_parameters(const Dune::ParameterTree& ptree,
                              const MPIHelper& mpihelper,
                              std::ofstream& ofs,
                              bool writefilenames = true) const
    {
      assert(ofs);

      auto var = 0.0;
      auto rho = 0.0;
      auto nu  = 0.0;

      if (rfininame_ == "")
      {
        var = ptree.get<RealNumber>("RANDOMFIELD.variance", 0.0);
        rho = ptree.get<RealNumber>("RANDOMFIELD.corrlength", 0.0);
        nu  = ptree.get<RealNumber>("RANDOMFIELD.smoothness", 0.0);
      }
      else
      {
        DUNE_THROW(Dune::NotImplemented, "Implement Circulant embedding ini.");
      }

      if (var * rho * nu == 0.0)
        DUNE_THROW(Dune::Exception, "Error: At least one random field "
                   "parameter is zero");

      if (numcellsinit_[0] != numcellsinit_[1])
        DUNE_THROW(Dune::NotImplemented, "For now only same number of cells "
                   "in each dimension is supported.");

      ofs << "# numprocessors, initcellsperdim, numrefinements, samplesize, "
          << "qoidomhalfwidth, variance, corrlength, smoothness, CEReference"
          << std::endl;
      ofs << mpihelper.size() << ", " << numcellsinit_[0] << ", " << numrefs_
          << ", " << numsamples_ << ", " << qoidomhalfwidth_ << ", " << var
          << ", " << rho << ", " << nu << ", " << usecereference_ << std::endl;

      if (writefilenames)
        ofs << meandatasignature_ << ", " << vardatasignature_ << std::endl;
    }


  protected:

    // -------------------------- MEMBERS -------------------------------------

    // the total number of mesh refinements, which to consider
    const int numrefs_;

    // number of samples for each run
    const std::size_t numsamples_;

    // The domain, on which the UQ problem is to be solved
    const RectangularDomain targetdomain_;

    // the number of cells per dimension for the coarsest/initial mesh
    const CellLayout numcellsinit_;

    // The corners of the rectangle, that represents the targetdomain over which
    // to integrate to obtain the quantity of interest
    const RectangularDomain qoidomain_;
    const int qoidomhalfwidth_;

    // whether to use estimates generated with a Circulant Embedding random
    // field on the finest level as reference, rather than the estimates
    // on the finest level themselves
    const bool usecereference_;

    // the filename of the configuration file for the random field
    const std::string rfininame_;

    // the filename of the file containing the simulation configuration
    const std::string outputconfigfilename_;

    // the filename of the fiel containing the QoI mean values
    const std::string meandatasignature_;

    // the signature of the file containing the QoI variances
    const std::string vardatasignature_;


    // -------------------------- METHODS -------------------------------------

    RectangularDomain parse_target_domain(const std::string& section,
                                          const Dune::ParameterTree& pt) const
    {
      const auto llstrarr =
        Utility::get_option_split(pt, section + ".lowerleft", "0.0 0.0");

      const auto urstrarr =
        Utility::get_option_split(pt, section + ".upperright", "0.0 0.0");

      const auto lowerleft = DomainVector{ std::stod(llstrarr[0]),
                                           std::stod(llstrarr[1]) };
      const auto upperright = DomainVector{ std::stod(urstrarr[0]),
                                            std::stod(urstrarr[1]) };

      if (upperright[0] == lowerleft[0] or upperright[1] == lowerleft[1])
        DUNE_THROW(Dune::Exception, "Invalid target domain");

      return RectangularDomain{ lowerleft, upperright };
    }


    CellLayout parse_initial_cell_layout(const std::string& section,
                                         const Dune::ParameterTree& pt) const
    {
      const auto layout =
        Utility::get_option_split(pt, section + ".initcells", "0 0");

      return CellLayout{ std::stoi(layout[0]), std::stoi(layout[1]) };
    }


    std::string parse_randomfield_ini_filename(const Dune::ParameterTree& pt)
    const
    {
      auto filename = pt.get<std::string>("FILES.randomfieldconfig", "");

      filename.erase(
          std::remove_if(filename.begin(), filename.end(), ::isspace),
          filename.end());

      return filename;
    }


    std::string parse_output_config_filename(const std::string& section,
                                             const Dune::ParameterTree& pt)
    const
    {
      auto filename = pt.get<std::string>(section + ".config", "config.csv");

      filename.erase(
          std::remove_if(filename.begin(), filename.end(), ::isspace),
          filename.end());

      return filename + ".csv";
    }


    std::string parse_mean_data_signature(const std::string& section,
                                         const Dune::ParameterTree& pt) const
    {
      auto signature = pt.get<std::string>(section + ".meandata", "INVALID");

      signature.erase(
          std::remove_if(signature.begin(), signature.end(), ::isspace),
          signature.end());

      return signature;
    }


    std::string parse_variance_data_signature(const std::string& section,
                                             const Dune::ParameterTree& pt)
    const
    {
      auto signature =
        pt.get<std::string>(section + ".variancedata", "INVALID");

      signature.erase(
          std::remove_if(signature.begin(), signature.end(), ::isspace),
          signature.end());

      return signature;
    }


    RectangularDomain parse_qoi_domain(const int& idhw) const
    {
      // assume numcellsinit_ was already initialized
      assert(!numcellsinit_.empty());

      // negative idhw signals that no qoi domain is needed
      if (idhw >= 0)
      {
        // In the end, the integration targetdomain will be a rectangular
        // (2*idhw) x (2*idhw) domain inside the coarsest grid
        if (idhw == 0)
          DUNE_THROW(Dune::Exception,
                     "Error: Invalid Integration domain width");

        if (numcellsinit_[0] % 2 == 1 or numcellsinit_[1] % 2 == 1)
          DUNE_THROW(Dune::Exception,
                     "Error: Cells per dimension must be even");

         // compute the mesh widths
        const auto h0 = (targetdomain_[1][0] - targetdomain_[0][0]) /
          static_cast<RealNumber>(numcellsinit_[0]);
        const auto h1 = (targetdomain_[1][1] - targetdomain_[0][1]) /
          static_cast<RealNumber>(numcellsinit_[1]);

        // number of cells per dimension not in the integration domain. Counting
        // from the boundary. Only works properly for even number of cells per
        // dimension in the coarsest grid
        const auto pd0 = static_cast<RealNumber>(numcellsinit_[0] / 2 - idhw);
        const auto pd1 = static_cast<RealNumber>(numcellsinit_[1] / 2 - idhw);

        if (pd0 < 0 or pd1 < 0)
          DUNE_THROW(Dune::Exception, "Error: Integration domain for quantity "
                     "of interest is larger than the problem domain");

        return RectangularDomain{ DomainVector{ pd0 * h0,
                                                pd1 * h1 },
                                  DomainVector{ (pd0 + 2 * idhw) * h0,
                                                (pd1 + 2 * idhw) * h1 } };
      }

      return RectangularDomain{};
    }
  };
} // namespace Utility

#endif

