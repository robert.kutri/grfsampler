#ifndef GRIDHELPER_HH
#define GRIDHELPER_HH

#include <vector>
#include <array>
#include <bitset>

#include <dune/common/parametertree.hh>
#include <dune/common/fvector.hh>
#include <dune/common/exceptions.hh>


namespace Utility
{
  // direct copy of the GridHelper from dune-randomfield, fieldgenerator.cc
  template<typename GT>
  class GridHelper
  {
    enum {dim = GT::dim};

    using DF = typename GT::DomainField;

    int levels;
    std::vector<DF> maxExt;
    std::vector<unsigned int> minCells, maxCells;

    public:

    GridHelper(const Dune::ParameterTree& config)
    {
      levels   = config.get<int>                      ("grid.levels",1);
      maxExt   = config.get<std::vector<DF>>          ("grid.extensions");
      maxCells = config.get<std::vector<unsigned int>>("grid.cells");

      if (maxExt.size() != maxCells.size())
	DUNE_THROW(Dune::Exception,
		   "cell and extension vectors differ in size");

      minCells = maxCells;
      for (int i = 0; i < levels - 1; i++)
	for (unsigned int j = 0; j < maxCells.size(); j++)
	{
	  if (minCells[j]%2 != 0)
	    DUNE_THROW(Dune::Exception, "cannot create enough levels for "
		       "hierarchical grid, check number of cells");

	  minCells[j] /= 2;
	}
    }

    Dune::FieldVector<DF,dim> L() const
    {
      Dune::FieldVector<DF,dim> Lvector;

      for (unsigned int i = 0; i < dim; i++)
	Lvector[i] = maxExt[i];

      return Lvector;
    }

    std::array<int,dim> N() const
    {
      std::array<int,dim> Nvector;

      for (unsigned int i = 0; i < dim; i++)
        Nvector[i] = minCells[i];

      return Nvector;
    }

    std::bitset<dim> B() const
    {
      return std::bitset<dim>(false);
    }
  };
} // namespace Utility

#endif

