#ifndef MOMENTESTIMATORS_HH
#define MOMENTESTIMATORS_HH

#include <vector>
#include <algorithm>
#include <random>
#include <cstdint>
#include <functional>

#include <dune/grfsampler/statistics/statisticstypes.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>


namespace Statistics
{
  template <class Sample>
  RealNumber mean(const Sample& sample)
  {
    auto mean = RealNumber{ 0.0 };
    auto sizef = static_cast<RealNumber>(sample.size());

    for (const RealNumber& entry : sample)
      mean += entry / sizef;

    return mean;
  }


  /** compute_sample_mean
   *
   *  Compute an unbiased estimate of the sample mean.
   *
   *  \param[in] obs a container for samples
   *  \return the sample mean
   */
  template <class Sample>
  Sample sample_mean(const Experiment<Sample>& obs)
  {
    using Index = typename Sample::size_type;

    // assume all samples have the same size and initialize mean vector
    auto vectorsize = obs.front().size();
    auto mean = Sample(vectorsize);
    std::fill(mean.begin(), mean.end(), 0.0);

    // sum corresponding entries of the samples
    auto ssdouble = static_cast<RealNumber>(obs.size());
    for (const auto& sample : obs)
      for (Index entryidx{0}; entryidx < vectorsize; ++entryidx)
        mean[entryidx] += sample[entryidx] / ssdouble;

    return mean;
  }


  RealNumber unbiased_sample_variance(const std::vector<RealNumber>& sample,
                                      const RealNumber& samplemean)
  {
    using R = RealNumber;
    using Utility::is_equal;
    using namespace std::placeholders;

    // little exercise in the <functional> header
    auto subtrmean = std::bind(std::minus<R>(), _1, samplemean);
    auto square = std::bind(std::multiplies<R>(), _1, _1);
    auto sumsqdiff = [subtrmean, square](const R& sum, const R& r) -> R
                     { return sum + square(subtrmean(r));};

    auto svar = std::accumulate(sample.cbegin(), sample.cend(), 0., sumsqdiff);

    const auto ssizem1f = static_cast<R>(sample.size() - 1);
    return svar / ssizem1f;
  }


  // assumes all Samples have the same dimension
  template <class Sample>
  Sample compute_sample_variance(const Experiment<Sample>& obs,
                                 const Sample& samplemean)
  {
    using Index = typename Sample::size_type;
    auto samplesizem1 = static_cast<RealNumber>(obs.size() - 1);
    auto vectorsize = samplemean.size();

    auto samplevariance = Sample(vectorsize);
    std::fill(samplevariance.begin(), samplevariance.end(), 0.0);

    for (const auto& sample : obs)
    {
      for (Index i{0}; i < vectorsize; ++i)
      {
        // if component value is equal to the mean we don't have a contribution
        if (Utility::is_equal(sample[i], samplemean[i]))
          continue;
        else
          samplevariance[i] += (sample[i] - samplemean[i]) *
                               (sample[i] - samplemean[i]) / samplesizem1;
      }
    }

    return samplevariance;
  }


  template <class Sample>
  Sample sample_variance_zero_mean(const Experiment<Sample>& obs)
  {
    const auto ssizem1 = static_cast<RealNumber>(obs.size() - 1);
    const auto vsize = obs.front().size();

    auto samplevar = Sample(vsize);
    std::fill(samplevar.begin(), samplevar.end(), 0.0);

    for (const auto& sample : obs)
      for (std::size_t i{0}; i < vsize; ++i)
        samplevar[i] += sample[i] * sample[i] / ssizem1;

    return samplevar;
  }


  // The variance of the sample variance estimator (i.e. the variance of
  // the chi-squared distribution with samplesize-1 degrees of freedom
  template <typename Size>
  RealNumber variance_of_sample_variance(const RealNumber& popvar,
                                         const Size& samplesize)
  {
    return 2.0 * popvar * popvar / static_cast<RealNumber>(samplesize);
  }


  /** compute_sample_covariance
   *
   *  Compute an unbiased estimate of the sample covariance matrix.
   *
   *  \param[in] obs a container for samples
   *  \param[in] smean an estimate of the sample mean
   *  \return the sample covariance matrix
   */
  template <class Sample>
  Matrix compute_sample_covariance(const Experiment<Sample>& obs,
                                   const Sample& smean)
  {
    using Index = typename Sample::size_type;
    auto vectorsize = smean.size();

    // initialize sample convariance matrix to zero
    auto covmat = Matrix(vectorsize, std::vector<RealNumber>(vectorsize, 0.0));

    // compute entries
    for (const auto& sample : obs)
      for (Index j{0}; j < vectorsize; ++j)
        for (Index k{0}; k < vectorsize; ++k)
          covmat[j][k] += (sample[j] - smean[j]) * (sample[k] - smean[k]);

    // scale entries to yield an unbiased estimator
    auto scaling = static_cast<RealNumber>(obs.size() - 1);
    for (auto& row : covmat)
      for (auto& col : row)
        col /= scaling;

    return covmat;
  }


  // sample covariance estimator assuming population mean zero
  template <class Sample>
  Matrix sample_covariance_zero_mean(const Experiment<Sample>& experiment)
  {
    using Index = typename Sample::size_type;
    auto vectorsize = experiment[0].size();
    auto scaling = 1.0 / static_cast<RealNumber>(experiment.size());

    auto covmat = Matrix(vectorsize, std::vector<RealNumber>(vectorsize, 0.0));

    // as we use the population mean (and not the sample mean) in the
    // computation of the covariance, this time an unbiased estimate is
    // achieved by dividing by the sample size (not sample size minus 1)
    for (const auto& sample : experiment)
      for (Index j{0}; j < vectorsize; ++j)
        for (Index k{0}; k < vectorsize; ++k)
          covmat[j][k] += sample[j] * sample[k] * scaling;

    return covmat;
  }
} // namepsace Statistics

#endif

