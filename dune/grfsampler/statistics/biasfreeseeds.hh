#ifndef BIASFREESEEDS_HH
#define BIASFREESEEDS_HH

#include <array>
#include <random>
#include <memory>
#include <algorithm>
#include <iterator>


namespace Statistics
{
  class BiasFreeSeeds
  {
    static constexpr std::array<int, 120> s_initseq{
       3,   1,   4,  -2,   5,   9,   2,   6,  -5,  -7,   7,   8,   2,   3,   5,
       7,  11,  13,  17,  19,  23,  29,  31,  37,  41,  43,  47,  53,  59,  61,
      67,  71,  73,  79,  83,  89,  97, 101, 103, 107, 109, 113, 127, 131, 137,
     139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223,
     227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307,
     311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397,
     401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487,
     491, 499, 503, 509, 521, 523, 541, -42, -28,  42, -11, 101, 111,  18,  95
    };


  public:

    BiasFreeSeeds()
    : seedseq_{ nullptr }
    {}


    std::seed_seq& seed_sequence()
    {
      seedseq_.reset(new std::seed_seq(s_initseq.begin(), s_initseq.end()));
      return *seedseq_;
    }


    std::seed_seq& seed_sequence(const unsigned int& seed)
    {
      auto initseq = std::vector<int>{};

      std::transform(s_initseq.begin(), s_initseq.end(),
                     std::back_inserter(initseq),
                     [seed](const int& isv){ return isv + seed; });

      seedseq_.reset(new std::seed_seq(initseq.begin(), initseq.end()));

      return *seedseq_;
    }


  private:

    std::unique_ptr<std::seed_seq> seedseq_;
  };
} // namespace Statistics

#endif

