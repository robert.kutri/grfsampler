#ifndef MATERNCOVARIANCE_HH
#define MATERNCOVARIANCE_HH

#include <cmath>

#include <dune/grfsampler/statistics/statisticstypes.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>


namespace Statistics
{
  template <typename ParameterValue = double>
  struct MaternParameters
  {
    using Value = ParameterValue;

    ParameterValue variance;
    ParameterValue corrlength;
    ParameterValue smoothness;
  };


  
  template <typename ParameterValue = double>
  ParameterValue matern_kappa(const MaternParameters<ParameterValue>& mparam)
  {
    // return std::sqrt(8. * mparam.smoothness) / mparam.corrlength;
    return std::sqrt(2. * mparam.smoothness) / mparam.corrlength;
  }


  template <typename ParameterValue = double, typename Range = double>
  Range analytic_matern_covariance(const Range& r,
                                   const MaternParameters<ParameterValue>& mp)
  {
    if (Utility::is_equal(r, 0.0))
    {
      return mp.variance;
    }
    else
    {
      auto nu = mp.smoothness;
      auto kappar = matern_kappa(mp) * r;
      auto modbessel2ndkind = std::cyl_bessel_k(nu, kappar);
      auto scalingfactor = mp.variance / std::pow(2.0, nu-1.0)
                           / std::tgamma(nu);

      return scalingfactor * std::pow(kappar, nu) * modbessel2ndkind;
    }
  }


  template <typename ParameterValue = double>
  DiscreteFunction matern_covariance(
      const ValueArray& val, const MaternParameters<ParameterValue>& rfparam)
  {
    auto materncovvalues = DiscreteFunction{};
    for (const auto& value : val)
      materncovvalues.push_back(analytic_matern_covariance(value, rfparam));

    return materncovvalues;
  }
} // namespace Statistics

#endif

