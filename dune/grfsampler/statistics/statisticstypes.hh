#ifndef STATISTICSTYPES_HH
#define STATISTICSTYPES_HH

#include <vector>

#include <dune/grfsampler/globaltypes.hh>


namespace Statistics
{
  using GlobalTypes::DIM;
  using GlobalTypes::RealNumber;
  using GlobalTypes::DomainVector;

  using ValueArray = std::vector<RealNumber>;
  using DiscreteFunction = std::vector<RealNumber>;
  using Matrix = std::vector<std::vector<RealNumber> >;
  using IndexSet = std::vector<std::size_t>;

  template <class Sample>
  using Experiment = std::vector<Sample>;
}

#endif

