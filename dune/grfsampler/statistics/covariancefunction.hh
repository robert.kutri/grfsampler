#ifndef COVARIANCEFUNCTION_HH
#define COVARIANCEFUNCTION_HH

#include <cmath>
#include <vector>
#include <map>
#include <algorithm>

#include <dune/grfsampler/statistics/statisticstypes.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>
#include <dune/grfsampler/assembly/triangularmatrix.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>


namespace Statistics
{
  class StationaryIsotropicCovarianceFunction
  {};

  // FIXME: For now just place all functions here. Make them a class later on.

  // FIXME: only works for rectangular grid
  // assume lower left corner is at (0, 0)
  inline bool in_layer(const DomainVector& center,
                       const DomainVector& ur,
                       const RealNumber& lwidth)
  {
    bool xinlayer = (center[0] < lwidth) or ((ur[0] - lwidth) < center[0]);
    bool yinlayer = (center[1] < lwidth) or ((ur[1] - lwidth) < center[1]);

    return (xinlayer or yinlayer);
  }


  template <class GridView>
  IndexSet determine_layer_indices(const GridView& gridview,
                                   const DomainVector& upperright,
                                   const RealNumber& layerwidth)
  {
    auto layeridx = IndexSet{};
    auto runningidx = std::size_t{ 0 };
    for (const auto& element : elements(gridview))
    {
      auto cellcenter = element.geometry().center();
      if (in_layer(cellcenter, upperright, layerwidth))
        layeridx.push_back(runningidx);

      ++runningidx;
    }

    return layeridx;
  }


  template <class CellRelatedQuantity>
  void remove_boundary_layer(std::vector<CellRelatedQuantity>& val,
                             const IndexSet& layeridx)
  {
    // remove those entries in val, whose initial index was in layeridx. The
    // decrement layeridx[n] - n accounts for the shrinking of val while values
    // are removed, and the resulting shift in the indices to be removed.
    const auto valbegin = val.begin();
    for (std::size_t n{0}; n < layeridx.size(); ++n)
      val.erase(valbegin + layeridx[n] - n);
  }


  inline RealNumber distance(const DomainVector& x0, const DomainVector& x1)
  {
    return std::hypot(x0[0] - x1[0], x0[1] - x1[1]);
  }


  template <class GridView, class GlobalCoordinate>
  GRFS::Assembly::TriangularMatrix<std::size_t>
  distance_matrix(const GridView& gridview)
  {
    auto cellcenters = std::vector<GlobalCoordinate>{};

    // store the cell centers
    for (const auto& element : elements(gridview))
      cellcenters.push_back(element.geometry().center());

    const auto numcells = static_cast<std::size_t>(cellcenters.size());
    auto distmat = GRFS::Assembly::TriangularMatrix<std::size_t>{};
    distmat.set_size(numcells);

    // do not include the diagonal entry as it is supposed to be zero anyways
    for (std::size_t i{0}; i < numcells; ++i)
      for (std::size_t j{0}; j < i; ++j)
        distmat.set_entry(i, j, distance(cellcenters[i], cellcenters[j]));

    return distmat;
  }


  template <class GridView>
  GRFS::Assembly::TriangularMatrix<std::size_t>
    compute_distance_matrix(const GridView& gridview, const IndexSet& layeridx)
  {
    static_assert(DIM == 2);

    auto cellcenter = std::vector<DomainVector>{};
    for (const auto& element : elements(gridview))
      cellcenter.push_back(element.geometry().center());

    remove_boundary_layer(cellcenter, layeridx);

    auto distmat = GRFS::Assembly::TriangularMatrix<std::size_t>{};
    distmat.set_size(cellcenter.size());
    auto numcells = static_cast<std::size_t>( cellcenter.size() );

    // do not include the diagonal entry as it is supposed to be zero anyways
    for (int i{0}; i < numcells; ++i)
      for (int j{0}; j < i; ++j)
        distmat.set_entry(i, j, distance(cellcenter[i], cellcenter[j]));

    return distmat;
  }


  void map_distance_to_covariance(
    std::map<std::size_t, ValueArray>& covmapping,
    ValueArray& rval,
    const Matrix& covmat,
    const GRFS::Assembly::TriangularMatrix<std::size_t>& distmat,
    const RealNumber& binsize = 1e-6)
  {
    rval.clear();
    covmapping.clear();

    // comparison function used to distinguish distance values
    auto lesscmp = [binsize](const RealNumber& d1, const RealNumber& d2)
                   {
                     return d1 < d2 and std::abs(d1 - d2) > binsize;
                   };

    auto N = distmat.get_number_of_rows();
    for (std::size_t i{0}; i < N; ++i)
    {
      for (std::size_t j{0}; j <= i; ++j)
      {
        const auto distval = static_cast<RealNumber>(distmat.get_entry(i, j));

        // assuming rval is sorted, find the position of distval
        const auto distpos =
          std::lower_bound(rval.begin(), rval.end(), distval, lesscmp);

        // if distval is the new largest value, add it to rval and create
        // a new key in covmapping. Add the corresponding covariance matrix
        // entry to this key
        if (distpos == rval.end())
        {
          rval.push_back(distval);
          covmapping.insert({ rval.size() - 1, ValueArray(1, covmat[i][j]) });
        }
        else // add the covariance value to the existing distance value
          covmapping[distpos - rval.cbegin()].push_back(covmat[i][j]);
      }
    }
  }


  // return a vector, whose i-th entry is the mean covariance value at
  // distance rval[i]
  DiscreteFunction compute_stationary_isotropic_covariance_function(
    const std::map<std::size_t, ValueArray >& covmapping)
  {
    auto covfcn = DiscreteFunction{};

    for (const auto& covsamples : covmapping)
      covfcn.push_back(mean(covsamples.second));

    return covfcn;
  }
} // namespace Statistics

#endif

