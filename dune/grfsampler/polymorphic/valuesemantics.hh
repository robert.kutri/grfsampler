#ifndef VALUE_SEMANTICS_HH
#define VALUE_SEMANTICS_HH

#include <any>
#include <utility>


namespace Polymorphic
{
  // credit due to
  // https://www.fluentcpp.com/2021/01/29/inheritance-without-pointers/
  template<typename Interface>
  class Implementation
  {
  public:

    template<typename ConcreteType>
    Implementation(ConcreteType&& object)
    : storage{ std::forward<ConcreteType>(object) }
    , getter{ [](std::any &storage) -> Interface&
              {
                return std::any_cast<ConcreteType&>(storage);
              }
            }
     {}

    Interface* operator->() { return &getter(storage); }


  private:

    std::any storage;
    Interface& (*getter)(std::any&);
  };
} // namespace Polymorphic
