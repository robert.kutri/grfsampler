#ifndef ASSEMBLYUTILITY_HH
#define ASSEMBLYUTILITY_HH

#include <cstddef>
#include <vector>
#include <set>
#include <algorithm>

#include <dune/geometry/type.hh>
#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/common/capabilities.hh>

#include <dune/grfsampler/assembly/assemblytypes.hh>
#include <dune/grfsampler/assembly/cholesky.hh>


namespace GRFS
{
namespace Assembly
{
  using ColumnIndexSet = std::set<std::size_t>;
  using SparsityPattern = std::vector<ColumnIndexSet>;

  enum class GridConstruction { affine, generic };


  struct QuadratureParameters
  {
    std::vector<DomainVector> points;
    std::vector<RealNumber> weights;
  };


  template <class LocalMatrixFactor>
  class AssemblyDistributor
  {
  public:

    using FEMatrix = SparseMatrix;
    using MassMatrixFactorization =
      Assembly::ElementwiseMassMatrixFactorization<LocalMatrixFactor>;

    AssemblyDistributor() = delete;
    AssemblyDistributor(const AssemblyDistributor& other) = delete;
    AssemblyDistributor& operator=(const AssemblyDistributor& other) = delete;


    AssemblyDistributor(FEMatrix* rawfematrix,
                        MassMatrixFactorization* rawfactor)
      : fematrix_(rawfematrix)
      , massmatfactor_(rawfactor)
    {}


    bool empty() const
    {
      return ((fematrix_ == nullptr) and (massmatfactor_ == nullptr));
    }


    bool matrix_released() const
    {
      return (fematrix_ == nullptr);
    }


    bool factorization_released() const
    {
      return (massmatfactor_ == nullptr);
    }


    FEMatrix* release_matrix()
    {
      return fematrix_.release();
    }


    MassMatrixFactorization* release_factorization()
    {
      return massmatfactor_.release();
    }


  private:

    std::unique_ptr<FEMatrix> fematrix_;
    std::unique_ptr<MassMatrixFactorization> massmatfactor_;
  };


  struct QuadratureInformation
  {
    // is the grid affinely constructed ?
    GridConstruction gridtype;

    // Total number of nonzero matrix entries
    std::size_t nnz;

    // Sparsity pattern
    SparsityPattern pattern;

    QuadratureInformation(const GridConstruction& _gridtype,
                          const std::size_t& _nnz,
                          const SparsityPattern& _pattern)
      : gridtype{ _gridtype }
      , nnz{ _nnz }
      , pattern{ _pattern }
    {}
  };


  // only works for Lagrange Elements
  template <class GridView>
  std::unique_ptr<QuadratureInformation> get_quadrature_information(
    const PQ1Basis<GridView>& pq1basis)
  {
    auto quadratureinfo = std::make_unique<QuadratureInformation>(
      GridConstruction::affine, 0, SparsityPattern(pq1basis.size()));
    auto& sparsitypattern = quadratureinfo->pattern;

    auto gridview = pq1basis.gridView();
    auto localview = pq1basis.localView();

    for (const auto& element : elements(gridview))
    {
      if (!element.geometry().affine())
        quadratureinfo->gridtype = GridConstruction::generic;

      localview.bind(element);

      // For linear Lagrange elements, we have a contribution to global index
      // i, whenever i is the global index of any of the local indices.
      auto numlocindices = localview.tree().size();
      for (std::size_t ilocal{0}; ilocal < numlocindices; ++ilocal)
      {
        auto iglobal = localview.index(ilocal);

        for (std::size_t jlocal{0}; jlocal < numlocindices; ++jlocal)
          sparsitypattern[iglobal].insert(localview.index(jlocal));
      }
    }

    // determine the number of nonzeroes
    auto add_size = [](std::size_t currentsize, const ColumnIndexSet& nextset)
                    {
                      return currentsize + nextset.size();
                    };

    quadratureinfo->nnz = std::accumulate(sparsitypattern.begin(),
                                          sparsitypattern.end(), 0, add_size);

    return quadratureinfo;
  }


  inline void set_up_sparsity_pattern(SparseMatrix& matrix,
                                      const SparsityPattern& spattern)
  {
    for (auto crow = matrix.createbegin(); crow != matrix.createend(); ++crow)
      for (const auto& colindex : spattern[crow.index()])
        crow.insert(colindex);

    return;
  }


  /** get_geometry_type_from_grid_view
   *  /brief exctracts the geometry type of the cells in the grid view
   *
   *  \exception The cells in the grid have several geometry types
   *
   *  \param[in] gridview Dune::GridView we are currently working on
   *  \returns Dune::GeometryType of the cells in the GridView
   */
  // FIXME: Deprecated
  template<class GridView>
  Dune::GeometryType get_geometry_type_from_grid_view(const GridView& gridview)
  {
    constexpr int GRIDDIM{ GridView::Grid::dimension };

    using SingleGeometryGrid =
      Dune::Capabilities::hasSingleGeometryType<typename GridView::Grid>;

    if (!SingleGeometryGrid::v)
      DUNE_THROW(Dune::Exception,
                 "Elements of the grid have several different types.");

    return Dune::GeometryType(SingleGeometryGrid::topologyId, GRIDDIM);
  }


  /** get_quadrature_parameters
   *  \brief Sets up a quadrature rule and extracts the quadrature points and
   *         quadrature weights.
   *
   *  \param[in] gt Dune::GeometryType of the reference element
   *  \param[in] qord Quadrature order to be used.
   *  \returns QuadartureParameters container for quadrature points and
   *           quadrature weights.
   */
  QuadratureParameters
  get_quadrature_parameters(const Dune::GeometryType& gt, const int& qord)
  {
    // set up suitable quadrature
    const auto quadrature =
      Dune::QuadratureRules<RealNumber, DIM>::rule(gt, qord);

    // extract the quadrature points and quadrature weights
    QuadratureParameters quadratureparameters;
    for(const auto& qterm : quadrature)
    {
      quadratureparameters.points.push_back(qterm.position());
      quadratureparameters.weights.push_back(qterm.weight());
    }

    return quadratureparameters;
  }

} // namespace Assembly
}

#endif

