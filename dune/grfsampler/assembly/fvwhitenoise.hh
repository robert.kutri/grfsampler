#ifndef FVWHITENOISE_HH
#define FVWHITENOISE_HH

#include <vector>
#include <random>
#include <algorithm>
#include <iterator>
#include <functional>

#include <dune/common/unused.hh>

#include <dune/grfsampler/assembly/factorization.hh>
#include <dune/grfsampler/statistics/biasfreeseeds.hh>


namespace GRFS
{
namespace Assembly
{
  template <class GridView, class DomainVector, typename RealNumber>
  class FiniteVolumeWhiteNoise
  {
    using DofContainer = std::vector<RealNumber>;

    using RandomEngine = std::mt19937;

    using Factorization =
      FVMassMatrixFactorization<GridView, DofContainer, RealNumber>;

    using SeedGenerator = Statistics::BiasFreeSeeds;


    class FiniteVolumeWhiteNoiseRealization
    {
    public:

      using Dofs = DofContainer;


      FiniteVolumeWhiteNoiseRealization(const GridView& gv)
      : gv_{ gv }
      , dofs_{}
      {}


      Dofs& dofs()
      {
        return dofs_;
      }


      const Dofs& get_dofs() const
      {
        return dofs_;
      }


      Dofs copy_dofs() const
      {
        return dofs_;
      }


      template <class Element>
      RealNumber evaluate(const Element& e, const DomainVector& x) const
      {
        DUNE_UNUSED_PARAMETER(x);
        const auto& indexset = gv_.indexSet();

        return dofs_[indexset.index(e)];
      }


    private:

      const GridView& gv_;
      DofContainer    dofs_;
    };


  public:

    using Seed = unsigned int;

    using Realization = FiniteVolumeWhiteNoiseRealization;


    FiniteVolumeWhiteNoise(const GridView& gv)
    : gv_{ gv }
    , realization_{ std::make_shared<Realization>(gv) }
    , massmatrixfactorization_(gv_)
    , randomengine_{}
    , seedgenerator_{}
    , standardnormal_(0., 1.)
    {}


    const Realization& get_realization() const
    {
      return *realization_;
    }


    std::shared_ptr<Realization> share_realization()
    {
      return realization_;
    }


    void generate_realization(const Seed& seed)
    {
      standardnormal_.reset();
      randomengine_.seed(seedgenerator_.seed_sequence(seed));

      const auto samplesize = massmatrixfactorization_.number_of_dofs();
      auto stdnorm = std::vector<RealNumber>{};

      std::generate_n(std::back_inserter(stdnorm), samplesize,
                      std::bind(standardnormal_, std::ref(randomengine_)));

      massmatrixfactorization_.apply(stdnorm, realization_->dofs());
    }


    void generate_scaled_realization(const RealNumber& factor,
                                     const Seed& seed)
    {
      standardnormal_.reset();
      randomengine_.seed(seedgenerator_.seed_sequence(seed));

      const auto samplesize = massmatrixfactorization_.number_of_dofs();
      auto scaledstdnorm = std::vector<RealNumber>(samplesize, factor);

      auto gensn = std::bind(standardnormal_, std::ref(randomengine_));
      std::transform(scaledstdnorm.begin(), scaledstdnorm.end(),
                     scaledstdnorm.begin(),
                     [&gensn](RealNumber& factor){ return factor * gensn(); });

      massmatrixfactorization_.apply(scaledstdnorm, realization_->dofs());
    }


 private:

    const GridView&                       gv_;
    std::shared_ptr<Realization>          realization_;
    Factorization                         massmatrixfactorization_;
    RandomEngine                          randomengine_;
    SeedGenerator                         seedgenerator_;
    std::normal_distribution<RealNumber>  standardnormal_;
  };
} // namespace Assembly
} // namespace GRFS

#endif

