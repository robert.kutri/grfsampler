#ifndef DIAGONALMATRIX_HH
#define DIAGONALMATRIX_HH

#include <dune/common/exceptions.hh>

#include <vector>
#include <algorithm>
#include <functional>


namespace GRFS
{
namespace Assembly
{
  template <typename R>
  class DiagonalMatrix
  {
  public:

    using Size = std::size_t;

    using Entry = R;

    using Container = std::vector<Entry>;


    DiagonalMatrix(const Size& size)
    : diagonal_(size, Entry(0.0))
    {}


    Size size() const
    {
      return diagonal_.size();
    }


    Entry get_entry(const Size& index) const
    {
      return diagonal_[index];
    }


    void set_entry(const Size& index, const Entry& value)
    {
      diagonal_[index] = value;
    }


    template <class ResultContainer>
    inline void apply(const std::vector<Entry>& vec, ResultContainer& result)
    const
    {
      if (vec.size() != result.size())
        DUNE_THROW(Dune::Exception, "Vector and Result Container don't have "
                                    "the same size");

      std::transform(diagonal_.cbegin(), diagonal_.cend(), vec.cbegin(),
                     result.begin(), std::multiplies<Entry>());
    }


  private:

    Container diagonal_;
  };
} // namespace Assembly
} // namespace GRFS

#endif

