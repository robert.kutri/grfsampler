#ifndef CHOLESKY_HH
#define CHOLESKY_HH

#include <cstddef>
#include <memory>
#include <cmath>
#include <algorithm>
#include <cassert>

#include <dune/common/unused.hh>
#include <dune/common/exceptions.hh>

#include <dune/grfsampler/assembly/assemblytypes.hh>
#include <dune/grfsampler/assembly/triangularmatrix.hh>


namespace GRFS
{
namespace Assembly
{
  // interface class for the possible elementwise factorizations of the mass
  // matrix
  template <class LocalMatrixFactor>
  class ElementwiseMassMatrixFactorization
  {
  public:

    using Index = typename LocalMatrix::size_type;
    using FactorEntry = typename LocalMatrixFactor::Entry;

    virtual const LocalMatrixFactor& on(const Index& elementidx) const = 0;

    virtual void add_factorization(const LocalMatrix& localmatrix,
                                   const Index& elementidx) = 0;

    virtual ElementwiseMassMatrixFactorization* clone() const = 0;

    virtual ~ElementwiseMassMatrixFactorization() = default;
  };


  // make the class final and hope for devirtualization by the compiler
  // TODO: benchmark different compilers
  // WARNING: assumes s.p.d. matrix as input, no internal checks
  class CholeskyOnAffineGrid final
    : public ElementwiseMassMatrixFactorization<TriangularMatrix<int> >
  {
  public:

    using Factorization =
      ElementwiseMassMatrixFactorization<TriangularMatrix<int> >;
    using Index = typename Factorization::Index;

    CholeskyOnAffineGrid()
      : issetup_{ false }
      , factor_{}
    {}


    virtual const TriangularMatrix<int>& on(const Index& elementidx)
      const override
    {
      DUNE_UNUSED_PARAMETER(elementidx);

      if (issetup_)
        return factor_;
      else
        DUNE_THROW(Dune::InvalidStateException, "Request for factorization "
                   "that was not yet performed.");
    }


    virtual void add_factorization(const LocalMatrix& matrix,
                                   const Index& elementidx) override
    {
      DUNE_UNUSED_PARAMETER(elementidx);

      if (issetup_)
        DUNE_THROW(Dune::InvalidStateException, "Request to add factorization "
                   "to AffineCholeskyFactorization, which already stores a "
                   "factorization.");

      if (matrix.N() == 0)
        DUNE_THROW(Dune::InvalidStateException, "Request for factorization of "
                   "an empty matrix.");

      factor_.set_size(matrix.N());

      // compute the (0, 0) entry of the cholesky factor
      FactorEntry ell00 = std::sqrt(matrix[0][0]);

      // iterate through the matrix rows
      Index ri{ 0 };
      for (auto& row : matrix)
      {
        // iterate through the columns (until the diagonal entry)
        for (auto col = row.begin(); col.index() <= ri; ++col)
        {
          auto ci = col.index();

          if (ci == 0)
            factor_.set_entry(ri, ci, *col / ell00);
          else if (ci == ri)
            factor_.set_entry(ri, ci,
                              std::sqrt(*col - factor_.chol_rowsum(ri, ci)));
          else
            factor_.set_entry(ri, ci, (*col - factor_.chol_rowsum(ri, ci)) /
                              factor_.get_entry(ci, ci));
        }

        ++ri;
      }

      issetup_ = true;

      return;
    }


    virtual CholeskyOnAffineGrid* clone() const override
    {
      return new CholeskyOnAffineGrid(*this);
    }


  private:

    bool issetup_;

    TriangularMatrix<int> factor_;
  };
} // namespace Assembly
} // namespace GRFS

#endif

