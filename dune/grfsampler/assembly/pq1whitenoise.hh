#ifndef PQ1WHITENOISE_HH
#define PQ1WHITENOISE_HH

#include <cstddef>
#include <memory>
#include <random>
#include <cassert>

#include <dune/grfsampler/assembly/assemblytypes.hh>
#include <dune/grfsampler/assembly/cholesky.hh>
#include <dune/grfsampler/statistics/biasfreeseeds.hh>


namespace GRFS
{
namespace Assembly
{
  template <class GridView>
  class PQ1WhiteNoise
  {
    using Factorization =
      ElementwiseMassMatrixFactorization<TriangularMatrix<int> >;

  public:

    using FEBasis = Assembly::PQ1Basis<GridView>;
    using Randomengine = std::mt19937;


    // default constructor
    PQ1WhiteNoise()
      : pq1basis_{ nullptr }
      , cholf_{ nullptr }
      , randomengine_{}
      , standardnormal_(0.0, 1.0)
    {}


    // constructor using specific basis and factorization
    PQ1WhiteNoise(std::shared_ptr<const FEBasis> pq1basis,
                  Factorization* cholf)
      : pq1basis_{ pq1basis }
      , cholf_{ cholf }
      , randomengine_{}
      , biasfreeseeds_{}
      , standardnormal_(0.0, 1.0)
    {}


    // copy constructor
    PQ1WhiteNoise(const PQ1WhiteNoise& whitenoise)
      : pq1basis_{ whitenoise.pq1basis_ }
      , cholf_{ std::unique_ptr<Factorization>(whitenoise.cholf_->clone()) }
      , randomengine_{}
      , biasfreeseeds_{}
      , standardnormal_(0.0, 1.0)
    {}


    // copy assignment
    PQ1WhiteNoise& operator=(const PQ1WhiteNoise& whitenoise)
    {
      if (this == &whitenoise)
        return *this;

      this->pq1basis_ = whitenoise.pq1basis_;
      this->cholf_.reset(whitenoise.cholf_->clone());

      this->randomengine_ = Randomengine{};
      this->standardnormal_.reset();

      return *this;
    }


    FlatDofVector sample_white_noise(const Seed& seed)
    {
      standardnormal_.reset();
      randomengine_.seed(biasfreeseeds_.seed_sequence(seed));

      auto genstdnorm = std::bind(standardnormal_, std::ref(randomengine_));

      // initialize global degrees of freedom vector for the white noise sample
      auto dofvector = FlatDofVector(pq1basis_->size());
      dofvector = 0;

      auto gridview = pq1basis_->gridView();
      auto localview = pq1basis_->localView();
      auto& indexset = gridview.indexSet();

      for (const auto& element : elements(gridview))
      {
        const auto elementindex = indexset.index(element);
        const auto geometry = element.geometry();

        localview.bind(element);
        const auto localdim = localview.tree().size();

        // compiler should be required to perform copy elision here
        auto stdnormvector = LocalVector(localdim);
        std::generate(stdnormvector.begin(), stdnormvector.end(), genstdnorm);

        // compute the local contribution to the right hand side
        const auto localfactor = cholf_->on(elementindex);
        auto localsample = LocalVector(localdim);
        localfactor.mv(stdnormvector, localsample);

        // FIXME: implement  res = a * A * rhs for triangular matrices, i.e.
        //        a method A.amv(rhs, res, a)
        auto jdsqrt = std::sqrt(geometry.integrationElement(geometry.center()));
        for (auto& entry : localsample)
          entry *= jdsqrt;

        // distribute the local contribution to corresponding global entries
        for (std::size_t localidx{0}; localidx < localdim; ++localidx)
          dofvector[localview.index(localidx)] += localsample[localidx];
      }

      return dofvector;
    }


    template <typename ScalingFactor>
    FlatDofVector sample_scaled_white_noise(const ScalingFactor& scaling,
                                            const Seed& seed)
    {
      standardnormal_.reset();

      randomengine_.seed(biasfreeseeds_.seed_sequence(seed));

      auto genstdnorm = std::bind(standardnormal_, std::ref(randomengine_));

      // initialize global degrees of freedom vector for the white noise sample
      auto dofvector = FlatDofVector(pq1basis_->size());
      dofvector = 0;

      auto gridview = pq1basis_->gridView();
      auto localview = pq1basis_->localView();
      auto& indexset = gridview.indexSet();

      for (const auto& element : elements(gridview))
      {
        const auto elementindex = indexset.index(element);
        const auto geometry = element.geometry();

        localview.bind(element);
        const auto localdim = localview.tree().size();

        // compiler should be required to perform copy elision here
        auto stdnormvector = LocalVector(localdim);
        std::generate(stdnormvector.begin(), stdnormvector.end(), genstdnorm);

        // compute the local contribution to the right hand side
        const auto localfactor = cholf_->on(elementindex);
        auto localsample = LocalVector(localdim);
        localfactor.mv(stdnormvector, localsample);

        // FIXME: implement  res = a * A * rhs for triangular matrices, i.e.
        //        a method A.amv(rhs, res, a)
        auto jdsqrt = std::sqrt(geometry.integrationElement(geometry.center()));
        for (auto& entry : localsample)
          entry *= jdsqrt;

        // distribute the local contribution to corresponding global entries
        for (std::size_t localidx{0}; localidx < localdim; ++localidx)
          dofvector[localview.index(localidx)] += localsample[localidx];
      }

      // scale the white noise dofs
      for (auto& entry : dofvector)
        entry *= scaling;

      return dofvector;
    }


  private:

    // -------------------------- MEMBERS -------------------------------------

    std::shared_ptr<const FEBasis> pq1basis_;

    // the local Cholesky factors in the factorization of the mass matrix
    std::unique_ptr<Factorization> cholf_;

    // The PRNG engine
    Randomengine randomengine_;

    // The seed sequence used to initialize the random engine
    Statistics::BiasFreeSeeds biasfreeseeds_;

    // Standard normal distribution
    std::normal_distribution<RealNumber> standardnormal_;
  };
} // namespace Assembly
} // namespace GRFS

#endif

