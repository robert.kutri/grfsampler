#ifndef FACTORIZATION_HH
#define FACTORIZATION_HH

#include <vector>
#include <cmath>

#include <dune/grfsampler/assembly/diagonalmatrix.hh>


namespace GRFS
{
namespace Assembly
{
  template <class GridView, class DofContainer, typename R>
  class FVMassMatrixFactorization
  {
  public:

    using Size = std::size_t;


    FVMassMatrixFactorization(const GridView& gv)
    : massmatrixsqrt_(gv.size(0))
    {
      assemble(gv);
    }


    Size number_of_dofs() const
    {
      return massmatrixsqrt_.size();
    }


    inline void apply(const std::vector<R>& vec, DofContainer& result) const
    {
      result.resize(vec.size());

      massmatrixsqrt_.apply(vec, result);
    }


  private:

    DiagonalMatrix<R> massmatrixsqrt_;


    void assemble(const GridView& gv)
    {
      const auto& indexset = gv.indexSet();
      for (const auto& e : elements(gv))
      {
        const auto eindex = indexset.index(e);
        massmatrixsqrt_.set_entry(eindex, std::sqrt(e.geometry().volume()));
      }
    }
  };
} // namespace Assembly
} // namespace GRFS

#endif

