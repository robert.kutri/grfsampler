#ifndef QUADRATURE_HH
#define QUADRATURE_HH

#include <vector>
#include <memory>
#include <cassert>

#include <dune/common/exceptions.hh>
#include <dune/common/transpose.hh>

#include <dune/grfsampler/assembly/assemblytypes.hh>
#include <dune/grfsampler/assembly/assemblyutility.hh>

#include <dune/grfsampler/utility/fuzzycompare.hh>


namespace GRFS
{
namespace Assembly
{
  template <class LocalBasis, class Quadrature>
  void assemble_local_mass_matrix(LocalMatrix& localmassmatrix,
                                  const LocalBasis& localbasis,
                                  const Quadrature& quadrature)
  {
    using Index = typename LocalMatrix::size_type;

    // resize the local matrix to correct size and initialize with 0.0
    auto localdim = localbasis.size();
    localmassmatrix.resize(localdim, localdim, 0.0);

    for (const auto& qpoint : quadrature)
    {
      // evaluate the shape function bases at the quadrature point
      auto sfevals = std::vector<ShapeFunctionValue>{};
      localbasis.evaluateFunction(qpoint.position(), sfevals);

      // fill the local matrix
      for (Index i{0}; i < localdim; ++i)
        for (Index j{0}; j < localdim; ++j)
          localmassmatrix[i][j] += qpoint.weight() * sfevals[i] * sfevals[j];
    }
  }


  template <class LocalBasis, class Quadrature, class Geometry>
  void add_local_diffusion_matrix(LocalMatrix& localmatrix,
                                  const LocalBasis& localbasis,
                                  const Quadrature& quadrature,
                                  const Geometry& geometry,
                                  const RealNumber& kappa)
  {
    using Index = typename LocalMatrix::size_type;

    auto localdim = localbasis.size();
    for (const auto& qpoint : quadrature)
    {
      const auto qp = qpoint.position();
      const auto qw = qpoint.weight();

      // evaluate the gradient of the shape function bases
      std::vector<JacobianValue> sfgrad;
      localbasis.evaluateJacobian(qp, sfgrad);
      const auto N = sfgrad.size();

      // transform shape function gradients to the grid element
      const auto jacinvtrans = geometry.jacobianInverseTransposed(qp);
      std::vector<JacobianValue> bgrad(N);
      for (Index n{0}; n < N; ++n)
        bgrad[n] = sfgrad[n] * Dune::transpose(jacinvtrans);

      // compute the matrix entries (barring determinant of jacobian)
      for (Index i{0}; i < localdim; ++i)
        for (Index j{0}; j < localdim; ++j)
          localmatrix[i][j] += (bgrad[i] * Dune::transpose(bgrad[j])) * qw /
                               (kappa * kappa);
    }
  }


  template <class GridView>
  std::unique_ptr<AssemblyDistributor<TriangularMatrix<int> > >
  assemble_for_pq1basis_on_affine_grid(
    const PQ1Basis<GridView>& pq1basis,
    const RealNumber& kappa,
    const QuadratureInformation& quadratureinfo)
  {
    using Index = typename PQ1Basis<GridView>::size_type;

    // initialize empty cholesky factorization on affine grid
    auto cholfactor = std::make_unique<CholeskyOnAffineGrid>();

    // set up the construction of the matrix and set up the sparsity pattern
    const auto basissize = pq1basis.size();
    const auto buildmode = SparseMatrix::row_wise;

    auto fematrixuptr =
      std::make_unique<SparseMatrix>(basissize, basissize,
                                     quadratureinfo.nnz, buildmode);
    auto& fematrix = *fematrixuptr;

    set_up_sparsity_pattern(fematrix, quadratureinfo.pattern);

    // FIXME: Are the entries in the matrix zero ?
    //        If not, initialize in set_up_sparsity_pattern

    // construct a quadrature rule of appropriate order
    const auto qorder = DIM * ORD;
    const auto quadrature =
      Dune::QuadratureRules<RealNumber, DIM>::rule(ElementType, qorder);

    // prepare the iteration over the grid
    auto gridview = pq1basis.gridView();
    auto localview = pq1basis.localView();
    auto localmassmatrix = LocalMatrix{};
    bool localmassmatrixassembled(false);

    // iterate over the grid on this level
    for (const auto& element : elements(gridview))
    {
      localview.bind(element);
      const auto geometry = element.geometry();
      const auto& localbasis = localview.tree().finiteElement().localBasis();
      const auto localdim = localbasis.size();

      if (!localmassmatrixassembled)
      {
        assemble_local_mass_matrix(localmassmatrix, localbasis, quadrature);
        cholfactor->add_factorization(localmassmatrix, 0);

        // we have now assembled the local matrix on the reference element. As
        // we deal with an affine grid, each local matrix on the elements is
        // just a scalar multiple of this matrix, so we need not assemble the
        // local matrix again, but only scale it and map to global entries.
        localmassmatrixassembled = true;

        // DEBUG: Is the jacobian determinant really constant ?
        // FIXME: Remove or write own test for that
        auto jdets = std::vector<RealNumber>{};
        for (const auto& qp : quadrature)
          jdets.push_back(geometry.integrationElement(qp.position()));

        // equality comparison on purpose
        for (const auto& jd : jdets)
          assert(std::count_if(jdets.begin(), jdets.end(), [jd](auto j){
            return j != jd;
          }) == 0);
      }
      // initialize local matrix with the already assembled local mass matrix
      auto localmatrix = LocalMatrix(localmassmatrix);

      add_local_diffusion_matrix(localmatrix, localbasis, quadrature, geometry,
                                 kappa);

      // As we deal with an affine transformation, the determinant of the
      // jacobian is constant throughout the element
      const auto jdet = geometry.integrationElement(quadrature[0].position());

      // distribute the local matrix to corresponding global entries
      for (Index ilocal(0); ilocal < localdim; ++ilocal)
      {
        const auto iglobal = localview.index(ilocal);

        for (Index jlocal(0); jlocal < localdim; ++jlocal)
        {
          const auto jglobal = localview.index(jlocal);
          fematrix[iglobal][jglobal] += jdet * localmatrix[ilocal][jlocal];
        }
      }
    }

    return std::make_unique<AssemblyDistributor<TriangularMatrix<int> > >(
      fematrixuptr.release(), cholfactor.release());
  }


  template <class GridView>
  std::unique_ptr<AssemblyDistributor<TriangularMatrix<int> > >
  assemble_for_pq1basis(const PQ1Basis<GridView>& pq1basis,
                        const RealNumber& kappa)
  {
    auto quadinfo = get_quadrature_information(pq1basis);

    if (quadinfo->gridtype == GridConstruction::affine)
      return assemble_for_pq1basis_on_affine_grid(pq1basis, kappa, *quadinfo);
    else
      DUNE_THROW(Dune::NotImplemented, "Currently, only affine grids are "
                 "supported.");
  }
} // namespace Assembly
} // namespace GRFS

#endif

