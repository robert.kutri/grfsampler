#ifndef TRIANGULARMATRIX_HH
#define TRIANGULARMATRIX_HH

#include <vector>
#include <algorithm>
#include <cassert>

#include <dune/grfsampler/assembly/assemblytypes.hh>


namespace GRFS
{
namespace Assembly
{
  template <typename I>
  class TriangularMatrix
  {
  public:

    using Entry = double;
    using Size  = I;
    using Index = I;


    TriangularMatrix()
      : entries_{}
      , rowindices_{}
    {}


    // afterwards matrix has dimensions size x size
    inline void set_size(Size size)
    {
      entries_.clear();
      rowindices_.clear();

      for (Index i{0}; i <= size; ++i)
        rowindices_.push_back(i * (i + 1) / 2);// sum of all integers up to i

      // deletes all previous entries
      if (size > 0)
        entries_ = std::vector<Entry>(rowindices_[size], 0.0);

      rowindices_.shrink_to_fit();
      entries_.shrink_to_fit();

      return;
    }


    inline void set_entry(Index row, Index col, Entry x)
    {
      if (col <= row)
        entries_[rowindices_[row] + col] = x;
      else
      {
        if (x == 0.0) // exact comparison is on purpose
          return;
        else
          DUNE_THROW(Dune::Exception, "Invalid entry indices for triangular"
                     "matrix.");
      }

      return;
    }


    // mainly for testing purposes
    inline Size get_number_of_rows() const
    {
      if (rowindices_.empty())
        return 0;
      else
        return rowindices_.size() - 1;
    }


    // mainly for testing purposes
    inline Size get_number_of_columns() const
    {
      if (rowindices_.empty())
        return 0;
      else
      {
        auto lastentry = rowindices_.rbegin();
        return *lastentry - *(++lastentry);
      }
    }


    // mainly for testing purposes
    inline Size get_number_of_entries() const
    {
      return entries_.size();
    }


    // \sum\limits_{k = 0}^{j-1} \ell_{ik} \ell{jk}, row i, column j
    inline Entry chol_rowsum(Index row, Index col) const
    {
      if (col == 0)
        return Entry{ 0.0 };

      assert(col <= row);

      Entry sum{ 0.0 };
      Index rowibegin{ rowindices_[row] };
      Index rowjbegin{ rowindices_[col] };

      for (Index k{0}; k < col; ++k)
        sum += entries_[rowibegin + k] * entries_[rowjbegin + k];

      return sum;
    }


    inline Entry get_entry(Index row, Index col) const
    {
      if (col <= row)
        return entries_[rowindices_[row] + col];
      else
        return Entry{ 0.0 };
    }


    template <class Vector1, class Vector2>
    inline void mv(const Vector1& vec, Vector2& result) const
    {
      std::fill(result.begin(), result.end(), Entry{ 0.0 });

      int row{ 0 };
      int col{ 0 };
      for (Entry entry : entries_)
      {
        if (col > row)
        {
          ++row;
          col = 0;
        }

        result[row] += entry * vec[col++];
      }

      return;
    }


  private:

    // flat array storing the lower triangular entries
    std::vector<Entry> entries_;

    // array storing the starting indices for each row. Additionally, the last
    // component stores the total number of entries.
    std::vector<Index> rowindices_;
  };
} // namespace Assembly
} // namespace GRFS

#endif

