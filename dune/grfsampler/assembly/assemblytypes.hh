#ifndef ASSEMBLYTYPES_HH
#define ASSEMBLYTYPES_HH

#include <dune/common/dynvector.hh>
#include <dune/common/dynmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/grfsampler/globaltypes.hh>


namespace GRFS
{
namespace Assembly
{
  // types transferred from global types
  using GlobalTypes::DIM;
  using GlobalTypes::ORD;
  using GlobalTypes::Seed;
  using GlobalTypes::ElementType;
  using GlobalTypes::RealNumber;
  using GlobalTypes::SparseMatrix;
  using GlobalTypes::DomainVector;
  using GlobalTypes::FlatDofVector;

  template <class GridView> using PQ1Basis = GlobalTypes::PQ1Basis<GridView>;

  // type aliases used specifically for assembly
  using LocalMatrix = Dune::DynamicMatrix<RealNumber>;
  using LocalVector = Dune::DynamicVector<RealNumber>;
  using ShapeFunctionValue = Dune::FieldVector<RealNumber, 1>;
  using JacobianValue = Dune::FieldMatrix<RealNumber, 1, DIM>;
} // namespace assembly
} // namespace GRFS

#endif

