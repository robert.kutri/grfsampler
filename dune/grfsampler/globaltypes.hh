#ifndef GLOBALTYPES_HH
#define GLOBALTYPES_HH

#include <vector>
#include <array>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/dynvector.hh>
#include <dune/common/dynmatrix.hh>

#include <dune/grid/common/capabilities.hh>
#include <dune/grid/yaspgrid.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>


// global constants and type aliases for the project
namespace GlobalTypes
{
  using RealNumber = double;

  static constexpr unsigned int DIM{ 2 };
  static constexpr unsigned int ORD{ 1 };

  static_assert(DIM == 2, "Only 2 dimensional domains are supported.");

  using Grid = Dune::YaspGrid<DIM,
      Dune::EquidistantCoordinates<RealNumber, DIM> >;

  using GeometryType = Dune::Capabilities::hasSingleGeometryType<Grid>;

  // Mixed grids are not yet supported, so we assert whether the grid uses
  // a single geometry type.
  // FIXME: For support of mixed grids, the definition of the quadrature rules
  //        and the factorization of the matrix have to be done element-wise
  static_assert(GeometryType::v,
                "Mixed grids are not supported. Grid must only use a single "
                "type of reference element.");

  static constexpr Dune::GeometryType ElementType(GeometryType::topologyId,
                                                  DIM);

  template <class GridView>
  using PQ1Basis = Dune::Functions::LagrangeBasis<GridView, ORD, RealNumber>;

  using Seed = unsigned int;
  using Scalar = Dune::FieldVector<RealNumber, 1>;
  using FlatDofVector = Dune::BlockVector<Scalar>;
  using SparseMatrix = Dune::BCRSMatrix<Dune::FieldMatrix<RealNumber, 1, 1> >;
  using DomainVector = Dune::FieldVector<RealNumber, DIM>;
  using CellLayout = std::array<int, DIM>;

  template <class GridView>
  using DiscreteGlobalBasisFunction =
    Dune::Functions::DiscreteGlobalBasisFunction<PQ1Basis<GridView>,
        FlatDofVector>;
} // namespace Types

#endif

