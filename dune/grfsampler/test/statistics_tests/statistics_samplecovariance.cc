#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cstddef>
#include <algorithm>
#include <cmath>
#include <random>
#include <cassert>
#include <memory>
#include <functional>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/statistics/biasfreeseeds.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/assembly/cholesky.hh>


using SampleVector = std::vector<GlobalTypes::RealNumber>;
using Experiment = Statistics::Experiment<SampleVector>;

using Utility::is_equal;


int main(int argc, char** argv)
{
  auto vectorsize = 40;
  auto numsamples = static_cast<std::size_t>(1e6);

  // generate standard normal samples
  auto biasfreeseeds = Statistics::BiasFreeSeeds{};
  auto randomengine = std::mt19937(biasfreeseeds.seed_sequence());

  // define a standard normal distribution
  auto sndist = std::normal_distribution(0.0, 1.0);
  auto snsample = std::bind(sndist, std::ref(randomengine));

  // define a normal distribution with different mean and standard deviation
  auto nddist = std::normal_distribution(-1.5, 2.3);
  auto ndsample = std::bind(nddist, std::ref(randomengine));

  // sample standard normal vectors
  auto snsamples = Experiment(numsamples, SampleVector(vectorsize));
  for (SampleVector& sample : snsamples)
    std::generate(sample.begin(), sample.end(), snsample);

  // sample vectors according to the normal distribution
  auto ndsamples = Experiment(numsamples, SampleVector(vectorsize));
  for (SampleVector& sample : ndsamples)
    std::generate(sample.begin(), sample.end(), ndsample);

  // compute sample covariance using the sample mean
  auto snmean = Statistics::sample_mean(snsamples);
  auto sncov = Statistics::compute_sample_covariance(snsamples, snmean);

  auto ndmean = Statistics::sample_mean(ndsamples);
  auto ndcov = Statistics::compute_sample_covariance(ndsamples, ndmean);

  // compute sample covariance assuming zero mean
  auto zmcov = Statistics::sample_covariance_zero_mean(snsamples);

  auto snvar = sndist.stddev() * sndist.stddev();
  auto ndvar = nddist.stddev() * nddist.stddev();

  auto sntol = 6.0 * std::sqrt(2.0 * snvar / double(numsamples));
  auto ndtol = 6.0 * std::sqrt(2.0 * ndvar / double(numsamples));

  for (int i{0}; i < vectorsize; ++i)
  {
    for (int j{0}; j < vectorsize; ++j)
    {
      if (i == j)
      {
        assert(is_equal(sncov[i][j], snvar, sntol));
        assert(is_equal(zmcov[i][j], snvar, sntol));
        assert(is_equal(ndcov[i][j], ndvar, ndtol));
      }
      else
      {
        assert(is_equal(sncov[i][j], 0.0, sntol));
        assert(is_equal(zmcov[i][j], 0.0, sntol));
        assert(is_equal(ndcov[i][j], 0.0, ndtol));
      }
    }
  }

  /*
   * To achieve a certain covariance matrix C, we factorize C = L*L^T. Then,
   * with a standard normal sample s, the covariance of v = L*s is given by
   *   E(v*v^T) = E(L*s*s^T*L^T) = L * E(s*s^T) * L^T = L*L^T = C
   */

  // define a reference matrix and its factorization
  int refsize = 5;
  // choosing the number of samples too high in this case, the numerical error
  // from the matrix vector multiplication dominates the statistical error
  // of the estimator. This could be remedied by choosing the tolerance as the
  // larger of the two. We are, however more interested in the statistics.
  auto numcosamples = 500000;
  auto refcovmat = GRFS::Assembly::LocalMatrix(refsize, refsize, 0.0);

  refcovmat[0][0]= 5.36471; refcovmat[0][1] = 2.356; refcovmat[0][2] = 3.34567;
  refcovmat[0][3] = 1.2034; refcovmat[0][4] = 0.2361;
  refcovmat[1][0] = 2.356; refcovmat[1][1] = 42.424242;
  refcovmat[1][2] = 3.1346; refcovmat[1][3] = 0.231;
  refcovmat[1][4] = 0.23461;
  refcovmat[2][0] = 3.34567; refcovmat[2][1] = 3.1346;
  refcovmat[2][2] = 12.2356; refcovmat[2][3] = 0.23461;
  refcovmat[2][4] = 1.232;
  refcovmat[3][0] = 1.2034; refcovmat[3][1] = 0.231; refcovmat[3][2] = 0.23461;
  refcovmat[3][3] = 7.2346; refcovmat[3][4] = 2.3346;
  refcovmat[4][0] = 0.2361; refcovmat[4][1] = 0.23461; refcovmat[4][2] = 1.232;
  refcovmat[4][3] = 2.3346; refcovmat[4][4] = 7.25773;

  auto factorization = GRFS::Assembly::CholeskyOnAffineGrid{};
  factorization.add_factorization(refcovmat, 0);
  const auto& cholf = factorization.on(0);

  auto corrsamples = Experiment(numcosamples, SampleVector(refsize));
  for (auto& sample : corrsamples)
    std::generate(sample.begin(), sample.end(), snsample);

  auto lintrans = [&cholf](const SampleVector& sample) -> SampleVector
                  {
                    auto corrsample = SampleVector(sample);
                    cholf.mv(sample, corrsample);

                    return corrsample;
                  };

  std::transform(corrsamples.begin(), corrsamples.end(), corrsamples.begin(),
                 lintrans);


  // sqrt(E(vv^T)) / sqrt(numcosamples)
  auto corrtol = SampleVector(refsize);
  for (int i{0}; i < refsize; ++i)
    corrtol[i] = 7.0 * std::sqrt(2.0 * refcovmat[i][i] / double(numcosamples));

  // compute the covariance matrix of correlated samples using sample mean
  auto comean = Statistics::sample_mean(corrsamples);
  auto cocov = Statistics::compute_sample_covariance(corrsamples, comean);

  // compute the covariance matrix assuming zero mean
  auto zmcocov = Statistics::sample_covariance_zero_mean(corrsamples);

  for (int i{0}; i < refsize; ++i)
  {
    for (int j{0}; j < refsize; ++j)
    {
      if (!is_equal(cocov[i][j], refcovmat[i][j], corrtol[i]))
        std::cout << "COV: " << cocov[i][j] << ", REF: " << refcovmat[i][j]
                  << ", TOL: " << corrtol[i] << std::endl;
      assert(is_equal(cocov[i][j], refcovmat[i][j],  corrtol[i]));
      assert(is_equal(zmcocov[i][j], refcovmat[i][j],  corrtol[i]));
    }
  }

  return 0;
}

