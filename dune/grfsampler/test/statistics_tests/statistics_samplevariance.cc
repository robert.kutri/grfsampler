#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cstddef>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <random>
#include <cassert>
#include <memory>
#include <array>
#include <iterator>
#include <functional>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/statistics/statisticstypes.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/assembly/cholesky.hh>

using GlobalTypes::RealNumber;
using Utility::is_equal;

using Sample = std::vector<GlobalTypes::RealNumber>;
using Experiment = Statistics::Experiment<Sample>;



// throughout the test, statistical quantities are compared to determininstic
// values. The comparisons are chosen such, that the test failing for a correct
// program  corresponds to a 6-sigma event, i.e. an event with probability at
// most 9.87e-10. The case of the methods being just "slightly" off (false
// positives so to say) is (hopefully) covered by choosing the sample sizes
// large enough.
int main(int argc, char** argv)
{
  const auto vectorsize = std::size_t{ 5 };
  const auto numsamples = static_cast<std::size_t>(5e6);

  auto varvalues = std::array<RealNumber, 4>{ 1e-3, 3e-1, 2e0, 4e1 };

  for (const auto& variance : varvalues)
  {
    // generate samples according to variance
    auto biasfreeseeds = std::seed_seq{3, 1, 4, 1, 5};
    auto randomengine = std::mt19937(biasfreeseeds);
    auto normaldist = std::normal_distribution(0.0, std::sqrt(variance));
    auto genrlz = std::bind(normaldist, std::ref(randomengine));

    auto normalsamples = Experiment(numsamples, Sample(vectorsize));
    for (Sample& sample : normalsamples)
      std::generate(sample.begin(), sample.end(), genrlz);

    // compute the moments
    auto smean = Statistics::sample_mean(normalsamples);
    auto svar = Statistics::compute_sample_variance(normalsamples, smean);

    auto svarsd = std::sqrt(variance / double(numsamples));
    auto tolerance = 6.0 * svarsd;

    // assert correctness
    auto refvar = std::vector<RealNumber>(svar.size(), variance);
    assert(Utility::ranges_equal(svar, refvar, tolerance, 1e-8));

    // use estimator assuming zero mean
    auto zmsvar = Statistics::sample_variance_zero_mean(normalsamples);
    assert(Utility::ranges_equal(zmsvar, refvar, tolerance, 1e-8));

    // test the sample variance estimator for a sample with scalar entries
    auto largesample = std::vector<RealNumber>{};
    std::generate_n(std::back_inserter(largesample),
                    numsamples, genrlz);

    const auto largesamplevar =
      Statistics::unbiased_sample_variance(largesample, normaldist.mean());

    assert(Utility::is_equal(largesamplevar, variance, tolerance));

    // check whether all methods yield the same result
    const auto wrappedlargesample =
      std::vector<std::vector<RealNumber> >(1, largesample);
    const auto zmlargesamplevar =
      Statistics::sample_variance_zero_mean(wrappedlargesample);

    assert(is_equal(largesamplevar, zmlargesamplevar.front()));
  }

  return 0;
}

