#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <vector>
#include <cmath>
#include <random>

#include <dune/grfsampler/statistics/momentestimators.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>

using Scalar = double;
using Vector = std::vector<Scalar>;
using Sample = std::vector<Vector>;


int main(int argc, char** argv)
{

  const auto vectorsize = static_cast<std::size_t>(80);
  const auto samplesize = static_cast<std::size_t>(1e6);

  const auto refmeans = std::vector<Scalar>{ 0.0, 1.65, -1.42, 111., 0.01 };
  const auto refvar = 1.0;

  for (const auto& refmean : refmeans)
  {
    // generate standard normal samples
    auto biasfreeseeds = std::seed_seq{1, 2, 3, 5, 7, 11};
    auto randomengine  = std::mt19937(biasfreeseeds);
    auto normaldist    = std::normal_distribution(refmean, refvar);
    auto getnormrv     = std::bind(normaldist, std::ref(randomengine));

    auto sample = Sample(samplesize, Vector(vectorsize));
    for (auto& vec : sample)
      std::generate(vec.begin(), vec.end(), getnormrv);

    auto samplemean = Statistics::sample_mean(sample);

    double tolerance = 5.0 * std::sqrt(refvar / double(samplesize));
    for (const auto& mean : samplemean)
      assert(Utility::is_equal(mean, refmean, tolerance));
  }

  return 0;
}

