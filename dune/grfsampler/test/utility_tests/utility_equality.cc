#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/fvector.hh>

#include <dune/grfsampler/utility/fuzzycompare.hh>

using Utility::is_equal;


int main(int argc, char** argv)
{
  double d{ 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 };
  assert(is_equal(d, 1.0));
  assert(is_equal(d - 1.0, 0.0));
  assert(!is_equal(d, 0.999999));
  assert(!is_equal(d - 0.999999, 0.0));

  float f{ 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f + 0.1f
           + 0.1f };
  assert(is_equal(f, 1.0f));
  assert(is_equal(f - 1.0f, 0.0f));
  assert(!is_equal(f, 0.9999f));
  assert(!is_equal(f - 0.9999f, 0.0f));

  long double ld{ 0.1l + 0.1l + 0.1l + 0.1l + 0.1l + 0.1l + 0.1l + 0.1l + 0.1l
                  + 0.1l };
  assert(is_equal(ld, 1.0l));
  assert(is_equal(ld - 1.0l, 0.0l));
  assert(!is_equal(ld, 0.99999999999l));
  assert(!is_equal(ld - 0.99999999999l, 0.0l));

  Dune::FieldVector<double, 1> fd{d};
  assert(is_equal(fd, 1.0));
  assert(is_equal(fd - 1.0, 0.0));
  assert(!is_equal(fd, 0.999999));
  assert(!is_equal(fd - 0.999999, 0.0));

  Dune::FieldVector<float, 1> ff{f};
  assert(is_equal(ff, 1.0f));
  assert(is_equal(ff - 1.0f, 0.0f));
  assert(!is_equal(ff, 0.9999f));
  assert(!is_equal(ff - 0.9999f, 0.0f));

  Dune::FieldVector<long double, 1> fld{ld};
  assert(is_equal(fld, 1.0l));
  assert(is_equal(fld - 1.0l, 0.0l));
  assert(!is_equal(fld, 0.99999999999l));
  assert(!is_equal(fld - 0.99999999999l, 0.0l));

  // FIXME: add tests for range comparisons

  return 0;
}

