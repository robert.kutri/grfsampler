#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#undef HAVE_MPI

#include <cassert>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/utility/test.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>


using GlobalTypes::RealNumber;


/*
 * do not test assemble_mass_matrix, as it is basically copy paste from the
 * assembly method in assembly/quadrature.hh, which is tested separately
 */
int main(int argc, char** argv)
{
  auto numcells = int{ 100 };
  auto domlength = RealNumber{ 1.0 };
  auto upperright = GlobalTypes::DomainVector{ domlength, domlength };
  auto cells = GlobalTypes::CellLayout{ numcells, numcells };

  auto grid = std::make_unique<GlobalTypes::Grid>(upperright, cells);
  auto gv = grid->levelGridView(grid->maxLevel());

  auto pq1basis = GlobalTypes::PQ1Basis<decltype(gv)>(gv);

  // discretization of the constant-zero right hand side should be the zero
  // vector
  auto zerofcn = [](const auto& x) -> RealNumber{ return 0.0; };
  auto zerorhs = Utility::assemble_rhs_from_analytic(zerofcn, pq1basis);

  for (const auto& entry : zerorhs)
    assert(Utility::is_equal(entry, 0.0));

  // cell centers for constant gridfunctions should also be constant
  auto zerocoeff = GlobalTypes::FlatDofVector(pq1basis.size());
  Dune::Functions::interpolate(pq1basis, zerocoeff, zerofcn);

  auto cnstonefcn = [](const auto& x) -> RealNumber { return 1.0; };
  auto cnstonecoeff = GlobalTypes::FlatDofVector(pq1basis.size());
  Dune::Functions::interpolate(pq1basis, cnstonecoeff, cnstonefcn);

  auto zerogridfcn =
    Dune::Functions::makeDiscreteGlobalBasisFunction<RealNumber>(
      pq1basis, zerocoeff);
  auto cnstonegridfcn =
    Dune::Functions::makeDiscreteGlobalBasisFunction<RealNumber>(
      pq1basis, cnstonecoeff);

  // compute the cell center values
  auto zerocellcenters = Utility::compute_cell_center_values(zerogridfcn);
  auto onecellcenters = Utility::compute_cell_center_values(cnstonegridfcn);

  for (std::size_t i{0}; i < zerocellcenters.size(); ++i)
  {
    assert(Utility::is_equal(zerocellcenters[i], 0.0));
    assert(Utility::is_equal(onecellcenters[i], 1.0));
  }

  return 0;
}

