#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/istl/bcrsmatrix.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>


int main(int argc, char** argv)
{
  int nrows{ 5 };
  int ncols{ 5 };
  int avg{ 3 };
  double oflow{ 0.0 };
  auto bm = GlobalTypes::SparseMatrix::implicit;

  // create symmetric reference matrix
  auto smat = GlobalTypes::SparseMatrix(nrows, ncols, avg, oflow, bm);

  smat.entry(0,0) = 1.0; smat.entry(0, 3) = 1e-8; smat.entry(0,4) = 4.5;
  smat.entry(1,1) = 20.0; smat.entry(1,2) = 4.0 + 1e-9; smat.entry(1,4) = 0.0;
  smat.entry(2,1) = 4.0 + 1e-9; smat.entry(2,2) = 300.0; smat.entry(2,3) = 0.0;
  smat.entry(3,0) = 1e-8; smat.entry(3,2) = 0.0; smat.entry(3,3) = 40.0;
  smat.entry(4,0) = 4.5; smat.entry(4,1) = 0.0; smat.entry(4,4) = 0.5;

  smat.compress();

  if (!Utility::is_symmetric(smat))
    return 1;

  // copy the symmetric matrix and change an entry
  auto asmat = GlobalTypes::SparseMatrix(smat);
  asmat[3][0] = 1e-9;

  if (Utility::is_symmetric(asmat))
    return 1;

  return 0;
}

