#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <vector>
#include <cmath>

#include <dune/grfsampler/assembly/diagonalmatrix.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>

using Utility::is_equal;


int main(int argc, char** argv)
{
  const auto matrixsizes = std::vector<std::size_t>{ 15, 1000, 500000 };

  for (const auto& size : matrixsizes)
  {
    auto testmat = GRFS::Assembly::DiagonalMatrix<double>(size);
    assert(testmat.size() == size);

    auto vec = std::vector<double>(size);
    for (std::size_t n{0}; n < size; ++n)
    {
      testmat.set_entry(n, 1.0);
      vec.at(n) = double(n);
    }

    assert(testmat.size() == size);

    auto resultvec = std::vector<double>{};

    try
    {
      testmat.apply(vec, resultvec);
    }
    catch(Dune::Exception)
    {
      resultvec = std::vector<double>(size);
    }

    assert(testmat.size() == size);
    testmat.apply(vec, resultvec);
    assert(testmat.size() == size);

    for (std::size_t m{0}; m < size; ++m)
    {
      assert(is_equal(vec.at(m), resultvec.at(m)));
      assert(testmat.get_entry(m) == 1.0);
    }

    for (std::size_t n{0}; n < size; ++n)
    {
      testmat.set_entry(n, std::sqrt(vec.at(n)));
      vec[n] = std::sqrt(vec[n]);
    }

    testmat.apply(vec, resultvec);

    for (std::size_t m{0}; m < size; ++m)
    {
      assert(is_equal(testmat.get_entry(m), std::sqrt(double(m))));
      assert(is_equal(resultvec.at(m), double(m)));
    }

    assert(testmat.size() == size);

    return 0;
  }
}

