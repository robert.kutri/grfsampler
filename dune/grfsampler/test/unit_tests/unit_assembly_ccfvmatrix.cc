#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <memory>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/geometry/type.hh>

#include <dune/grfsampler/traits/gridtraits.hh>
#include <dune/grfsampler/traits/adaptertraits.hh>
#include <dune/grfsampler/randomfield/solver.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>
#include <dune/grfsampler/adapter/spdeoperator.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>

#include "headers/assembly_ccfvmatrix.hh"


using R = Test::RF;

using GT =
  GRFS::Traits::GridTraits<GRFS::Traits::Switches::GridLowerLeft::DEFAULT>;

using DomainVector = typename GT::DomainVector;
using CellLayout   = typename GT::CellLayout;
using Grid         = typename GT::Grid;
using GridView     = typename Grid::LeafGridView;

using PTraits =
  GRFS::Traits::PDELabTraits<GridView, R, GT::dimension,
    GRFS::Traits::Switches::Discretization::CCFV>;

template <class ModelProblem>
using AssemblerTraits =
  GRFS::Traits::PDELabAssemblerTraits<ModelProblem, PTraits>;


class TestRealization
{
public:

  TestRealization(const R& sourceval)
  : sourceval_{ sourceval }
  {}


  template <class Element>
  R evaluate(const Element& e, const DomainVector& x) const
  {
    DUNE_UNUSED_PARAMETER(e);
    DUNE_UNUSED_PARAMETER(x);

    return sourceval_;
  }


private:

  R sourceval_;
};



int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  const auto testsourcevals = std::vector<R>{ -1.5, 0.0, 0.6, 25.3 };
  const auto testcorrlengths = std::vector<R>{ 0.06, 0.3 };

  const auto upperright = DomainVector{ 1.5, 1.5 };
  const auto layout     = CellLayout{ {200, 200} };

  const auto grid = Grid(upperright, layout);
  const auto gv = grid.leafGridView();

  for (const auto& sourceval : testsourcevals)
  {
    for (const auto& rho : testcorrlengths)
    {
      // these values are only needed to compute a kappa value
      constexpr R dummyvar = 1.0;
      constexpr R dummynu = 1.0;

      const auto kappa = Statistics::matern_kappa<R>({dummyvar, rho, dummynu});

      // Define the left hand side of the SPDE Sampling problem with constant
      // right hand side
      auto refmodelproblem =
        Test::TestModelProblem<GridView>(kappa, sourceval);


      //-----------------------------------------------------------------------
      /* Use PDELab machinery to obtain a reference solution */
      // ----------------------------------------------------------------------

      const auto refbcadapter = Test::BCA<GridView>(gv, refmodelproblem);

      const auto reffem = Test::FEM(Dune::GeometryTypes::cube(2));
      const auto refgfs = Test::GFS<GridView>(gv, reffem);

      auto refcc = Test::CC<GridView>{};
      Dune::PDELab::constraints(refbcadapter, refgfs, refcc);

      auto reflop = Test::LOP<GridView>(refmodelproblem);

      constexpr int maxnzs = 9;
      auto refgop =
        Test::GOP<GridView>(refgfs, refcc, refgfs, refcc, reflop,
                            Test::MBE(maxnzs));

      auto refsol = Test::DOF<GridView>(refgfs, 0.0);

      auto g = Test::DEA<GridView>(gv, refmodelproblem);
      Dune::PDELab::interpolate(g, refgfs, refsol);

      auto lsb = Test::LSB(100, 3);
      auto slp = Test::SLP<GridView>(refgop, lsb, refsol, 1e-10);

      slp.apply();

      // ----------------------------------------------------------------------

      // define the SPDE sampling problem with the same right hand side
      const auto sourcerlz = std::make_shared<TestRealization>(sourceval);
      auto modelproblem =
        GRFS::Adapter::SPDEOperator<TestRealization, GridView, R>(
          kappa, 1.0, sourcerlz);

      // ----------------------------------------------------------------------
      /* assemble the same system using the types from the trait structs */
      // ----------------------------------------------------------------------

      using ATraits = AssemblerTraits<decltype(modelproblem)>;
      const auto fem = typename PTraits::FiniteVolumeMap(GT::geometrytype);
      const auto gfs = typename PTraits::GridFunctionSpace(gv, fem);

      auto cc = typename PTraits::ConstraintsContainer{};
      auto bcadapter =
        typename ATraits::BoundaryConditionAdapter(gv, modelproblem);
      Dune::PDELab::constraints(bcadapter, gfs, cc, 1);

      using AMBE = typename ATraits::MatrixBackend;
      auto lop = typename ATraits::LocalOperator(modelproblem);
      auto gop =
        typename ATraits::GridOperator(gfs, cc, gfs, cc, lop, AMBE(maxnzs));

      auto sol = typename PTraits::CoefficientVector(gfs, 0.0);

      auto residual = typename PTraits::CoefficientVector(gfs, 0.0);
      gop.residual(sol, residual);

      auto fematrix = typename ATraits::JacobianMatrix(gop);
      gop.jacobian(sol, fematrix);

      using Dune::PDELab::Backend::native;

      assert(Utility::is_symmetric(native(fematrix)));

      auto statistics = Dune::InverseOperatorResult{};
      auto solver =
        Dune::LDL<typename ATraits::NativeMatrix>(native(fematrix));
      solver.setVerbosity(1);

      auto defect = typename PTraits::CoefficientVector(gfs, 0.0);
      solver.apply(native(defect), native(residual), statistics);

      sol -= defect;

      // ----------------------------------------------------------------------

      assert(Utility::ranges_equal(sol, refsol));
    }
  }

  return 0;
}

