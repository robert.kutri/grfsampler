#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <memory>
#include <cassert>
#include <cmath>

#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/assembly/cholesky.hh>


using Factorization =
  GRFS::Assembly::ElementwiseMassMatrixFactorization<
    GRFS::Assembly::TriangularMatrix<int> >;
using CholAffine = GRFS::Assembly::CholeskyOnAffineGrid;


int main (int argc, char** argv)
{
  std::unique_ptr<Factorization> cholaffine(new CholAffine{});

  // calling any method on a factorization that is not yet set up should throw
  // an exception
  try
  {
    cholaffine->on(0);
    DUNE_THROW(Dune::Exception, "Invalid factorization is returned for element"
                                " with index 0.");
  }
  catch (Dune::InvalidStateException& ise)
  {
    cholaffine.reset(new CholAffine{});
  }

  try
  {
    cholaffine->on(1);
    DUNE_THROW(Dune::Exception, "Invalid factorization is returned for element"
                                " with index 1.");
  }
  catch (Dune::InvalidStateException& ise)
  {
    cholaffine.reset(new CholAffine{});
  }

  try
  {
    cholaffine->on(17);
    DUNE_THROW(Dune::Exception, "Invalid factorization is returned for element"
                                " with index 17.");
  }
  catch (Dune::InvalidStateException& ise)
  {
    cholaffine.reset(new CholAffine{});
  }

  // trying to factorize an empty matrix should throw an exception
  try
  {
    auto emptymatrix = GRFS::Assembly::LocalMatrix{};
    cholaffine->add_factorization(emptymatrix, 0);
    DUNE_THROW(Dune::Exception, "Attempting factorization of empty matrix.");
  }
  catch (Dune::InvalidStateException& ise)
  {
    cholaffine.reset(new CholAffine{});
  }

  // adding factorization of the same matrix should fail
  try
  {
    auto matrix = GRFS::Assembly::LocalMatrix(3, 3, 1.0);
    cholaffine->add_factorization(matrix, 0);
    cholaffine->add_factorization(matrix, 0);
    DUNE_THROW(Dune::Exception, "Attempting to factorize the same matrix "
                                "twice.");
  }
  catch (Dune::InvalidStateException& ise)
  {
    cholaffine.reset(new CholAffine{});
  }

  // adding factorization of a second matrix should fail for the affine
  // cholesky factorization
  try
  {
    auto firstmatrix = GRFS::Assembly::LocalMatrix(3, 3, 1.0);
    cholaffine->add_factorization(firstmatrix, 0);
    auto secondmatrix = GRFS::Assembly::LocalMatrix(3, 3, 1.5);
    cholaffine->add_factorization(secondmatrix, 0);
    DUNE_THROW(Dune::Exception, "Attempting to factorize a second matrix for "
                                "a Cholesky factorization on an affine grid.");
  }
  catch (Dune::InvalidStateException& ise)
  {
    cholaffine.reset(new CholAffine{});
  }

  int refmatsize{ 5 };

  // check for a diagonal matrix
  auto diagrefmat = GRFS::Assembly::LocalMatrix(refmatsize, refmatsize, 0.0);

  for (int n{0}; n < refmatsize; ++n)
    diagrefmat[n][n] = static_cast<GlobalTypes::RealNumber>((n+1)*(n+1));

  cholaffine->add_factorization(diagrefmat, 17);

  auto diagcholf = cholaffine->on(17);

  for (int i{0}; i < refmatsize; ++i)
  {
    for (int j{0}; j < refmatsize; ++j)
    {
      if (i == j)
      {
        if (!Utility::is_equal(diagcholf.get_entry(i, j),
                               static_cast<GlobalTypes::RealNumber>(i+1)))
          DUNE_THROW(Dune::Exception, "Cholesky Factorization is not equal to "
                                      "reference factorization.");
      }
      else
      {
        if (!Utility::is_equal(diagcholf.get_entry(i, j), 0.0))
          DUNE_THROW(Dune::Exception, "Cholesky Factorization is not equal to "
                                      "reference factorization.");
      }
    }
  }

  // define a reference matrix and its factorization
  auto refmat = GRFS::Assembly::LocalMatrix(refmatsize, refmatsize, 0.0);

  refmat[0][0]= 5.36471; refmat[0][1] = 2.356; refmat[0][2] = 3.34567;
    refmat[0][3] = 1.2034; refmat[0][4] = 0.2361;
  refmat[1][0] = 2.356; refmat[1][1] = 42.424242; refmat[1][2] = 3.1346;
     refmat[1][3] = 0.231; refmat[1][4] = 0.23461;
  refmat[2][0] = 3.34567; refmat[2][1] = 3.1346; refmat[2][2] = 12.2356;
    refmat[2][3] = 0.23461; refmat[2][4] = 1.232;
  refmat[3][0] = 1.2034; refmat[3][1] = 0.231; refmat[3][2] = 0.23461;
    refmat[3][3] = 7.2346; refmat[3][4] = 2.3346;
  refmat[4][0] = 0.2361; refmat[4][1] = 0.23461; refmat[4][2] = 1.232;
    refmat[4][3] = 2.3346; refmat[4][4] = 7.25773;

  auto refmatcholf = GRFS::Assembly::LocalMatrix(refmatsize, refmatsize, 0.0);

  refmatcholf[0][0] = 2.3161843622648;
  refmatcholf[1][0] = 1.017190185023211; refmatcholf[1][1] = 6.433472322742396;
  refmatcholf[2][0] = 1.444474824417066; refmatcholf[2][1] = 0.258848457341302;
    refmatcholf[2][2] = 3.175230693627995;
  refmatcholf[3][0] = 0.519561404353536;
    refmatcholf[3][1] = -0.046241399061227;
    refmatcholf[3][2] = -0.158701493572232;
    refmatcholf[3][3] = 2.633881454442661;
  refmatcholf[4][0] = 0.101934890782674; refmatcholf[4][1] = 0.02035025924051;
    refmatcholf[4][2] = 0.339972142965904;
    refmatcholf[4][3] = 0.887106620676264;
    refmatcholf[4][4] = 2.51880644960058;

  // compute cholesky factorization and compare
  cholaffine.reset(new CholAffine{});

  cholaffine->add_factorization(refmat, 0);

  auto cholf = cholaffine->on(0);

  for (int i{0}; i < refmatsize; ++i)
    for (int j{0}; j < refmatsize; ++j)
      if (!Utility::is_equal(cholf.get_entry(i, j), refmatcholf[i][j]))
        DUNE_THROW(Dune::Exception, "Cholesky Factorization is not equal to "
                                    "reference factorization.");

  return 0;
}

