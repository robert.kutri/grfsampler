#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <memory>
#include <random>
#include <algorithm>
#include <iterator>
#include <functional>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grfsampler/traits/gridtraits.hh>
#include <dune/grfsampler/assembly/fvwhitenoise.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>


using R = double;

using GT =
  GRFS::Traits::GridTraits<GRFS::Traits::Switches::GridLowerLeft::DEFAULT>;

using GridView = typename GT::Grid::LeafGridView;
using DV       = typename GT::DomainVector;

using FVWhiteNoise = GRFS::Assembly::FiniteVolumeWhiteNoise<GridView, DV, R>;

using Seed   = typename FVWhiteNoise::Seed;
using FVDofs = typename FVWhiteNoise::Realization::Dofs;

using Utility::is_equal;


bool dofs_unequal(const FVDofs& c1, const FVDofs& c2)
{
  return !Utility::ranges_equal(c1, c2);
}


bool duplicate_exists(const std::vector<FVDofs>& samples)
{
  for (const FVDofs& sample : samples)
  {
    auto count = std::count_if(samples.begin(), samples.end(),
                               [&](const FVDofs& c) -> bool
                               {
                                 return Utility::ranges_equal(sample, c);
                               });

    if (count > 1)
      return true;
  }

  return false;
}


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // parameters
  const auto samplesize = 200;
  const auto cellsperdim = 50;
  const auto upperright = typename GT::DomainVector{ 1.0, 1.0 };
  const auto cells = typename GT::CellLayout{ cellsperdim, cellsperdim };

  // instantiate grid and GridView to the only level
  auto grid = typename GT::Grid(upperright, cells);
  const auto gv = grid.leafGridView();

  auto whitenoise = FVWhiteNoise(gv);

  // same seed
  GlobalTypes::Seed seed{ 42 };

  auto cseedsample = std::vector<FVDofs>();
  cseedsample.reserve(samplesize);

  auto cseedgen = [&whitenoise, seed]() -> FVDofs
                      {
                        whitenoise.generate_realization(seed);
                        return whitenoise.get_realization().copy_dofs();
                      };

  std::generate_n(std::back_inserter(cseedsample), samplesize, cseedgen);

  // check whether the same seed results in the same realizations
  auto unequalpos = std::adjacent_find(cseedsample.begin(),
                                       cseedsample.end(),
                                       dofs_unequal);
  if (unequalpos != cseedsample.end())
    DUNE_THROW(Dune::Exception,
               "Same seed results in different realizations.");

  // different seeds
  GlobalTypes::Seed tmpseed{ 1 };

  auto dseedsamples1 = std::vector<FVDofs>();
  auto dseedsamples2 = std::vector<FVDofs>();
  dseedsamples1.reserve(samplesize);
  dseedsamples2.reserve(samplesize);

  auto dseedgen = [&whitenoise, &tmpseed]() -> FVDofs
                  {
                    whitenoise.generate_realization(tmpseed++);
                    return whitenoise.get_realization().copy_dofs();
                  };

  std::generate_n(std::back_inserter(dseedsamples1), samplesize, dseedgen);

  tmpseed = 1;
  std::generate_n(std::back_inserter(dseedsamples2), samplesize, dseedgen);

  // check whether different seeds yield different realizations
  if (duplicate_exists(dseedsamples1))
    DUNE_THROW(Dune::Exception, "Different seeds yield same realizations.");

  // check whether the same sequence of seeds yields the same sequence of
  // realizations
  auto sample1 = dseedsamples1.begin();
  auto sample2 = dseedsamples2.begin();
  auto end1 = dseedsamples1.end();
  auto end2 = dseedsamples2.end();
  auto range_cmp = [](const FVDofs& c1, const FVDofs& c2)
                   {
                     return Utility::ranges_equal(c1, c2, 1e-12, 1e-8);
                   };
  auto mismatchpos = std::mismatch(sample1, end1, sample2, end2, range_cmp);

  if (mismatchpos.first != end1 or mismatchpos.second != end2)
    DUNE_THROW(Dune::Exception, "Same sequence of seeds yields different"
                                " sequence of realizations.");

  // random samples
  auto rd = std::random_device();

  auto rndsample = std::vector<FVDofs>();
  rndsample.reserve(samplesize);

  auto rndgen = [&whitenoise, &rd]() -> FVDofs
                {
                  whitenoise.generate_realization(rd());
                  return whitenoise.get_realization().copy_dofs();
                };

  std::generate_n(std::back_inserter(rndsample), samplesize, rndgen);

  // check whether random seeds yield random realizations (the probability
  // of two samples being equal by chance, is negligible)
  if (duplicate_exists(rndsample))
    DUNE_THROW(Dune::Exception, "Random seeds yield same realizations.");

  return 0;
}

