#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cassert>
#include <vector>
#include <algorithm>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/assembly/triangularmatrix.hh>


using Entry = GRFS::Assembly::TriangularMatrix<int>::Entry;
using Size = GRFS::Assembly::TriangularMatrix<int>::Size;
using Index = GRFS::Assembly::TriangularMatrix<int>::Index;

using Utility::is_equal;
using Utility::ranges_equal;

bool is_zero_matrix(const GRFS::Assembly::TriangularMatrix<int>& trimat)
{
  for (Index i{0}; i < trimat.get_number_of_rows(); ++i)
  {
    for (Index j{0}; j < trimat.get_number_of_columns(); ++j)
    {
      if (!is_equal(trimat.get_entry(i, j), 0.0))
        return false;
    }
  }

  return true;
}


int main(int argc, char** argv)
{
  // construct empty matrix
  GRFS::Assembly::TriangularMatrix<int> trimat;

  assert(trimat.get_number_of_rows() == 0);
  assert(trimat.get_number_of_columns() == 0);
  assert(trimat.get_number_of_entries() == 0);

  // set size to negative number
  trimat.set_size(-1);

  assert(trimat.get_number_of_rows() == 0);
  assert(trimat.get_number_of_columns() == 0);
  assert(trimat.get_number_of_entries() == 0);

  // set size to zero
  trimat.set_size(0);

  assert(trimat.get_number_of_rows() == 0);
  assert(trimat.get_number_of_columns() == 0);
  assert(trimat.get_number_of_entries() == 0);

  // set positive large size
  const Size largesize{ 100 };
  trimat.set_size(largesize);

  assert(trimat.get_number_of_rows() == largesize);
  assert(trimat.get_number_of_columns() == largesize);
  assert(trimat.get_number_of_entries() == largesize * (largesize+1) / 2);

  assert(is_zero_matrix(trimat));

  // set positive smaller size
  const Size smallsize{ 6 };
  trimat.set_size(smallsize);

  assert(is_zero_matrix(trimat));

  // copy the zero matrix
  auto trimatcopy = trimat;

  assert(trimatcopy.get_number_of_rows() == smallsize);
  assert(trimatcopy.get_number_of_columns() == smallsize);
  assert(trimatcopy.get_number_of_entries() == smallsize * (smallsize+1) / 2);
  assert(is_zero_matrix(trimatcopy));

  // define a reference matrix
  GRFS::Assembly::LocalMatrix refmat(smallsize, smallsize, 0.0);

  refmat[0][0] = 1.0;
  refmat[1][0] = 3.14; refmat[1][1] = 0.6;
  refmat[2][0] = 64.8; refmat[2][1] = 2.0; refmat[2][2] = 3.0;
  refmat[3][0] = 0.4; refmat[3][1] = 0.55; refmat[3][2] = 6.0;
    refmat[3][3] = 10.0;
  refmat[4][0] = 2.71; refmat[4][1] = 42.0; refmat[4][2] = 19.0;
    refmat[4][3] = 44.0; refmat[4][4] = 1.5;
  refmat[5][0] = 1.0; refmat[5][1] = 2.0; refmat[5][2] = 3.0;
    refmat[5][3] = 4.0; refmat[5][4] = 5.0; refmat[5][5] = 6.0;

  // populate the triangular matrix using set_entry
  for (Index i{0}; i < smallsize; ++i)
    for (Index j{0}; j < smallsize; ++j)
      trimat.set_entry(i, j, refmat[i][j]);

  // check whether population succeeded using get_entry
  bool equaltorefmat{ true };
  for (Index i{0}; i < smallsize; ++i)
  {
    for (Index j{0}; j < smallsize; ++j)
    {
      if (!is_equal(trimat.get_entry(i, j), refmat[i][j]))
      {
        equaltorefmat = false;
        break;
      }

      if (!equaltorefmat)
        break;
    }
  }

  assert(equaltorefmat);

  // copy and check whether deep copy is made
  trimatcopy = trimat;

  bool copyequaltorefmat{ true };
  for (Index i{0}; i < smallsize; ++i)
  {
    for (Index j{0}; j < smallsize; ++j)
    {
      if (!is_equal(trimat.get_entry(i, j), trimatcopy.get_entry(i, j)))
      {
        copyequaltorefmat = false;
        break;
      }

      if (!copyequaltorefmat)
        break;
    }
  }

  assert(copyequaltorefmat);

  // check whether the copy is not actually a reference
  trimatcopy.set_entry(0, 0, 0.5);
  trimatcopy.set_entry(3, 1, 2.1);

  assert(!is_equal(trimat.get_entry(0, 0), trimatcopy.get_entry(0, 0)));
  assert(!is_equal(trimat.get_entry(3, 1), trimatcopy.get_entry(3, 1)));

  // resizing should yield a zero matrix again
  const Size miscsize{ 10 };
  trimatcopy.set_size(miscsize);

  assert(trimatcopy.get_number_of_rows() == miscsize);
  assert(trimatcopy.get_number_of_columns() == miscsize);
  assert(trimatcopy.get_number_of_entries() == miscsize * (miscsize+1) / 2);
  assert(is_zero_matrix(trimatcopy));

  // check whether cholesky rowsums are correctly computed. This is the
  // naive computation, which differs from the internal computation, so I am
  // not merely reimplementing this computation.
  for (Index i{0}; i < smallsize; ++i)
  {
    for (Index j{0}; j <= i; ++j)
    {
      Entry refcholsum = 0.0;

      for (Index k{0}; k < j; ++k)
        refcholsum += refmat[i][k] * refmat[j][k];

      assert(trimat.chol_rowsum(i, j) == refcholsum);
    }
  }

  // test the matrix vector product against the Types::DynamicDenseMatrix
  // implementation
  const Size numcomps{ smallsize + 5 };
  auto vecs =
    std::vector<std::vector<Entry> >(numcomps,
                                     std::vector<Entry>(smallsize, 0.0));

  // include the unit vectors
  for (Index n{0}; n < smallsize; ++n)
    vecs[n][n] = 1.0;

  // include the constant-one vector
  std::fill(vecs[smallsize].begin(), vecs[smallsize].end(), 1.0);

  // include the vector containing 0, 1, 2, ...
  for (Index n{0}; n < smallsize; ++n)
    vecs[smallsize+1][n] = static_cast<double>(n);

  // include negative indices
  for (Index n{0}; n < smallsize; ++n)
    vecs[smallsize+2][n] = (n % 2 == 0) ? static_cast<double>(n)
                                        : -static_cast<double>(n);

  // include true decimal places
  for (Index n{0}; n < smallsize; ++n)
    vecs[smallsize+3][n] = (n % 3 == 0) ? 0.01 * static_cast<double>(n)
                                        : -0.001 * static_cast<double>(n);

  // perform matrix vector product and compare
  auto result = GRFS::Assembly::LocalVector(smallsize);
  auto refresult = GRFS::Assembly::LocalVector(smallsize);
  for (const auto& testvec : vecs)
  {
    trimat.mv(testvec, result);
    refmat.mv(testvec, refresult);

    assert(ranges_equal(result, refresult));
  }

  return 0;
}

