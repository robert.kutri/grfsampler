#ifndef TESTQUANTITYOFINTEREST_HH
#define TESTQUANTITYOFINTEREST_HH

#include <dune/grfsampler/traits/adaptertraits.hh>
#include <dune/grfsampler/adapter/darcyproblem.hh>
#include <dune/grfsampler/utility/rateestimation.hh>

#include <dune/grfsampler/traits/switches.hh>

namespace Test
{
  struct RFDummy
  {
    struct DummyRealization
    {};

    static constexpr unsigned int dimension = 2;

    static constexpr GRFS::Traits::Switches::Discretization discretization =
      GRFS::Traits::Switches::Discretization::PQ1FEM;
    
    static constexpr int polynomialorder = 1;

    using RealNumber = double;

    using Realization = DummyRealization;

  };


  template <class TargetGridView>
  class DarcyTestProblem :
    public GRFS::Adapter::DarcyUQProblem<RFDummy, TargetGridView>
  {
    using Base =
      GRFS::Adapter::DarcyUQProblem<RFDummy, TargetGridView>;


  public:

    using RangeField = double;


    DarcyTestProblem(const TargetGridView& gv,
                     const typename Base::QoIDomain& qoid)
      : Base(gv, qoid)
    {
      set_solution_to_constant(1.0);
    }


    void set_solution_to_constant(const RangeField& value)
    {
      auto constsol = [value](const auto& x) -> RangeField
                      {
                        return static_cast<RangeField>(value);
                      };

      this->soldofs_.reset(new typename Base::DOF(*(this->gfs_)));
      Dune::PDELab::interpolate(constsol, *(this->gfs_), *(this->soldofs_));

      this->solgvf_.reset(
          new typename Base::GVF(this->gfs_,
                                 std::make_shared<const typename Base::DOF>(
                                   *(this->gfs_), *(this->soldofs_))));
    }



    template <class Element>
    RangeField idomain_characteristic_function(const Element& element) const
    {
      bool isinside = this->inside_qoi_domain(element.geometry().center());

      return ((isinside) ? 1.0 : 0.0);
    }
  };


  template <typename Scalar>
  class TestDomainSetup :
    public Utility::RateEstimationSetup<Scalar>
  {
    using Base = Utility::RateEstimationSetup<Scalar>;


  public:

    using RD = typename Base::GridTraits::RectangularDomain;
    using DV = typename Base::GridTraits::DomainVector;
    using CL = typename Base::GridTraits::CellLayout;


    TestDomainSetup(const RD& dom, const CL& N)
      : Base(dom, N)
    {}


    TestDomainSetup(const DV& L, const CL& N)
      : Base(L, N)
    {}


    RD integration_domain(const int& idhw) const
    {
      return this->parse_qoi_domain(idhw);
    }
  };
} // namspace Test

#endif

