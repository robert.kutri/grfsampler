#ifndef ASSEMBLY_CCFVMATRIX_HH
#define ASSEMBLY_CCFVMATRIX_HH

#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include <dune/pdelab/localoperator/convectiondiffusionccfv.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/constraints/common/constraintsparameters.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/stationary/linearproblem.hh>


namespace Test
{
  using RF = double;

  template <typename GV>
  class TestModelProblem
  {
    typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

  public:

    typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;


    TestModelProblem(const typename Traits::RangeFieldType& k,
                     const typename Traits::RangeFieldType& f)
    : kappasqinv_{ 1.0 / k / k }
    , sourceval_{ f }
    {}


    static constexpr bool permeabilityIsConstantPerCell()
    {
      return true;
    }


    //! tensor diffusion coefficient
    typename Traits::PermTensorType
    A (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      typename Traits::PermTensorType I;
      for (std::size_t i=0; i<Traits::dimDomain; i++)
        for (std::size_t j=0; j<Traits::dimDomain; j++)
          I[i][j] = (i==j) ? kappasqinv_ : 0.;
      return I;
    }


    //! velocity field
    typename Traits::RangeType
    b (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      typename Traits::RangeType v(0.0);
      return v;
    }


    //! sink term
    typename Traits::RangeFieldType
    c (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return 1.0;
    }


    //! source term
    typename Traits::RangeFieldType
    f (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return sourceval_;
    }


    //! boundary condition type function
    BCType
    bctype (const typename Traits::IntersectionType& is,
            const typename Traits::IntersectionDomainType& x) const
    {
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
    }


    //! Dirichlet boundary condition value
    typename Traits::RangeFieldType
    g (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return 0.0;
    }


    //! Neumann boundary condition
    typename Traits::RangeFieldType
    j (const typename Traits::IntersectionType& is,
       const typename Traits::IntersectionDomainType& x) const
    {
      return 0.0;
    }


    //! outflow boundary condition
    typename Traits::RangeFieldType
    o (const typename Traits::IntersectionType& is,
       const typename Traits::IntersectionDomainType& x) const
    {
      return 0.0;
    }


  private:

    const typename Traits::RangeFieldType kappasqinv_;
    const typename Traits::RangeFieldType sourceval_;
  };


  // --------------------------------------------------------------------------
  // type alias needed for the PDELab machinery
  // --------------------------------------------------------------------------

  using DF = double;

  template <class GV>
  using BCA =
    Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<
      TestModelProblem<GV> >;

  template <class GV>
  using DEA =
    Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<
      TestModelProblem<GV> >;

  using FEM = Dune::PDELab::P0LocalFiniteElementMap<DF, RF, 2>;
  using CON = Dune::PDELab::ConformingDirichletConstraints;

  template <class GV>
  using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM, CON>;

  template <class GV>
  using CC  = typename GFS<GV>::template ConstraintsContainer<RF>::Type;

  template <class GV>
  using LOP = Dune::PDELab::ConvectionDiffusionCCFV<TestModelProblem<GV> >;

  using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;

  template <class GV>
  using GOP =
    Dune::PDELab::GridOperator<GFS<GV> , GFS<GV> , LOP<GV>, MBE,
                               RF, RF, RF, CC<GV>, CC<GV> >;

  template <class GV>
  using DOF = Dune::PDELab::Backend::Vector<GFS<GV>, RF>;

  template <class GV>
  using JAC = typename GOP<GV>::Jacobian;

  using LSB = Dune::PDELab::ISTLBackend_SEQ_CG_ILU0;

  template <class GV>
  using SLP =
    Dune::PDELab::StationaryLinearProblemSolver<GOP<GV>, LSB, DOF<GV> >;
} // namespace Test

#endif

