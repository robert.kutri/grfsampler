#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <string>
#include <cassert>
#include <memory>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/pdelab/function/callableadapter.hh>

#include <dune/grfsampler/traits/randomfieldtraits.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>


using SGrid = GRFS::Adapter::SamplingGrid;

using OSGridView  = typename SGrid::OversamplingLeafGridView;
using TGridView   = typename SGrid::TargetLeafGridView;
using Domain      = typename SGrid::RectangularDomain;
using CellLayout  = typename SGrid::CellLayout;

using RandomField = GRFS::PQ1GaussianRandomField<OSGridView>;

using R = typename RandomField::RealNumber;

using Sample = std::vector<std::vector<R> >;


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // define reference marginal variances
  const auto refmargvars = std::vector<R>{ 0.5, 34.3 };

  for (const auto& refmv : refmargvars)
  {
    const auto param = Statistics::MaternParameters<R>{ refmv, 0.25, 1.0 };

    // target domain
    const auto tdom = Domain{ { {0., 0.}, {1.5, 1.5} } };

    // cell layout in target grid
    const auto layout = CellLayout{ 125, 125 };

    // minimal layer width for the oversampling domain
    const auto mlw = 1.0 * param.corrlength;

    // define target grid embedded into an oversampling grid
    auto sgrid = SGrid(tdom, layout, mlw);

    const auto samplesize = 1500;

    // set up the random field sampler
    const auto osgv = sgrid.oversampling_leaf_grid_view();
    auto rf = RandomField(sgrid, osgv, param);

    // sample values of random field realizations at the cell centers of the
    // target grid
    const auto tgv = sgrid.target_leaf_grid_view();
    auto ccsample = Sample{};
    for (int n{0}; n < samplesize; ++n)
    {
      // use loop counter as seed for a new random field realization
      const auto seed = static_cast<decltype(rf)::Seed>(n);
      rf.generate_realization(seed);
      const auto& rfrlz = rf.get_realization();

      // initialize the vector containing the cell center values for this entry
      ccsample.emplace_back();
      auto& ccvals = ccsample.at(n);

      // add the cell center values
      for (const auto& element : elements(tgv))
        ccvals.push_back(rfrlz.evaluate(element, element.geometry().center()));
    }

    // compute marginal variance (zero mean is asserted in another test)
    const auto margvar = Statistics::sample_variance_zero_mean(ccsample);

    // assert correctness (slightly higher tolerance than usual, because of the
    // intrinsic discretization error regarding the marginal variance)
    const auto tolerance = 10.0 * std::sqrt(2.0 * refmv / double(samplesize));
    for (const auto& mv : margvar)
    {
      if (!Utility::is_equal(mv, refmv, tolerance))
        std::cout << "MV: " << mv << ", REF: " << refmv << ", TOL: "
                  << tolerance << std::endl;

      assert(Utility::is_equal(mv, refmv, tolerance));
    }
  }

  return 0;
}

