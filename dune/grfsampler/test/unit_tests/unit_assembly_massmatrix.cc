#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

// Ugly, but the only way to ensure sequential execution of the program.
// Dune::FakeMPIHelper does not work as intended for Dune::YaspGrid.
#undef HAVE_MPI

#include <dune/common/timer.hh>
#include <dune/common/exceptions.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/assembly/assemblyutility.hh>
#include <dune/grfsampler/assembly/quadrature.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/utility/test.hh>


using GlobalTypes::RealNumber;

using Index = GlobalTypes::SparseMatrix::size_type;
using PQ1Basis = GlobalTypes::PQ1Basis<GlobalTypes::Grid::LevelGridView>;


template<class Basis>
RealNumber integrate_product(Index i, Index j, Basis& pq1basis, int qord)
{
  // define the coefficient vectors
  std::vector<RealNumber> ci(pq1basis.size(), 0.0);
  std::vector<RealNumber> cj(pq1basis.size(), 0.0);

  ci[i] = 1.0;
  cj[j] = 1.0;

  // make GridViewFunctions out of the coefficients
  auto bi =
    Dune::Functions::makeDiscreteGlobalBasisFunction<RealNumber>(
        pq1basis, ci
      );
  auto bj =
    Dune::Functions::makeDiscreteGlobalBasisFunction<RealNumber>(
        pq1basis, cj
      );

  RealNumber integral{ 0.0 };

  auto floci = localFunction(bi);
  auto flocj = localFunction(bj);

  auto gridview = pq1basis.gridView();

  // loop over the elements
  for (const auto& e : elements(gridview))
  { 
    auto geometry = e.geometry();

    floci.bind(e);
    flocj.bind(e);

    const auto& quad =
      Dune::QuadratureRules<RealNumber, GlobalTypes::DIM>::rule(e.type(),qord);

    // loop over the quadrature points
    for (const auto& qpoint : quad)
    {
      auto qpos = qpoint.position();

      auto jacdet = geometry.integrationElement(qpos);

      integral += floci(qpos) * flocj(qpos) * qpoint.weight() * jacdet;
    }

    floci.unbind();
    flocj.unbind();
  }

  return integral;
}

    
int main(int argc, char** argv)
{
  auto timer = Dune::Timer();

  // grid parameters
  const auto numcellsx = int{ 12 };
  const auto numcellsy = int{ 10 };
  const auto upperright = GlobalTypes::DomainVector{ 2.0 , 1.0 };
  const auto cells = GlobalTypes::CellLayout{ numcellsx, numcellsy };

  auto grid = std::make_unique<GlobalTypes::Grid>(upperright, cells);
  auto gv = grid->levelGridView(grid->maxLevel());

  auto pq1basis = PQ1Basis(gv);
  int quadorder{ GlobalTypes::ORD * GlobalTypes::DIM };

  std::cout << "SETUP: " << timer.elapsed() << "s" << std::endl;
  timer.reset();

  auto quadinfo = GRFS::Assembly::get_quadrature_information(pq1basis);
  auto massmatrix = Utility::assemble_mass_matrix(pq1basis, *quadinfo);
  assert(massmatrix.N() == massmatrix.M() && "Mass matrix not square.");

  std::cout << "ASSEMBLY: " << timer.elapsed() << "s" << std::endl;
  timer.reset();

  if (!Utility::is_symmetric(massmatrix))
    DUNE_THROW(Dune::Exception, "Mass matrix is not symmetric.");

  std::cout << "SYMMETRY TEST: " << timer.elapsed() << "s" << std::endl;
  timer.reset();

  auto nbasis = pq1basis.size();

  // by now, symmetry of the mass matrix can be assumed, so it suffices to
  // loop through the lower triangle (including diagonals)
  for (Index i{0}; i < nbasis; ++i)
  {
    for (Index j{0}; j <= i; ++j)
    {
      // compute the integral of the product of the basis functions
      auto refentry = integrate_product(i, j, pq1basis, quadorder);

      // compare entries
      if (Utility::is_equal(refentry, 0.0))
      {
        if (massmatrix.exists(i, j))
        {
          auto matentry = static_cast<RealNumber>((massmatrix)[i][j]);
          if (!Utility::is_equal(matentry, 0.0))
            DUNE_THROW(Dune::Exception,
                       "Mass matrix entry is not zero when it should be.");
        }
      }
      else
      {
        // we need to initialize the matrix entry here, because outside this
        // block its existence is not guaranteed
        auto matentry = static_cast<RealNumber>((massmatrix)[i][j]);
        if (!Utility::is_equal(refentry, matentry))
          DUNE_THROW(Dune::Exception,
                     "Mass matrix differs from reference matrix.");
      }
    }
  }

  std::cout << "ENTRY TEST: " << timer.elapsed() << "s" << std::endl;

  return 0;
}

