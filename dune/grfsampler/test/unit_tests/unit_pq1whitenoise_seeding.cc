#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <memory>
#include <random>
#include <algorithm>
#include <iterator>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/assembly/quadrature.hh>
#include <dune/grfsampler/assembly/pq1whitenoise.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>

using GlobalTypes::FlatDofVector;
using PQ1Basis = GlobalTypes::PQ1Basis<GlobalTypes::Grid::LevelGridView>;


bool dofs_unequal(const FlatDofVector& c1, const FlatDofVector& c2)
{
  return !Utility::ranges_equal(c1, c2);
}


bool duplicate_exists(const std::vector<FlatDofVector>& samples)
{
  for (const FlatDofVector& sample : samples)
  {
    auto count = std::count_if(samples.begin(), samples.end(),
                               [&](const FlatDofVector& c) -> bool
                               {
                                 return Utility::ranges_equal(sample, c);
                               });

    if (count > 1)
      return true;
  }

  return false;
}


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // grid parameters
  const auto ncells = 50;
  const auto upperright = GlobalTypes::DomainVector{ 1.0, 1.0 };
  const auto cells = GlobalTypes::CellLayout{ ncells, ncells };

  // instantiate grid and GridView to the only level
  auto grid = std::make_unique<GlobalTypes::Grid>(upperright, cells);
  auto gv = grid->levelGridView(grid->maxLevel());

  auto pq1basis = std::make_shared<const PQ1Basis>(gv);

  auto kappa = GlobalTypes::RealNumber{ 1.0 };
  auto assembly = GRFS::Assembly::assemble_for_pq1basis(*pq1basis, kappa);
  auto whitenoise = GRFS::Assembly::PQ1WhiteNoise(
      pq1basis, assembly->release_factorization());

  auto samplesize = 200;

  // same seed
  auto constseedsamples = std::vector<FlatDofVector>();
  constseedsamples.reserve(samplesize);
  GlobalTypes::Seed seed{ 42 };

  auto constseedsampler = [&whitenoise, seed]()
                          {
                            return whitenoise.sample_white_noise(seed);
                          };

  std::generate_n(std::back_inserter(constseedsamples),
                  samplesize,
                  constseedsampler);

  // check whether the same seed results in the same realizations
  auto unequalpos = std::adjacent_find(constseedsamples.begin(),
                                       constseedsamples.end(),
                                       dofs_unequal);
  if (unequalpos != constseedsamples.end())
    DUNE_THROW(Dune::Exception,
               "Same seed results in different realizations.");

  // different seeds
  auto diffseedsamples1 = std::vector<FlatDofVector>();
  auto diffseedsamples2 = std::vector<FlatDofVector>();
  diffseedsamples1.reserve(samplesize);
  diffseedsamples2.reserve(samplesize);
  GlobalTypes::Seed tmpseed{ 1 };

  auto diffseedsampler = [&whitenoise, &tmpseed]()
                         {
                           return whitenoise.sample_white_noise(tmpseed++);
                         };

  std::generate_n(std::back_inserter(diffseedsamples1),
                  samplesize,
                  diffseedsampler);

  tmpseed = 1;
  std::generate_n(std::back_inserter(diffseedsamples2),
                  samplesize,
                  diffseedsampler);

  // check whether different seeds yield different realizations
  if (duplicate_exists(diffseedsamples1))
    DUNE_THROW(Dune::Exception, "Different seeds yield same realizations.");

  // check whether the same sequence of seeds yields the same sequence of
  // realizations
  auto sample1 = diffseedsamples1.begin();
  auto sample2 = diffseedsamples2.begin();
  auto end1 = diffseedsamples1.end();
  auto end2 = diffseedsamples2.end();
  auto range_cmp = [](const FlatDofVector& c1, const FlatDofVector& c2)
                   {
                     return Utility::ranges_equal(c1, c2, 1e-12, 1e-8);
                   };
  auto mismatchpos = std::mismatch(sample1, end1, sample2, end2, range_cmp);

  if (mismatchpos.first != end1 or mismatchpos.second != end2)
    DUNE_THROW(Dune::Exception, "Same sequence of seeds yields different"
                                " sequence of realizations.");

  // random samples
  auto randomsamples = std::vector<FlatDofVector>();
  randomsamples.reserve(samplesize);
  auto rd = std::random_device();

  auto randomsampler = [&whitenoise, &rd]()
                       {
                         return whitenoise.sample_white_noise(rd());
                       };

  std::generate_n(std::back_inserter(randomsamples),
                  samplesize,
                  randomsampler);

  // check whether random seeds yield random realizations (the probability
  // of two samples being equal by chance, is negligible)
  if (duplicate_exists(randomsamples))
    DUNE_THROW(Dune::Exception, "Random seeds yield same realizations.");

  return 0;
}

