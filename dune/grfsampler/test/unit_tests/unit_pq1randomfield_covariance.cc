#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cmath>
#include <map>
#include <vector>
#include <iterator>
#include <algorithm>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>
#include <dune/grfsampler/statistics/covariancefunction.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>
#include <dune/grfsampler/utility/test.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>

using Statistics::ValueArray;

using SGrid = GRFS::Adapter::SamplingGrid;

using OSGridView   = typename SGrid::OversamplingLeafGridView;
using TGridView    = typename SGrid::TargetLeafGridView;
using Domain       = typename SGrid::RectangularDomain;
using CellLayout   = typename SGrid::CellLayout;
using DomainVector = typename SGrid::DomainVector;

using RandomField = GRFS::PQ1GaussianRandomField<OSGridView>;
using R           = typename RandomField::RealNumber;


int main(int argc, char** argv)
{
  // instantiate fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // define all relevant parameters
  const auto numsamples    = 600;
  const auto cellsperdim   = 60;
  const auto maternparam   = Statistics::MaternParameters<R>{ 0.1, 0.1, 1. };
  const auto minlayerwidth = 1.0 * maternparam.corrlength;
  const auto targetdom     = Domain{ { {0., 0.}, {1., 1.} } };
  const auto layout        = CellLayout{ cellsperdim, cellsperdim };

  // instantiate oversampling and target grids
  auto sgrid = SGrid(targetdom, layout, minlayerwidth);
  const auto ogv = sgrid.oversampling_leaf_grid_view();

  // set up random field sampler
  auto rf = RandomField(sgrid, ogv, maternparam);

  // define the grid view we want to investigate the random field on
  const auto tgv = sgrid.target_leaf_grid_view();

  // sample the random field values at the cell centers
  auto ccsample = Statistics::Experiment<ValueArray>{};
  for (int n{0}; n < numsamples; ++n)
  {
    // use loop counter as seed for a new random field realization
    const auto seed = static_cast<decltype(rf)::Seed>(n);
    rf.generate_realization(seed);

    ccsample.push_back(Utility::cell_center_values(tgv, rf.get_realization()));
  }

  // compute the covariance matrix for the sample
  const auto covmat = Statistics::sample_covariance_zero_mean(ccsample);

  // compute the distances of the cell centers from one another
  const auto distmat =
    Statistics::distance_matrix<TGridView, DomainVector>(tgv);

  // array storing the unique occurring distance values
  auto rval = ValueArray{};

  // {int:vector<R>} map storing the covariance values for the
  // distance value at the corresponding index in rvals
  auto covmapping = std::map<std::size_t, ValueArray >{};

  // Classify the field values according to their distance
  constexpr double binsize = 1e-12;
  Statistics::map_distance_to_covariance(covmapping, rval, covmat, distmat,
                                         binsize);

  // Compute the mean covariance as a function of r := || x - y ||_2
  auto covfcn = Statistics::compute_stationary_isotropic_covariance_function(
      covmapping);
  auto materncov = Statistics::matern_covariance(rval, maternparam);

  // tolerance is 4.0 times the standard deviation of the sample marginal
  // variance estimate
  auto tolerance = ValueArray{};
  for (std::size_t n{0}; n < covmat.size(); ++n)
    tolerance.push_back(5.0 * std::sqrt(covmat[n][n] / double(numsamples)));

  // Check for stationarity and isotropy
  for (std::size_t i{0}; i < covmat.size(); ++i)
  {
    for (std::size_t j{0}; j <= i; ++j)
    {
      auto r = distmat.get_entry(i, j);

      auto equaltorfcn = [r](const R& distance) -> bool
                         {
                           return std::abs(r - distance) < 1e-2;
                         };
      auto rposit = std::find_if(rval.begin(), rval.end(), equaltorfcn);

      assert(rposit != rval.end());
      auto rpos = rposit - rval.begin();

      assert(Utility::is_equal(covmat[i][j], covfcn[rpos], tolerance[i]));
    }
  }

  std::cout << "Covariance function is stationary and isotropic" << std::endl;

  // Check against the corresponding Matern covariance function values
  const auto meantol =
    4.0 * std::sqrt(maternparam.variance / double(numsamples));
  for (std::size_t r{0}; r < rval.size(); ++r)
  {
    // convergence is very slow at r = 0, but smaller meshsize too expensive
    const auto tol = (r == 0) ? 2.0 * meantol : meantol;
    assert(Utility::is_equal(covfcn[r], materncov[r], tol));
  }

  return 0;
}

