#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#undef HAVE_MPI

#include <cmath>
#include <array>
#include <iterator>
#include <map>

#include <dune/common/math.hh>

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>
#include <dune/grfsampler/statistics/covariancefunction.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/utility/test.hh>


using GlobalTypes::RealNumber;
using Utility::compute_cell_center_values;
using Experiment = Statistics::Experiment<GlobalTypes::FlatDofVector>;

using Constants = Dune::MathematicalConstants<RealNumber>;


int main(int argc, char** argv)
{
  const auto numcells = int{ 150 };
  const auto upperright = GlobalTypes::DomainVector{ 1.0, 1.0 };
  const auto cells = GlobalTypes::CellLayout{ numcells, numcells };

  auto grid = std::make_unique<GlobalTypes::Grid>(upperright, cells);
  auto gv = grid->levelGridView(grid->maxLevel());

  auto numsamples = int{ 100 };
  /*
  // FIXME: Higher smoothness changes the mean value !
  auto variancevalues = std::array<RealNumber, 1>{ 1.0 };
  auto corrlengthvalues = std::array<RealNumber, 3>{ 1.0, 0.1, 0.01 };
  auto smoothnessvalues = std::array<RealNumber, 1>{ 1.0 }; //, 3.0, 5.0 };


  for (const RealNumber& variance : variancevalues)
  {
    // standard deviation of the Monte Carlo estimate of the mean
    auto meansd = std::sqrt(variance / static_cast<RealNumber>(numsamples));

    for (const RealNumber& corrlength : corrlengthvalues)
    {
      for (const RealNumber& smoothness : smoothnessvalues)
      {
        std::cout << "VARIANCE " << variance << std::endl;
        std::cout << "CORRLENGTH " << corrlength << std::endl;
        std::cout << "SMOOTHNESS " << smoothness << std::endl;
        auto rfparam = GRFS::RFParameters{ variance, corrlength, smoothness };
        auto randomfield = GRFS::PQ1GaussianRandomField(gv, rfparam);

        auto cellcenters = Experiment{};
        cellcenters.reserve(numsamples);

        for (int n{0}; n < numsamples; ++n)
        {
          // we don't care about the specific seed value here
          auto seed = static_cast<decltype(randomfield)::Seed>(n);
          auto sample = randomfield.generate_sample(seed);

          auto samplegridfcn =
            Dune::Functions::makeDiscreteGlobalBasisFunction<RealNumber>(
              randomfield.get_finite_element_basis(), sample);

          cellcenters.push_back(compute_cell_center_values(samplegridfcn));
        }

        auto tolerance = 6.0 * meansd;
        std::cout << "TOLERANCE: " << tolerance << "\n\n" << std::endl;
        assert(has_zero_mean(cellcenters, tolerance));
      }
    }
  }
  */

  // test variance separately at first
  auto dbgparam = Statistics::MaternParameters{ 1.0, 0.05, 1.0 };
  std::cout << "DEBUG VARIANCE: " << dbgparam.variance << std::endl;
  std::cout << "DEBUG CORRLENGTH: " << dbgparam.corrlength << std::endl;
  std::cout << "DEBUG SMOOTHNESS: " << dbgparam.smoothness << std::endl;
  std::cout << "\n" << std::endl;

  auto dbgrandomfield = GRFS::PQ1GaussianRandomField(gv, dbgparam);
  
  // marginal variance that the field is supposed to have 
  auto kappa = RealNumber{ std::sqrt(8.0 * dbgparam.smoothness) / dbgparam.corrlength };
  std::cout << "KAPPA " << kappa << std::endl;

  auto refvar = RealNumber{ 1.0 / (4.0 * Constants::pi() * kappa * kappa) };

  auto ccsamples = Statistics::Experiment<Statistics::ValueArray>{};

  for (int n{0}; n < numsamples; ++n)
  {
    auto seed = static_cast<decltype(dbgrandomfield)::Seed>(n);
    auto sample = dbgrandomfield.generate_sample(seed);

    auto samplegridfcn =
      Dune::Functions::makeDiscreteGlobalBasisFunction<RealNumber>(
          dbgrandomfield.get_finite_element_basis(), sample);

    // FIXME: refactor dof_to_vector function from unit_randomfield_covariance
    //        and use it here
    auto ccdofvec = Utility::compute_cell_center_values(samplegridfcn);
    auto cellcenters = Statistics::ValueArray{}; 
    std::move(ccdofvec.begin(), ccdofvec.end(), std::back_inserter(cellcenters));
    ccsamples.push_back(cellcenters);
  }
  auto layerwidth = 2.0 * dbgparam.corrlength;
  auto lyridx = Statistics::determine_layer_indices(gv, upperright, layerwidth);

  for (auto& sample : ccsamples)
    Statistics::remove_boundary_layer(sample, lyridx);

  auto popmean = Statistics::ValueArray(ccsamples.front().size(), 0.0);
  auto margvar = Statistics::compute_sample_variance(ccsamples, popmean);

  for (const auto& var : margvar)
  {
    auto estimatorsd = std::sqrt(2.0 * var*var / double(numsamples));
    tolerance = 6.0 * estimatorsd;

    if (!Utility::is_equal(var, refvar, tolerance))
    {
      std::cout << "MARGINAL VARIANCE IS " << var << " BUT SHOULD BE "  
                << refvar << "." << std::endl;
      return 1;
    }
  }

  return 0;
}

