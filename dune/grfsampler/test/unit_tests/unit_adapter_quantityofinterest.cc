#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <memory>
#include <cassert>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grfsampler/traits/gridtraits.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>

#include "headers/test_quantityofinterest.hh"

using R = double;

using GT =
  GRFS::Traits::GridTraits<GRFS::Traits::Switches::GridLowerLeft::DEFAULT>;

using CL          = typename GT::CellLayout;
using DV          = typename GT::DomainVector;
using IDomain     = typename GT::RectangularDomain;

using Utility::is_equal;


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // domain extents
  const auto Lsq = DV{ 1.0, 1.0 };
  const auto Lre = DV{ 2.0, 5.0 };

  // initial number of cells per dimension
  const auto initlayout = CL{ 2, 2 };

  // initialize the corresponding grids
  auto sqgrid = std::make_unique<typename GT::Grid>(Lsq, initlayout);
  auto regrid = std::make_unique<typename GT::Grid>(Lre, initlayout);

  // cell layouts with same numbers of cells per dimension
  const auto uniformtestlayouts = std::vector<CL>{
    initlayout, CL{ 4, 4 }, CL{ 8, 8 }, CL{ 16, 16 }, CL{ 32, 32}};

  auto levelidx = 0;
  for (const auto& layout : uniformtestlayouts)
  {
    // sanity check
    assert(sqgrid->levelSize(levelidx)[0] == layout[0]);
    assert(sqgrid->levelSize(levelidx)[1] == layout[1]);
    assert(regrid->levelSize(levelidx)[0] == layout[0]);
    assert(regrid->levelSize(levelidx)[1] == layout[1]);

    // assume same number of cells in both dimensions
    const auto cpdx = layout[0];
    const auto cpdy = layout[1];
    const auto cpdxf = static_cast<R>(cpdx);
    const auto cpdyf = static_cast<R>(cpdy);
    const auto cpdxfh = 0.5 * cpdxf;
    const auto cpdyfh = 0.5 * cpdyf;

    // instantiate test setups
    auto sqsetup = Test::TestDomainSetup<R>(Lsq, layout);
    auto resetup = Test::TestDomainSetup<R>(Lre, layout);

    // test whether invalid domain is reognized
    try { sqsetup.integration_domain(0); } catch (const Dune::Exception& e) {}
    try { resetup.integration_domain(0); } catch (const Dune::Exception& e) {}
    try { sqsetup.integration_domain(cpdx); }
        catch (const Dune::Exception& e) {}
    try { resetup.integration_domain(cpdx); }
        catch (const Dune::Exception& e) {}
    try { resetup.integration_domain(cpdy); }
        catch (const Dune::Exception& e) {}

    // initialize corresponding grid views
    auto sqgv = sqgrid->levelGridView(levelidx);
    auto regv = regrid->levelGridView(levelidx);
    using SQGV = decltype(sqgv);
    using REGV = decltype(regv);

    // set up test problems for each possible domain width
    for (int idhw{1}; idhw < std::min(cpdx, cpdy) / 2; ++idhw)
    {
      const auto idhwf = static_cast<R>(idhw);

      // set up test problems
      auto sqtestproblem = Test::DarcyTestProblem<SQGV>(
          sqgv, sqsetup.integration_domain(idhw));
      auto retestproblem = Test::DarcyTestProblem<REGV>(
          regv, resetup.integration_domain(idhw));

      // the correct domain corners for the square domain
      const auto sllx = (cpdxfh - idhwf) * Lsq[0] / cpdxf;
      const auto slly = sllx;
      const auto surx = (cpdxfh + idhwf) * Lsq[0] / cpdxf;
      const auto sury = surx;

      // test whether the integration domain is correct for square grid
      for (const auto& element : elements(sqgv))
      {
        const auto cfval =
          sqtestproblem.idomain_characteristic_function(element);

        const auto cellcenter = element.geometry().center();
        const auto insideidom = (sllx < cellcenter[0] and cellcenter[0] < surx)
                                and
                                (slly < cellcenter[1] and cellcenter[1] < sury);

        const auto cfref = (insideidom) ? 1.0 : 0.0;

        assert(is_equal(cfval, cfref));
      }

      // the correct domain corners for the rectangular domain
      const auto rllx = (cpdxfh - idhwf) * Lre[0] / cpdxf;
      const auto rlly = (cpdyfh - idhwf) * Lre[1] / cpdyf;
      const auto rurx = (cpdxfh + idhwf) * Lre[0] / cpdxf;
      const auto rury = (cpdyfh + idhwf) * Lre[1] / cpdyf;

      // test whether the integration domain is correct for rectangular grid
      for (const auto& element : elements(regv))
      {
        const auto cfval =
          retestproblem.idomain_characteristic_function(element);

        const auto cellcenter = element.geometry().center();
        const auto insideidom = (rllx < cellcenter[0] and cellcenter[0] < rurx)
                                and
                                (rlly < cellcenter[1] and cellcenter[1] < rury);

        const auto cfref = (insideidom) ? 1.0 : 0.0;

        assert(is_equal(cfval, cfref));
      }

      // assert whether the quantity of interest is computed correctly. The
      // case of the integration domain coinciding with the problem domain
      // is omitted
      if (2 * idhw < std::min(cpdx, cpdy))
      {
        assert(is_equal(sqtestproblem.quantity_of_interest(), 1.0));
        assert(is_equal(retestproblem.quantity_of_interest(), 1.0));

        const auto solvalue = 2.54;
        sqtestproblem.set_solution_to_constant(solvalue);
        retestproblem.set_solution_to_constant(solvalue);

        assert(is_equal(sqtestproblem.quantity_of_interest(), solvalue));
        assert(is_equal(retestproblem.quantity_of_interest(), solvalue));
      }
    }

    // refine the grid and test again
    constexpr int refineonce = 1;
    sqgrid->globalRefine(refineonce);
    regrid->globalRefine(refineonce);

    levelidx++;
  }

  /*
   * Check whether the integration domain on the finest level coincides with
   * the one on the coarsest level that makes sense (i.e. level 1)
   */
  const auto coarselevel = 1;

  // instantiate setups
  const auto sqcoarsesetup =
    Test::TestDomainSetup<R>(Lsq, uniformtestlayouts[coarselevel]);
  const auto recoarsesetup =
    Test::TestDomainSetup<R>(Lre, uniformtestlayouts[coarselevel]);

  // define relevant grid parameters
  const auto cpdx = uniformtestlayouts[coarselevel][0];
  const auto cpdy = uniformtestlayouts[coarselevel][1];
  const auto cpdxf = static_cast<R>(cpdx);
  const auto cpdyf = static_cast<R>(cpdy);
  const auto cpdxfh = 0.5 * cpdxf;
  const auto cpdyfh = 0.5 * cpdyf;
  const auto idhw = 1;
  const auto idhwf = static_cast<R>(idhw);

  // define relevant grid views
  auto sqgv = sqgrid->levelGridView(sqgrid->maxLevel());
  auto regv = regrid->levelGridView(regrid->maxLevel());
  using SQGV = decltype(sqgv);
  using REGV = decltype(regv);

  // set up test problems
  auto sqproblem = Test::DarcyTestProblem<SQGV>(
      sqgv, sqcoarsesetup.integration_domain(idhw));
  auto reproblem = Test::DarcyTestProblem<REGV>(
      regv, recoarsesetup.integration_domain(idhw));

  // the correct domain corners for the square domain
  const auto sllx = (cpdxfh - idhwf) * Lsq[0] / cpdxf;
  const auto slly = sllx;
  const auto surx = (cpdxfh + idhwf) * Lsq[0] / cpdxf;
  const auto sury = surx;


  // test whether the integration domain is correct for square grid
  for (const auto& element : elements(sqgv))
  {
    const auto cfval = sqproblem.idomain_characteristic_function(element);

    const auto cellcenter = element.geometry().center();
    const auto insideidom = (sllx < cellcenter[0] and cellcenter[0] < surx)
			    and
			    (slly < cellcenter[1] and cellcenter[1] < sury);

    const auto cfref = (insideidom) ? 1.0 : 0.0;

    assert(is_equal(cfval, cfref));
  }

  // the correct domain corners for the rectangular domain
  const auto rllx = (cpdxfh - idhwf) * Lre[0] / cpdxf;
  const auto rlly = (cpdyfh - idhwf) * Lre[1] / cpdyf;
  const auto rurx = (cpdxfh + idhwf) * Lre[0] / cpdxf;
  const auto rury = (cpdyfh + idhwf) * Lre[1] / cpdyf;

  // test whether the integration domain is correct for rectangular grid
  for (const auto& element : elements(regv))
  {
    const auto cfval = reproblem.idomain_characteristic_function(element);

    const auto cellcenter = element.geometry().center();
    const auto insideidom = (rllx < cellcenter[0] and cellcenter[0] < rurx)
			    and
			    (rlly < cellcenter[1] and cellcenter[1] < rury);

    const auto cfref = (insideidom) ? 1.0 : 0.0;

    assert(is_equal(cfval, cfref));
  }

  // assert whether the quantity of interest is computed correctly
  assert(is_equal(sqproblem.quantity_of_interest(), 1.0));
  assert(is_equal(reproblem.quantity_of_interest(), 1.0));

  const auto solvalue = 17.466;
  sqproblem.set_solution_to_constant(solvalue);
  reproblem.set_solution_to_constant(solvalue);

  assert(is_equal(sqproblem.quantity_of_interest(), solvalue));
  assert(is_equal(reproblem.quantity_of_interest(), solvalue));

  return 0;
}

