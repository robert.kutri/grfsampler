#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/parallel/mpihelper.hh>

#include <memory>

#include <dune/grfsampler/randomfield/ccfvrandomfield.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>

using Utility::is_equal;

using SGrid = GRFS::Adapter::SamplingGrid;

using Domain     = typename SGrid::RectangularDomain;
using CellLayout = typename SGrid::CellLayout;
using OSGridView = typename SGrid::OversamplingLeafGridView;
using TGridView  = typename SGrid::TargetLeafGridView;

using RandomField = GRFS::CCFVGaussianRandomField<OSGridView>;

using R = typename RandomField::RealNumber;


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // parameters
  const auto cellsperdim   = 100;
  const auto minlayerwidth = 0.1;
  const auto variance      = 1.0;
  const auto corrlength    = 1.0;
  const auto smoothness    = 1.0;
  const auto rfparam       =
    Statistics::MaternParameters<R>{ variance, corrlength, smoothness };
  const auto targetdom     = Domain{ { {0., 0.}, {1., 1.} } };
  const auto layout        = CellLayout{ cellsperdim, cellsperdim };

  auto sgrid = SGrid(targetdom, layout, minlayerwidth);
  const auto gv = sgrid.oversampling_leaf_grid_view();

  auto randomfield = std::make_unique<RandomField>(sgrid, gv, rfparam);

  assert(is_equal(randomfield->get_variance(), rfparam.variance));
  assert(is_equal(randomfield->get_correlation_length(), rfparam.corrlength));
  assert(is_equal(randomfield->get_smoothness(), rfparam.smoothness));

  try
  {
    auto invalidsmoothness = Statistics::MaternParameters<R>{ 1.0, 1.0, 0.5 };
    randomfield->reset_parameters(invalidsmoothness);
  }
  catch (const Dune::InvalidStateException& e)
  {
    randomfield->reset_parameters(rfparam);
  }

  auto highernu1 = Statistics::MaternParameters<R>{
      variance, corrlength, smoothness + 2.0 };
  randomfield.reset(new RandomField(sgrid, gv, highernu1));

  randomfield->reset_parameters(rfparam);
  randomfield->reset_parameters(highernu1);
  assert(is_equal(randomfield->get_variance(), highernu1.variance));
  assert(is_equal(randomfield->get_correlation_length(), highernu1.corrlength));
  assert(is_equal(randomfield->get_smoothness(), highernu1.smoothness));

  auto highernu2 = Statistics::MaternParameters<R>{
      variance, corrlength, smoothness + 4.0 };
  randomfield->reset_parameters(highernu2);
  assert(is_equal(randomfield->get_variance(), highernu2.variance));
  assert(is_equal(randomfield->get_correlation_length(), highernu2.corrlength));
  assert(is_equal(randomfield->get_smoothness(), highernu2.smoothness));

  auto highervar1 = Statistics::MaternParameters<R>{
      2.0 * variance, corrlength, smoothness };
  randomfield.reset(new RandomField(sgrid, gv, highervar1));

  randomfield->reset_parameters(rfparam);
  randomfield->reset_parameters(highervar1);
  assert(is_equal(randomfield->get_variance(), highervar1.variance));
  assert(is_equal(randomfield->get_correlation_length(), highervar1.corrlength));
  assert(is_equal(randomfield->get_smoothness(), highervar1.smoothness));

  auto highervar2 = Statistics::MaternParameters<R>{
      20.0 * variance, corrlength, smoothness };
  randomfield->reset_parameters(highervar2);
  assert(is_equal(randomfield->get_variance(), highervar2.variance));
  assert(is_equal(randomfield->get_correlation_length(), highervar2.corrlength));
  assert(is_equal(randomfield->get_smoothness(), highervar2.smoothness));

  auto lowercorr = Statistics::MaternParameters<R>{
      variance, 0.25 * corrlength, smoothness };
  randomfield.reset(new RandomField(sgrid, gv, lowercorr));

  randomfield->reset_parameters(rfparam);
  randomfield->reset_parameters(lowercorr);
  assert(is_equal(randomfield->get_variance(), lowercorr.variance));
  assert(is_equal(randomfield->get_correlation_length(), lowercorr.corrlength));
  assert(is_equal(randomfield->get_smoothness(), lowercorr.smoothness));

  auto highercorr = Statistics::MaternParameters<R>{
      variance, 25.0 * corrlength, smoothness };
  randomfield->reset_parameters(highercorr);
  assert(is_equal(randomfield->get_variance(), highercorr.variance));
  assert(is_equal(randomfield->get_correlation_length(), highercorr.corrlength));
  assert(is_equal(randomfield->get_smoothness(), highercorr.smoothness));

  return 0;
}

