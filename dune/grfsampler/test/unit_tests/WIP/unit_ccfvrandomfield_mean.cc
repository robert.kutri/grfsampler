#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <string>
#include <cassert>
#include <memory>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/pdelab/function/callableadapter.hh>

#include <dune/grfsampler/traits/randomfieldtraits.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>


using GT    = GRFS::RFTraits<GRFS::GridPosition::GENERIC>;
using SGrid = Adapter::SamplingGrid<GT>;

using OSGridView  = typename SGrid::OversamplingLeafGridView;
using TGridView   = typename SGrid::TargetLeafGridView;
using Domain      = typename SGrid::RectangularDomain;

using R                = typename GT::RealNumber;
using CellLayout       = typename GT::CellLayout;
using MaternParameters = Statistics::MaternParameters<R>;

using RandomField = GRFS::PQ1GaussianRandomField<OSGridView, GT>;
using RFSample    = typename RandomField::RandomFieldSample;
using RFCoeff     = typename RFSample::DofVector;
using Sample      = std::vector<RFCoeff>;


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // define test parameters
  const auto mparam0 = MaternParameters{  0.5, 0.08, 1.0 };
  const auto mparam1 = MaternParameters{  0.5,  0.4, 1.0 };
  const auto mparam2 = MaternParameters{ 10.4, 0.08, 1.0 };
  const auto mparam3 = MaternParameters{ 10.4,  0.4, 1.0 };

  const auto refparams =
    std::vector<MaternParameters>{ mparam0, mparam1, mparam2, mparam3 };

  constexpr int samplesize = 5000;

  for (const auto& param : refparams)
  {
    // target domain
    const auto tdom = Domain{ { {0., 0.}, {2., 2.} } };

    // cell layout in target grid
    const auto layout = CellLayout{ 60, 60 };

    // minimal layer width for the oversampling domain
    const auto mlw = 1.0 * param.corrlength;

    // define target grid embedded into an oversampling grid
    auto sgrid = SGrid(tdom, layout, mlw);

    // set up the random field sampler
    const auto osgv = sgrid.oversampling_leaf_grid_view();
    auto rf = RandomField(sgrid, osgv, param);

    // sample values of random field realizations. As we are using a PQ1 basis,
    // the values of the degrees of freedom directly correspond to the values
    // of the realization at the grid vertices
    auto sample = Sample{};
    for (int n{0}; n < samplesize; ++n)
    {
      // use loop counter as seed for a new random field realization
      const auto seed = static_cast<decltype(rf)::Seed>(n);
      rf.generate_realization(seed);

      // copy the degrees of freedom vector into the sample
      sample.push_back(rf.get_realization().degrees_of_freedom());
    }

    auto mean = RFCoeff(Statistics::sample_mean(sample));

    // assert correctness
    const auto tolerance =
      5.0 * std::sqrt(param.variance / double(samplesize));
    for (const auto& m : mean)
      assert(Utility::is_equal(m, 0.0, tolerance));
  }

  return 0;
}

