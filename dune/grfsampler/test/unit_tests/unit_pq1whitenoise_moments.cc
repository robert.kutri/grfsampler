#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <random>
#include <algorithm>
#include <iterator>
#include <cassert>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/grfsampler/traits/gridtraits.hh>
#include <dune/grfsampler/assembly/quadrature.hh>
#include <dune/grfsampler/assembly/assemblyutility.hh>
#include <dune/grfsampler/assembly/pq1whitenoise.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/utility/test.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>


using GT =
  GRFS::Traits::GridTraits<GRFS::Traits::Switches::GridLowerLeft::DEFAULT>;

using GridView = typename GT::Grid::LeafGridView;

using RandomField = GRFS::PQ1GaussianRandomField<GridView>;

using R         = typename RandomField::RealNumber;
using Seed      = typename RandomField::Seed;
using PQ1Basis  = typename RandomField::Basis;
using DofVector = typename RandomField::Realization::DofVector;

using Utility::is_equal;


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  const auto numtestruns = 3;
  const auto ncells      = std::vector<int>{ 10, 50, 100 };
  const auto samplesizes = std::vector<std::size_t>{ 50000, 500, 40 };
  assert(numtestruns == ncells.size() and numtestruns == samplesizes.size());

  for (unsigned int r{0}; r < ncells.size(); ++r)
  {
    const auto cellsperdim = ncells.at(r);
    const auto samplesize = samplesizes.at(r);

    // grid parameters
    const auto upperright = typename GT::DomainVector{ 1.0, 1.0 };
    const auto cells = typename GT::CellLayout{ cellsperdim, cellsperdim };

    // instantiate grid and GridView to the single level
    auto grid = typename GT::Grid(upperright, cells);
    const auto gv = grid.leafGridView();

    auto pq1basis = std::make_shared<const PQ1Basis>(gv);

    // the kappa value needs to be passed for the assembly. It does not have
    // any effect on the white noise itself
    const auto dummykappa = R(1.0);
    auto assembly =
      GRFS::Assembly::assemble_for_pq1basis(*pq1basis, dummykappa);
    auto whitenoise = GRFS::Assembly::PQ1WhiteNoise(
        pq1basis, assembly->release_factorization());

    auto sample = std::vector<DofVector>{};
    sample.reserve(samplesize);

    // use the loop counter as seed for the white noise samples
    for (std::size_t n{0}; n < samplesize; ++n)
      sample.push_back(whitenoise.sample_white_noise(Seed(n)));

    const auto smean = Statistics::sample_mean(sample);
    const auto covmat = Statistics::sample_covariance_zero_mean(sample);

    // assert zero mean of the white noise
    auto tolerance = std::vector<double>{};
    for (std::size_t n{0}; n < smean.size(); ++n)
    {
      tolerance.push_back(5.0 * std::sqrt(covmat[n][n] / double(samplesize)));
      assert(is_equal(smean[n], 0.0, tolerance.at(n)));
    }

    // assemble mass matrix for reference
    auto quadinfo = GRFS::Assembly::get_quadrature_information(*pq1basis);
    auto massmat = Utility::assemble_mass_matrix(*pq1basis, *quadinfo);

    // sanity checks
    auto numdofs = covmat.size();
    assert((numdofs == massmat.N()) and (numdofs == massmat.M()));
    assert(numdofs == covmat.front().size());

    // for generalized white noise, the covariance of the white noise degrees
    // of freedom vector should equal the mass matrix of the corresponding basis
    for (std::size_t i{0}; i < numdofs; ++i)
    {
      for (std::size_t j{0}; j < numdofs; ++j)
      {
        if (massmat.exists(i, j))
          assert(is_equal(covmat[i][j], massmat[i][j][0][0], tolerance.at(i)));
        else // massmat[i][j] == 0.0
          assert(is_equal(covmat[i][j], 0.0, tolerance.at(i)));
      }
    }
  }

  return 0;
}

