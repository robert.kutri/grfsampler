#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cassert>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grfsampler/traits/randomfieldtraits.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>
#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>

using Utility::is_equal;

using SGrid = GRFS::Adapter::SamplingGrid;

using R  = double;
using RD = typename SGrid::RectangularDomain;
using CL = typename SGrid::CellLayout;
using DV = typename SGrid::DomainVector;


bool inline inside_domain(const DV& x, const RD& domain)
{
  return ((domain[0][0] < x[0]) and (x[0] < domain[1][0])
          and
          (domain[0][1] < x[1]) and (x[1] < domain[1][1]));
}


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);


  // --------------------------------------------------------------------------
  //                      SMALL EXPLICIT EXAMPLE
  // --------------------------------------------------------------------------

  // sampling grid parameters
  const auto maternparam = Statistics::MaternParameters<R>{ 1.0, 0.1, 1.0 };
  const auto unitdomain  = RD{ { {0., 0.}, {1., 1.} } };
  const auto smalllayout = CL{ 4, 4 };
  const auto minlyrwidth = 1.0 * maternparam.corrlength;

  auto smallsgrid = SGrid(unitdomain, smalllayout, minlyrwidth);

  // check domains
  assert(smallsgrid.oversampling_level_size(0)[0] == 6);
  assert(smallsgrid.oversampling_level_size(0)[1] == 6);

  const auto targetdomain = smallsgrid.target_domain();
  assert(is_equal(targetdomain[0][0], unitdomain[0][0]));
  assert(is_equal(targetdomain[0][1], unitdomain[0][1]));
  assert(is_equal(targetdomain[1][0], unitdomain[1][0]));
  assert(is_equal(targetdomain[1][1], unitdomain[1][1]));

  const auto offs = DV{ {0.25, 0.25} };
  const auto refosdomain = RD{ {unitdomain[0] - offs, unitdomain[1] + offs} };
  const auto osdomain = smallsgrid.oversampling_domain();
  assert(is_equal(osdomain[0][0], refosdomain[0][0]));
  assert(is_equal(osdomain[0][1], refosdomain[0][1]));
  assert(is_equal(osdomain[1][0], refosdomain[1][0]));
  assert(is_equal(osdomain[1][1], refosdomain[1][1]));

  // refine and check whether the correct cells are in the corresponding grids
  const auto numrefs = 5;
  for (int r{0}; r < numrefs; ++r)
    smallsgrid.global_refine(1);

  const auto numlvls = numrefs + 1;
  for (int l{0}; l < numlvls; ++l)
  {
    const auto tgv = smallsgrid.target_level_grid_view(l);
    for (const auto& telement : elements(tgv))
    {
      const auto center = telement.geometry().center();
      assert(inside_domain(center, unitdomain));
    }

    const auto ogv = smallsgrid.oversampling_level_grid_view(l);
    for (const auto& oelement : elements(ogv))
    {
      const auto center = oelement.geometry().center();
      assert(inside_domain(center, refosdomain));
    }
  }

  // check if the correct element in the oversampling grid is referenced
  const auto finetgv = smallsgrid.target_level_grid_view(numlvls - 1);
  for (const auto& te : elements(finetgv))
  {
    assert(inside_domain(
             smallsgrid.oversampling_element(te).geometry().center(),
             unitdomain));
  }


  // --------------------------------------------------------------------------
  //                      LARGE GENERIC EXAMPLE
  // --------------------------------------------------------------------------

  // define the grid parameters
  const auto ll       = DV{ -0.5, 0.2 };
  const auto ur       = DV{ 1.3, 2.1 };
  const auto sd       = RD{ ll, ur };
  const auto sdlayout = CL{ 150, 150 };
  const auto mlw = 0.1; // minimal required layer width

  // instantiate oversampling domain
  auto largesgrid = SGrid(sd, sdlayout, mlw);

  // assert that minimal layerwidth is ensured
  const auto losdomain = largesgrid.oversampling_domain();
  assert((losdomain[0][0] < ll[0] - mlw) and (losdomain[0][1] < ur[0] - mlw));
  assert((losdomain[1][0] > ur[0] + mlw) and (losdomain[1][1] > ll[1] + mlw));

  // assert that the target domain is preserved
  const auto ltargetdomain = largesgrid.target_domain();
  assert(is_equal(ltargetdomain[0][0], sd[0][0]) and
         is_equal(ltargetdomain[0][1], sd[0][1]) and
         is_equal(ltargetdomain[1][0], sd[1][0]) and
         is_equal(ltargetdomain[1][1], sd[1][1]));

  // do the same checks as for the small grid, only this time for a leaf grid
  // view
  const auto tleafgv = largesgrid.target_leaf_grid_view();
  for (const auto& telement : elements(tleafgv))
  {
    const auto center = telement.geometry().center();
    assert(inside_domain(center, sd));
  }

  const auto oleafgv = largesgrid.oversampling_leaf_grid_view();
  for (const auto& oelement : elements(oleafgv))
  {
    const auto center = oelement.geometry().center();
    assert(inside_domain(center, losdomain));
  }

  // check if the correct element in the oversampling grid is referenced
  const auto finetleafgv = largesgrid.target_leaf_grid_view();
  for (const auto& te : elements(finetleafgv))
  {
    assert(inside_domain(
             largesgrid.oversampling_element(te).geometry().center(), sd));
  }

  return 0;
}

