#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <random>
#include <algorithm>
#include <iterator>
#include <cassert>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/pdelab/localoperator/l2.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>

#include <dune/grfsampler/traits/gridtraits.hh>
#include <dune/grfsampler/traits/adaptertraits.hh>
#include <dune/grfsampler/assembly/fvwhitenoise.hh>
#include <dune/grfsampler/utility/test.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>

using R = double;

using GT =
  GRFS::Traits::GridTraits<GRFS::Traits::Switches::GridLowerLeft::DEFAULT>;

using DV       = typename GT::DomainVector;
using Grid     = typename GT::Grid;
using GridView = typename Grid::LeafGridView;

using PTraits =
  GRFS::Traits::PDELabTraits<GridView, R, GT::dimension,
    GRFS::Traits::Switches::Discretization::CCFV>;

using FVWhiteNoise = GRFS::Assembly::FiniteVolumeWhiteNoise<GridView, DV, R>;
using Seed         = typename FVWhiteNoise::Seed;
using FVDofs       = typename FVWhiteNoise::Realization::Dofs;

using Utility::is_equal;


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // test parameters
  const auto cellsperdim = 30;
  const auto samplesize  = 50000;

  // grid parameters
  const auto upperright = typename GT::DomainVector{ 1.2, 1.2 };
  const auto cells      = typename GT::CellLayout{ cellsperdim, cellsperdim };

  // instantiate grid and GridView to the single level
  auto grid = typename GT::Grid(upperright, cells);
  const auto gv = grid.leafGridView();

  auto whitenoise = FVWhiteNoise(gv);

  // sample contains realizations of the FV white noise at the cell centers
  auto ccsample = std::vector<std::vector<R> >{};
  ccsample.reserve(samplesize);

  // use the loop counter as seed for the white noise samples
  for (std::size_t n{0}; n < samplesize; ++n)
  {
    whitenoise.generate_realization(Seed(n));
    const auto& wnrealization = whitenoise.get_realization();

    ccsample.push_back(std::vector<R>{});
    auto& ccvals = ccsample.back();

    for (const auto& e : elements(gv))
      ccvals.push_back(wnrealization.evaluate(e, e.geometry().center()));
  }

  const auto smean = Statistics::sample_mean(ccsample);
  const auto covmat = Statistics::sample_covariance_zero_mean(ccsample);

  // assert zero mean of the white noise
  auto tolerance = std::vector<double>{};
  for (std::size_t n{0}; n < smean.size(); ++n)
  {
    tolerance.push_back(5.0 * std::sqrt(covmat[n][n] / double(samplesize)));
    assert(is_equal(smean[n], 0.0, tolerance.at(n)));
  }

  // --------------------------------------------------------------------------
  // assemble mass matrix for reference using PDELab machinery
  // --------------------------------------------------------------------------

  const auto fem = typename PTraits::FiniteElementMap(GT::geometrytype);
  auto gfs = typename PTraits::GridFunctionSpace(gv, fem);

  auto l2lop = Dune::PDELab::L2{};

  constexpr int maxnzs = 9;
  using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  auto l2gop =
    Dune::PDELab::GridOperator<decltype(gfs), decltype(gfs), decltype(l2lop),
                               MBE, R, R, R>
                                 (gfs, gfs, l2lop, MBE(maxnzs));

  auto dummydofs = typename PTraits::CoefficientVector(gfs, 0.0);

  using JAC = typename decltype(l2gop)::Jacobian;
  auto fvmassmat = JAC(l2gop);
  l2gop.jacobian(dummydofs, fvmassmat);

  using Dune::PDELab::Backend::native;
  const auto& natfvmassmat = native(fvmassmat);

  // --------------------------------------------------------------------------

  // sanity checks
  auto numdofs = covmat.size();
  assert(numdofs == covmat.front().size());
  assert(numdofs == fvmassmat.N());
  assert(numdofs == fvmassmat.M());

  // for generalized white noise, the covariance of the white noise degrees
  // of freedom vector should equal the mass matrix of the corresponding basis
  for (std::size_t i{0}; i < numdofs; ++i)
  {
    for (std::size_t j{0}; j < numdofs; ++j)
    {
      if (natfvmassmat.exists(i, j))
      {
        const auto mmentry = static_cast<R>(natfvmassmat[i][j]);
        assert(is_equal(mmentry, covmat[i][j], tolerance.at(i)));
      }
      else
        assert(is_equal(covmat[i][j], 0.0, tolerance.at(i)));
    }
  }

  return 0;
}

