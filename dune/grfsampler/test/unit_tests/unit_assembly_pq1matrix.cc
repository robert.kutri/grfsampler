#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <cstddef>
#include <memory>
#include <cmath>
#include <cassert>
#include <vector>
#include <array>

#include <dune/common/exceptions.hh>
#include <dune/common/math.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/istl/paamg/aggregates.hh>
#include <dune/istl/paamg/matrixhierarchy.hh>
#include <dune/istl/paamg/parameters.hh>
#include <dune/istl/paamg/fastamg.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/interpolate.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/randomfield/parameters.hh>
#include <dune/grfsampler/assembly/quadrature.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/utility/test.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>


using GlobalTypes::RealNumber;
using GlobalTypes::DomainVector;

using DofVector = GlobalTypes::FlatDofVector;
using PQ1Basis = GlobalTypes::PQ1Basis<GlobalTypes::Grid::LevelGridView>;
using Constants = Dune::MathematicalConstants<RealNumber>;

// type aliases used for CG and AMG
using Operator = Dune::MatrixAdapter<GlobalTypes::SparseMatrix,
                                     DofVector,
                                     DofVector>;

using CriterionInterface =
  Dune::Amg::AggregationCriterion<
    Dune::Amg::SymmetricMatrixDependency<GlobalTypes::SparseMatrix,
                                         Dune::Amg::FirstDiagonal>
                                 >;

using CoarseningCriterion = Dune::Amg::CoarsenCriterion<CriterionInterface>;

using AMG = Dune::Amg::FastAMG<Operator, DofVector>;

using MaternParameters = Statistics::MaternParameters<double>;


/** Test for the finite element matrix
 *
 *  Set up the finite element matrices for the system corresponding to
 *
 *  \begin{align*}
 *    - \Delta u + \kappa^2 u &= \eta f \text{in } D = (0,L_x) x (0, L_y) \\
 *    \nabla u \cdot n &= 0 \quad \text{on } \partial D
 *  \end{align*}
 *
 *  for different right-hand sides f and compare to the analytic solutions. The
 *  correctness of the solvers is asserted in dune-istl, so this is a direct
 *  test for the correctness of the finite element matrix assembly.
 */
int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  const auto ncellsx = 50;
  const auto ncellsy = 50; 

  // the domains we test
  const auto testdom = std::vector<std::array<RealNumber, 2> >
  {
    { 8.0, 8.0 }, { 8.0, 8.0 }, { 8.0, 8.0 },
    { 25.0, 4.75 }, { 25.0, 4.75 }, { 25.0, 4.75 }, 
    { 0.38, 2.4 }, { 0.38, 2.4 }, { 0.38, 2.4 }
  };

  // the correlation lengths we test for the corresponding domains
  const auto testcorr = std::vector<RealNumber>
  {
    0.25 * 8.0, 0.025 * 8.0, 0.0025 * 8.0,
    0.25 * 4.75, 0.025 * 4.75, 0.0025 * 4.75,
    0.25 * 0.38, 0.025 * 0.38, 0.0025 * 0.38
  };

  // the variance value used throughout the test
  const auto testvar = 5.0;

  // choose smoothness parameter nu = 1 for the tests
  constexpr RealNumber testnu{ 1.0 };

  auto testparam = std::vector<std::array<RealNumber, 2> >{};
  for (std::size_t t{0}; t < testcorr.size(); ++t)
  {
    const auto mparam = MaternParameters{ testvar, testcorr.at(t), testnu };

    const auto kappa = Statistics::matern_kappa(mparam);
    const auto eta = GRFS::Parameters::spde_eta(kappa, testvar, testnu);

    testparam.push_back({ kappa, eta });
  }

  // run tests
  for (std::size_t tcnt{0}; tcnt < testdom.size(); ++tcnt)
  {
    const auto upperright = DomainVector{ testdom[tcnt][0], testdom[tcnt][1] };
    const auto cells = GlobalTypes::CellLayout{ ncellsx, ncellsy };

    auto grid = std::make_unique<GlobalTypes::Grid>(upperright, cells);
    auto gv = grid->levelGridView(grid->maxLevel());

    auto pq1basis = std::make_shared<PQ1Basis>(gv);

    std::cout << "\n\nDomain: (0.0, " << testdom[tcnt][0] << ") x (0.0, "
              << testdom[tcnt][1] << ")" << ", " << ncellsx << " x " << ncellsy
              << " grid, " << pq1basis->size() << " dofs\n" << std::endl;

    for (const auto& param : testparam)
    {
      const auto kappa = param[0];
      const auto eta = param[1];

      std::cout << "Testing kappa = " << kappa << ", eta = " << eta
                << std::endl;

      auto assembly = GRFS::Assembly::assemble_for_pq1basis(*pq1basis, kappa);
      auto fematrix = std::unique_ptr<GlobalTypes::SparseMatrix>(
        assembly->release_matrix());

      if (!Utility::is_symmetric(*fematrix))
        DUNE_THROW(Dune::Exception, "Finite element matrix is not symmetric.");


      // --------------------------------------------------------------------
      //                       REFERENCE SOLUTIONS
      // --------------------------------------------------------------------

      // define the reference problems and solutions
      auto analyticf0 = [](const DomainVector& x) -> RealNumber
                          {
                            return 0.0;
                          };

      auto analyticf1 = [eta](const DomainVector& x) -> RealNumber
                          {
                            return eta;
                          };

      auto analyticsol0 = [](const DomainVector& x) -> RealNumber
                          {
                            return 0.0;
                          };

      auto analyticsol1 = [kappa, eta](const DomainVector& x) -> RealNumber
                          {
                            return eta;
                          };


      // interpolate the analytic solutions
      std::vector<DofVector> refdofs;
      auto dofs = DofVector{}; // dummy vector, used in interpolation

      Dune::Functions::interpolate(*pq1basis, dofs, analyticsol0);
      refdofs.push_back(dofs); // deep copy

      Dune::Functions::interpolate(*pq1basis, dofs, analyticsol1);
      refdofs.push_back(dofs); // deep copy

      dofs.resize(0);

      std::vector<DofVector> rhs;
      rhs.push_back(
          Utility::assemble_rhs_from_analytic(analyticf0, *pq1basis));
      rhs.push_back(
          Utility::assemble_rhs_from_analytic(analyticf1, *pq1basis));

      // Turn the matrix into a linear operator for CG
      auto op = Operator(*fematrix);

      // Sequential AMG as preconditioner
      auto coarsening = CoarseningCriterion{};
      coarsening.setDefaultValuesIsotropic(GlobalTypes::DIM);
      coarsening.setDebugLevel(0);

      auto amgparams = Dune::Amg::Parameters{};
      amgparams.setDebugLevel(0);

      // set up the actual amg preconditioner
      auto amg = AMG(op, coarsening, amgparams);

      // Preconditioned CG solver
      Dune::GeneralizedPCGSolver<DofVector> amgcg(op, amg, 1e-8, 40, 0);

      // Object storing statistics of the solution process
      Dune::InverseOperatorResult statistics;

      for (std::size_t i{0}; i < rhs.size(); ++i)
      {
        // initial guess
        auto sol = DofVector(pq1basis->size());
        sol = 0.0;

        amgcg.apply(sol, rhs.at(i), statistics);
        assert(statistics.converged);

        if (!Utility::ranges_equal(sol, refdofs.at(i)))
          DUNE_THROW(Dune::Exception, "Computed solution does not equal "
                                       "reference solution.");
      }
    }
  }

  return 0;
}

