#ifndef PARAMETERS_HH
#define PARAMETERS_HH

#include <cmath>

#include <dune/common/math.hh>


namespace GRFS
{
  namespace Parameters
  {
    template <typename Value, int dim = 2>
    Value spde_eta(const Value& kappa, const Value& var, const Value& nu)
    {
      using Constants = Dune::MathematicalConstants<Value>;

      const auto dimf = static_cast<Value>(dim);
      const auto dimfhalf = 0.5 * dimf;

      auto varscaling = std::tgamma(nu) * std::pow(kappa, 2.0 * nu);
      varscaling /= std::tgamma(nu + dimfhalf)
                    * std::pow(4.0 * Constants::pi(), dimfhalf);

      return std::sqrt(var / varscaling);
    }
  } // namespace Parameters

  // TODO: make nu a template parameter and write specializations
} // namespace GRFS

#endif
