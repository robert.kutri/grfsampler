#ifndef CCFVRANDOMFIELD_HH
#define CCFVRANDOMFIELD_HH

#include <memory>
#include <type_traits>

#include <dune/grfsampler/traits/randomfieldtraits.hh>
#include <dune/grfsampler/traits/switches.hh>
#include <dune/grfsampler/randomfield/solver.hh>
#include <dune/grfsampler/randomfield/parameters.hh>
#include <dune/grfsampler/assembly/fvwhitenoise.hh>
#include <dune/grfsampler/traits/adaptertraits.hh>
#include <dune/grfsampler/adapter/spdeoperator.hh>
#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>


namespace GRFS
{
  template <class OSGridView,
            template<class> class DirectSparseSolver = Dune::LDL>
  class CCFVGaussianRandomField
  {
    using SamplingGrid = Adapter::SamplingGrid;

    using RFTraits =
      Traits::RandomFieldTraits<Traits::Switches::Discretization::CCFV>;

    using R = typename RFTraits::RealNumber;

    using WhiteNoise =
      Assembly::FiniteVolumeWhiteNoise<
        OSGridView, typename SamplingGrid::DomainVector, R>;

    using ParameterValue = double;

    using MaternParameters = Statistics::MaternParameters<ParameterValue>;


    using PDELabTraits =
      Traits::PDELabTraits<OSGridView, R, RFTraits::dimension,
                            Traits::Switches::Discretization::CCFV>;

    using RandomFieldDofs = typename PDELabTraits::CoefficientVector;

    using FiniteVolumeMap = typename PDELabTraits::FiniteVolumeMap;

    using GridFunctionSpace = typename PDELabTraits::GridFunctionSpace;

    using GridViewFunction = typename PDELabTraits::GridViewFunction;

    template <class Realization>
    using SPDEOperator = Adapter::SPDEOperator<Realization, OSGridView, R>;

    template <class Realization>
    using PDELabAssemblerTraits =
      Traits::PDELabAssemblerTraits<SPDEOperator<Realization>, PDELabTraits>;

    template <class Realization>
    using Matrix = typename PDELabAssemblerTraits<Realization>::NativeMatrix;

    template <class Realization>
    using Solver = DirectSparseSolver<Matrix<Realization> >;

    using SolutionStatistics = Solver::SolutionStatistics;

    class CCFVRandomFieldRealization
    {
      using OSGVElement = typename OSGridView::template Codim<0>::Entity;


    public:

      using Dofs = RandomFieldDofs;

      using DomainVector = typename SamplingGrid::DomainVector;


      CCFVRandomFieldRealization(const SamplingGrid& sg,
                                 const GridFunctionSpace& gfs)
      : issetup_{ false }
      , sgrid_{ sg }
      , gfs_{ gfs }
      , dofs_(gfs_)
      , rlzgvf_{ nullptr }
      {}


      Dofs& dofs()
      {
        if (issetup_)
          DUNE_THROW(Dune::Exception, "Trying to get non-const reference to "
                     "degrees of freedom after realization has been computed");
        else
          return dofs_;
      }


      const Dofs& get_dofs() const
      {
        return dofs_;
      }


      Dofs copy_dofs() const
      {
        return dofs_;
      }


      void finalize()
      {
        rlzgvf_.reset(new GridViewFunction(gfs_, dofs_));
        issetup_ = true;
      }


      R evaluate(const OSGVElement& e, const DomainVector& x) const
      {
        // FIXME: Remove after debugging and testing
        if (!issetup_)
          DUNE_THROW(Dune::Exception, "Trying to evaluate a random field "
                     "realization, which has not yet been set up.");

        auto localfunction = localFunction(*rlzgvf_);
        localfunction.bind(e);

        return localfunction(x);
      }


      // overload for target grid elements
      template <class TargetGridElement>
      R evaluate(const TargetGridElement& te, const DomainVector& x) const
      {
        // FIXME: Remove after debugging and testing
        if (!issetup_)
          DUNE_THROW(Dune::Exception, "Trying to evaluate a random field "
                     "realization, which has not yet been set up.");

        auto localfunction = localFunction(*rlzgvf_);
        localfunction.bind(sgrid_.oversampling_element(te));

        return localfunction(x);

      }



    private:

      bool                              issetup_;
      const SamplingGrid&               sgrid_;
      const GridFunctionSpace&          gfs_;
      Dofs                              dofs_;
      std::unique_ptr<GridViewFunction> rlzgvf_;
    };


  public:

    static constexpr unsigned int dimension = RFTraits::dimension;
    
    static constexpr unsigned int polynomialorder = RFTraits::order;

    static constexpr auto discretization = RFTraits::discretization;


    using Seed = typename RFTraits::Seed;

    using RealNumber = R;

    using Realization = CCFVRandomFieldRealization;


    struct SPDEParameters
    {
      R kappa;
      R eta;

      int operatorpower;
    };


    CCFVGaussianRandomField(const SamplingGrid&    sgrid,
                            const OSGridView&      osgv,
                            const MaternParameters mparam)
    : osgv_       { osgv    }
    , sgrid_      { sgrid   }
    , mparam_     { mparam  }
    , whitenoise_ (osgv)
    , spdeparam_  {}
    , realization_{ nullptr }
    {
      set_parameters(mparam);

      fem_.reset(new FiniteVolumeMap(SamplingGrid::geometrytype));
      gfs_.reset(new GridFunctionSpace(osgv_, fem_));
    }


    const Realization& get_realization() const
    {
      return *realization_;
    }


    std::shared_ptr<Realization> share_realization()
    {
      return realization_;
    }


    void generate_realization(const Seed& seed)
    {
      // types relevant for the sampling process
      using WNR = typename WhiteNoise::Realization;

      using MBE = typename PDELabAssemblerTraits<WNR>::MatrixBackend;
      using JAC = typename PDELabAssemblerTraits<WNR>::JacobianMatrix;
      using BCA =
        typename PDELabAssemblerTraits<WNR>::BoundaryConditionAdapter;
      using SLV = Solver<WNR>;

      using WNLOP = typename PDELabAssemblerTraits<WNR>::LocalOperator;
      using WNGOP = typename PDELabAssemblerTraits<WNR>::GridOperator;

      using RFR = Realization;
      using RFLOP = typename PDELabAssemblerTraits<RFR>::LocalOperator;
      using RFGOP = typename PDELabAssemblerTraits<RFR>::GridOperator;

      // compile-time sanity checks
      using RFJAC = typename PDELabAssemblerTraits<RFR>::JacobianMatrix;
      using RFSLV = Solver<RFR>;
      static_assert(std::is_same<JAC, RFJAC>::value);
      static_assert(std::is_same<SLV, RFSLV>::value);

      // allocate a new realization
      realization_.reset(new Realization(sgrid_, *gfs_));

      // sample white noise realization
      whitenoise_.generate_realization(seed);
      auto initspdemodel = SPDEOperator<WNR>(spdeparam_.kappa,
                                             spdeparam_.eta,
                                             whitenoise_.share_realization());

      // enforce necessary constraints for the boundary conditions
      auto cc = typename PDELabTraits::ConstraintsContainer{};
      auto bcadapter = BCA(osgv_, initspdemodel);
      Dune::PDELab::constraints(bcadapter, *gfs_, cc, 0);

      // define the assemblers
      static_assert(RFTraits::dimension == 2);
      constexpr int maxnzs = 9;
      auto initlop = WNLOP(initspdemodel);
      auto initgop = WNGOP(*gfs_, cc, *gfs_, cc, initlop, MBE(maxnzs));

      // allocate the degrees of freedom vector and initialize to initial guess
      auto& rlzdofs = realization_->dofs();
      rlzdofs = RandomFieldDofs(*gfs_, 0.0);

      /* we assume homogeneous Neumann boundary conditions so we dont need
       * to interpolate any Dirichlet boundary conditions onto the coefficient
       */

      // assemble the residual
      auto residual = RandomFieldDofs(*gfs_, 0.0);
      initgop.residual(rlzdofs, residual);

      // assemble the stiffness matrix
      auto fematrix = JAC(initgop);
      initgop.jacobian(rlzdofs, fematrix);

      // prepare solver call
      using Dune::PDELab::Backend::native;
      auto statistics = SolutionStatistics{};
      auto solver = SLV(native(fematrix));
      solver.setVerbosity(0);

      // compute realization
      for (int k{0}; k < spdeparam_.operatorpower; ++k)
      {
        auto defect = RandomFieldDofs(*gfs_, 0.0);

        if (k == 0)
        {
          // solve the defect equation
          solver.apply(native(defect), native(residual), statistics);
        }
        else
        {
          // define a SPDE problem, where the previous sample is the new
          // source
          auto spdemodelk =
            SPDEOperator<RFR>(spdeparam_.kappa, 1.0, realization_);

          // define the assemblers
          auto lopk = RFLOP(spdemodelk);
          auto gopk = RFGOP(*gfs_, cc, *gfs_, cc, lopk, MBE(maxnzs));

          // assemble the residual
          gopk.residual(rlzdofs, residual);

          // set the realization degrees of freedom to the initial guess
          rlzdofs = 0.0;

          // as only the source term has changed, we can reuse the previous
          // factorization of the stiffness matrix and solve the defect
          // equation directly
          solver.apply(native(defect), native(residual), statistics);
        }

        // apply defect correction (exact in this case)
        rlzdofs -= defect;
        realization_->finalize();
      }

      // lock the current degrees of freedom of the realization
      realization_->finalize();
    }


    void reset_parameters(const MaternParameters& newmparam)
    {
      mparam_ = newmparam;
      set_parameters(mparam_);
    }


    R get_variance() const
    {
      return mparam_.variance;
    }


    R get_correlation_length() const
    {
      return mparam_.corrlength;
    }


    R get_smoothness() const
    {
      return mparam_.smoothness;
    }


  protected:

    const OSGridView&   osgv_;
    const SamplingGrid& sgrid_;

    MaternParameters mparam_;
    WhiteNoise       whitenoise_;
    SPDEParameters   spdeparam_;

    std::shared_ptr<FiniteVolumeMap>   fem_;
    std::shared_ptr<GridFunctionSpace> gfs_;
    std::shared_ptr<Realization>       realization_;


    void set_parameters(const MaternParameters& mparam)
    {
    assert(mparam.variance > 0.0);
    assert(mparam.corrlength > 0.0);
    assert(mparam.smoothness > 0.0);
    mparam_ = mparam;

    auto dimhalf = 0.5 * static_cast<ParameterValue>(RFTraits::dimension);
    auto operatorpowerf = 0.5 * (mparam.smoothness + dimhalf);
    int operatorpower = -1;

    // check if the operator exponent is an integer
    if (Utility::is_equal(std::ceil(operatorpowerf), operatorpowerf))
      operatorpower = static_cast<int>(operatorpowerf);
    else
      DUNE_THROW(Dune::InvalidStateException, "Only integer fractional "
                 "operator exponents are supported.");

    // FIXME: Remove when behaviour is fixed
    if (operatorpower > 1)
      std::cout << "WARNING, higher smoothness random fields exhibit "
                << "unexpected behaviour." << std::endl;

    auto kappa = Statistics::matern_kappa(mparam_);
    auto eta =
      Parameters::spde_eta(kappa, mparam_.variance, mparam_.smoothness);

    spdeparam_ = { kappa, eta, operatorpower };
  }
};
} // namespace GRFS

#endif

