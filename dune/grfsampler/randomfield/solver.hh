#ifndef SOLVER_HH
#define SOLVER_HH

#include <dune/grfsampler/traits/solvertraits.hh>


namespace GRFS
{
  namespace Solver
  {
    using SolutionStatistics = Dune::InverseOperatorResult;

    template <class Matrix, class Vector>
    using LinearOperator = Traits::LinearOperator<Matrix, Vector>;

    // template parameter Vector is only for the interface
    template <class Matrix>
    using DirectSparseSolver = Traits::Solver::DirectSparseSolvers<Matrix>;

    template <class Vector>
    using CGSolver = Traits::Solver::CGSolver<Vector>;

    template <class Matrix, class Vector>
    using ILU0Preconditioner = Traits::Preconditioner::ILU0<Matrix, Vector>;
    
    template <class Matrix, class Vector>
    using ILDLPreconditioner = Traits::Preconditioner::ILDL<Matrix, Vector>;
  } // namespace Solver
} // namespace GRFS

#endif

