#ifndef PQ1RANDOMFIELD_HH
#define PQ1RANDOMFIELD_HH

#include <iostream>
#include <memory>
#include <cmath>
#include <cassert>

#include <dune/common/math.hh>
#include <dune/common/exceptions.hh>

#include <dune/grfsampler/traits/randomfieldtraits.hh>
#include <dune/grfsampler/traits/solvertraits.hh>
#include <dune/grfsampler/traits/switches.hh>
#include <dune/grfsampler/randomfield/solver.hh>
#include <dune/grfsampler/randomfield/parameters.hh>
#include <dune/grfsampler/assembly/pq1whitenoise.hh>
#include <dune/grfsampler/assembly/quadrature.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>
#include <dune/grfsampler/adapter/randomfieldsample.hh>


namespace GRFS
{
// TODO: Remove second template parameters
template <class OversamplingGridView,
          template<class> class DirectSparseSolver = Dune::LDL>
class PQ1GaussianRandomField
{
  using SamplingGrid = Adapter::SamplingGrid;

  using WhiteNoise = Assembly::PQ1WhiteNoise<OversamplingGridView>;

  using RFTraits =
    Traits::RandomFieldTraits<Traits::Switches::Discretization::PQ1FEM>;

  using ParameterValue = typename RFTraits::RealNumber;

  using MaternParameters = Statistics::MaternParameters<ParameterValue>;

  using AssemblyResults =
    Assembly::AssemblyDistributor<Assembly::TriangularMatrix<int> >;


  // function pointer to the correct assembly method
  static constexpr auto assemble_ =
    &Assembly::assemble_for_pq1basis<OversamplingGridView>;


  // -------------------------- SOLVERS ---------------------------------------

  using Matrix = typename RFTraits::FlatSparseMatrix;

  using DofVector = typename RFTraits::FlatDofVector;

  using DirectSolver = DirectSparseSolver<Matrix>;

  using LinearOperator = Solver::LinearOperator<Matrix, DofVector>;

  using Preconditioner = Solver::ILU0Preconditioner<Matrix, DofVector>;

  using IterativeSolver = Solver::CGSolver<DofVector>;

  // below, use sparse direct solver, above, use iterative solver
  static constexpr int directsolvercap_s{ 200000 };

  static constexpr double reduction_s{ 1e-8 };



public:

  static constexpr unsigned int dimension = RFTraits::dimension;

  static constexpr unsigned int polynomialorder = RFTraits::order;

  static constexpr auto discretization = RFTraits::discretization;

  using Seed = typename RFTraits::Seed;

  // something along the lines of 'RangeField' would be a better name
  using RealNumber = typename RFTraits::RealNumber;

  using Realization =
    Adapter::PQ1RandomFieldSample<OversamplingGridView, RFTraits>;

  using Basis = typename RFTraits::Basis<OversamplingGridView>;

  struct SPDEParameters
  {
    ParameterValue kappa;
    ParameterValue eta;

    int operatorpower;
  };


  PQ1GaussianRandomField(const SamplingGrid&         sgrid,
                         const OversamplingGridView& gv,
                         const MaternParameters&     mparam)
    : gv_            { gv     }
    , sgrid_         { sgrid }
    , mparam_        {}
    , spdeparam_     {}
    , whitenoise_    {}
    , pq1basis_      { std::make_shared<const Basis>(gv) }
    , realization_   { nullptr }
    , fematrix_      { nullptr }
    , directsolver_  {}
    , linop_         { nullptr }
    , prec_          { nullptr }
    , itersolver_    { nullptr }
  {
    set_parameters(mparam);

    auto assembly = assemble_(*pq1basis_, spdeparam_.kappa);

    set_up_white_noise(*assembly);
    set_up_solver(*assembly);
  }


  // cannot be declared const, as sampling random numbers changes the
  // internal state of the random engine
  void generate_realization(Seed seed)
  {
    assert(fematrix_);

    // in higher dimensions the admissible values for the smoothness
    // parameter as well as the increments in these admissible values change
    static_assert(dimension == 2);
    auto statistics = Solver::SolutionStatistics{};

    // define degrees of freedom vector for the realization
    auto sampledofs = DofVector(pq1basis_->size());

    for (int k{0}; k < spdeparam_.operatorpower; ++k)
    {
      // define empty degrees of freedom vector for the right hand side
      auto prevsampledofs = DofVector{};
      auto& rhs = prevsampledofs;

      // choose correct right hand side for this iteration
      if (k == 0)
        rhs = whitenoise_.sample_scaled_white_noise(spdeparam_.eta, seed);
      else
        prevsampledofs = sampledofs;

      // initial guess
      sampledofs = 0.0;

      // solve (if iterative solver is null, use direct, else use iterative
      if (itersolver_)
        itersolver_->apply(sampledofs, rhs, reduction_s, statistics);
      else
        directsolver_.apply(sampledofs, rhs, statistics);
    }

    assert(statistics.converged);

    // move degrees of freedom to a const std::shared_ptr and set sample
    auto rfcoeffs = std::make_shared<const DofVector>(std::move(sampledofs));
    realization_.reset(new Realization(sgrid_, pq1basis_, rfcoeffs));
  }


  void reset_parameters(const MaternParameters& mparam)
  {
    set_parameters(mparam);

    auto assembly = assemble_(*pq1basis_, spdeparam_.kappa);

    set_up_solver(*assembly);

  }


  const Basis& get_basis() const
  {
    return *pq1basis_;
  }


  const Realization& get_realization() const
  {
    return *realization_;
  }


  std::shared_ptr<const Basis> share_basis()
  {
    return std::shared_ptr<const Basis>(pq1basis_);
  }


  std::shared_ptr<const Realization> share_realization()
  {
    return std::shared_ptr<const Realization>(realization_);
  }


  typename MaternParameters::Value get_variance() const
  {
    return mparam_.variance;
  }


  typename MaternParameters::Value get_correlation_length() const
  {
    return mparam_.corrlength;
  }


  typename MaternParameters::Value get_smoothness() const
  {
    return mparam_.smoothness;
  }


  ParameterValue get_kappa() const
  {
    return spdeparam_.kappa;
  }


  ParameterValue get_eta() const
  {
    return spdeparam_.eta;
  }


private:

  // ---------------------------- MEMBERS -----------------------------------

  const OversamplingGridView& gv_        ;
  const SamplingGrid&         sgrid_     ;
  MaternParameters            mparam_    ;
  SPDEParameters              spdeparam_ ;
  WhiteNoise                  whitenoise_;

  std::shared_ptr<const Basis>             pq1basis_;
  std::shared_ptr<const Realization>       realization_  ;
  std::unique_ptr<const Matrix>            fematrix_;

  DirectSolver    directsolver_;

  std::shared_ptr<LinearOperator> linop_;
  std::shared_ptr<Preconditioner> prec_;
  std::shared_ptr<IterativeSolver> itersolver_;

  // ---------------------------- METHODS -----------------------------------

  // construct from shared pointer to a finite element basis
  PQ1GaussianRandomField(const SamplingGrid& sgrid,
                         std::shared_ptr<const Basis> pq1basis,
                         const MaternParameters& mparam)
    : gv_            { pq1basis->gridView() }
    , sgrid_        { sgrid }
    , mparam_        {}
    , spdeparam_     {}
    , whitenoise_    {}
    , pq1basis_      { pq1basis }
    , realization_   { nullptr }
    , fematrix_      { nullptr }
    , directsolver_  {}
    , linop_         { nullptr }
    , prec_          { nullptr }
    , itersolver_    { nullptr }
  {
    set_parameters(mparam);

    auto assembly = assemble_(*pq1basis_, spdeparam_.kappa);
    set_up_white_noise(*assembly);
    set_up_solver(*assembly);
  }


  void set_parameters(const MaternParameters& mparam)
  {
    assert(mparam.variance > 0.0);
    assert(mparam.corrlength > 0.0);
    assert(mparam.smoothness > 0.0);
    mparam_ = mparam;

    auto dimhalf = 0.5 * static_cast<ParameterValue>(RFTraits::dimension);
    auto operatorpowerf = 0.5 * (mparam.smoothness + dimhalf);
    int operatorpower = -1;

    // check if the operator exponent is an integer
    if (Utility::is_equal(std::ceil(operatorpowerf), operatorpowerf))
      operatorpower = static_cast<int>(operatorpowerf);
    else
      DUNE_THROW(Dune::InvalidStateException, "Only integer fractional "
                 "operator exponents are supported.");

    // FIXME: Remove when behaviour is fixed
    if (operatorpower > 1)
      std::cout << "WARNING, higher smoothness random fields exhibit "
                << "unexpected behaviour." << std::endl;

    auto kappa = Statistics::matern_kappa(mparam_);
    auto eta =
      Parameters::spde_eta(kappa, mparam_.variance, mparam_.smoothness);

    spdeparam_ = { kappa, eta, operatorpower };
  }


  void set_up_white_noise(AssemblyResults& assembly)
  {
    assert(!assembly.factorization_released());

    whitenoise_ = WhiteNoise(pq1basis_, assembly.release_factorization());

    assert(assembly.factorization_released());
  }


  void set_up_solver(AssemblyResults& assembly)
  {
    assert(!assembly.matrix_released());

    fematrix_.reset(assembly.release_matrix());

    // if basis size is below cap, use direct solver, else use iterative solver
    if (pq1basis_->size() < directsolvercap_s)
    {
      // DEBUG
      std::cout << "USING DIRECT SPARSE SOLVER" << std::endl;
      directsolver_.setMatrix(*fematrix_);
      directsolver_.setVerbosity(0);
    }
    else
    {
      // DEBUG 
      std::cout << "USING ITERATIVE SOLVER" << std::endl;
      linop_.reset(new LinearOperator(*fematrix_));
      prec_.reset(new Preconditioner(*fematrix_, 1.0));
      itersolver_.reset(
        new IterativeSolver(*linop_, *prec_, reduction_s, 200, 0, false));
    }
                              
    assert(assembly.matrix_released());
  }
};
} // namespace GRFS

#endif

