#ifndef CIRCULANTEMBEDDINGRANDOMFIELD_HH
#define CIRCULANTEMBEDDINGRANDOMFIELD_HH

#include <memory>
#include <string>

#include <dune/common/unused.hh>

#include <dune/grfsampler/traits/adaptertraits.hh>
#include <dune/grfsampler/traits/switches.hh>


namespace GRFS
{
  class CirculantEmbeddingRandomField
  {
    using RangeField = double;

    using CERFTraits = Traits::CERandomFieldTraits<RangeField>;

    using DomainVector =
      typename CERFTraits::CirculantEmbeddingGridTraits::Domain;
    using RandomField = typename CERFTraits::RandomField;


    class CirculantEmbeddingRandomFieldRealization
    {
    public:

      CirculantEmbeddingRandomFieldRealization(const RandomField& rf)
      : randomfield_{ rf }
      {}


      template <class Element>
      RangeField evaluate(const Element& element, const DomainVector& x) const
      {
        DUNE_UNUSED_PARAMETER(element);

        auto rfieldval =
          typename CERFTraits::CirculantEmbeddingGridTraits::Scalar{};
        randomfield_.evaluate(x, rfieldval);

        return rfieldval;
      }


    private:

      const RandomField& randomfield_;
    };


  public:

    static constexpr int dimension = 2;

    static constexpr Traits::Switches::Discretization discretization =
      Traits::Switches::Discretization::CIRCULANTEMBEDDING;

    using RealNumber = RangeField;

    using GridTraits = typename CERFTraits::CirculantEmbeddingGridTraits;

    using Seed = typename CERFTraits::Seed;

    using Configuration = typename CERFTraits::Configuration;

    using Realization = CirculantEmbeddingRandomFieldRealization;

    CirculantEmbeddingRandomField(const Configuration& ptree)
    : randomfield_(ptree)
    , realization_(new Realization(randomfield_))
    {}


    void generate_realization(const Seed& seed)
    {
      randomfield_.generate(seed);
    }


    const Realization& get_realization() const
    {
      return *realization_;
    }


    std::shared_ptr<Realization> share_realization() const
    {
      return realization_;
    }


    void refine()
    {
      randomfield_.refine();
    }


    template <class GridView>
    void write_to_vtk(const std::string& filename, const GridView& gridview)
    {
      randomfield_.writeToVTKSeparate(filename, gridview);
    }


  private:

    RandomField randomfield_;

    std::shared_ptr<Realization> realization_;
  };
}

#endif
