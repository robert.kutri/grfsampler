#ifndef SPDEOPERATOR_HH
#define SPDEOPERATOR_HH

#include <memory>

#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>


namespace GRFS
{
namespace Adapter
{
  template<class RandomFieldRealization, class OSGridView, typename RealNumber>
  class SPDEOperator
  {
    using BCType = Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type;


  public:

    using Traits =
      Dune::PDELab::ConvectionDiffusionParameterTraits<OSGridView, RealNumber>;


    static constexpr bool permeabilityIsConstantPerCell()
    {
      return true;
    }


    SPDEOperator(const RealNumber& kappa,
                 const RealNumber& eta,
                 std::shared_ptr<RandomFieldRealization> source)
    : kappasqinv_{ 1.0 / (kappa * kappa) }
    , eta_{ eta }
    , source_{ source }
    {}


    // tensor diffusion coefficient
    typename Traits::PermTensorType
    A (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return typename Traits::PermTensorType{ { {kappasqinv_, 0          },
                                                {0.         , kappasqinv_} } };
    }


    // velocity field
    typename Traits::RangeType
    b (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return typename Traits::RangeType(0.);
    }


    // sink term
    typename Traits::RangeFieldType
    c (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return 1.0;
    }


    // source term
    typename Traits::RangeFieldType
    f (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return eta_ * source_->evaluate(e, x);
    }


    // boundary condition type function
    BCType
    bctype (const typename Traits::IntersectionType& is,
            const typename Traits::IntersectionDomainType& x) const
    {
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
    }


    // Dirichlet boundary condition value
    typename Traits::RangeFieldType
    g (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return 0.0;
    }


    // Neumann boundary condition
    typename Traits::RangeFieldType
    j (const typename Traits::IntersectionType& is,
       const typename Traits::IntersectionDomainType& x) const
    {
      return 0.0;
    }


    // outflow boundary condition
    typename Traits::RangeFieldType
    o (const typename Traits::IntersectionType& is,
       const typename Traits::IntersectionDomainType& x) const
    {
      return 0.0;
    }


  private:

    RealNumber kappasqinv_;
    RealNumber eta_;
    std::shared_ptr<RandomFieldRealization> source_;
  };
} // namespace Adapter
} // namespace GRFS

#endif

