#ifndef DARCYPROBLEM_HH
#define DARCYPROBLEM_HH

#include <memory>
#include <iostream>

#include <dune/grfsampler/traits/adaptertraits.hh>
#include <dune/grfsampler/traits/solvertraits.hh>
#include <dune/grfsampler/traits/gridtraits.hh>
#include <dune/grfsampler/adapter/darcydata.hh>

#include <dune/grfsampler/traits/switches.hh>


namespace GRFS
{
namespace Adapter
{
  template <class RandomField, class TargetGridView>
  class DarcyModelProblem
  {
    using RF = typename RandomField::RealNumber;

    using RFRealization = typename RandomField::Realization;

    using DPTraits =
      Traits::PDELabTraits<
        TargetGridView, RF, RandomField::dimension,
        RandomField::discretization>;

    using Data = LogNormalCoefficientData<TargetGridView, RFRealization, RF>;

    using AssemblerTraits = Traits::PDELabAssemblerTraits<Data, DPTraits>;

    using Solvers =
      Traits::Solver::DirectSparseSolvers<
        typename AssemblerTraits::NativeMatrix>;


  public:

    using ModelData = Data;

    using FEM = typename DPTraits::FiniteElementMap;

    using GFS = typename DPTraits::GridFunctionSpace;

    using GVF = typename DPTraits::GridViewFunction;

    using DOF = typename DPTraits::CoefficientVector;

    using MBE = typename AssemblerTraits::MatrixBackend;

    template <class GridFunction>
    using VTKF = typename DPTraits::template VTKAdapter<GridFunction>;


    DarcyModelProblem(const TargetGridView& gridview,
                      const RFRealization& rfs)
    : gridview_{ gridview }
    , rfs_     { rfs      }
    , fem_     { nullptr  }
    , gfs_     { nullptr  }
    , solgvf_  { nullptr  }
    {
      if constexpr (RandomField::discretization ==
                      Traits::Switches::Discretization::PQ1FEM)
      {
        fem_.reset(new FEM{});
      }

      if constexpr (RandomField::discretization ==
                      Traits::Switches::Discretization::CCFV)
      {
        fem_.reset(new FEM(DPTraits::gt));
      }

      if (!fem_)
      {
        DUNE_THROW(Dune::InvalidStateException, "Discretization Method is "
                   "neither piecewise linear FEM, nor CCFV");
      }

      gfs_.reset(new GFS(gridview_, fem_));
    }


    const GVF& get_solution() const
    {
      return *solgvf_;
    }


    void solve(double reduction = 1e-10)
    {
      auto md = ModelData(rfs_);

      // enforce constraits at the Dirichlet boundaries
      auto cc = typename DPTraits::ConstraintsContainer{};
      auto bcadapter =
        typename AssemblerTraits::BoundaryConditionAdapter(gridview_, md);
      Dune::PDELab::constraints(bcadapter, *gfs_, cc, 0);

      // define the assemblers
      static_assert(RandomField::polynomialorder < 2 and
                    RandomField::dimension == 2);
      constexpr int maxnzs = 9;
      auto lop = typename AssemblerTraits::LocalOperator(md);
      auto gop =
        typename AssemblerTraits::GridOperator(*gfs_, cc,
                                               *gfs_, cc,
                                               lop, MBE(maxnzs));

      // allocate the degrees of freedom vector and intialize to intial guess
      soldofs_.reset(new DOF(*gfs_, 0.0));

      // interpolate boundary conditions
      auto dirbc = typename AssemblerTraits::DirichletAdapter(gridview_, md);
      Dune::PDELab::interpolate(dirbc, *gfs_, *soldofs_);

      // use direct sparse solver for small systems and iterative solver
      // for large ones
      constexpr int directsolvercap = 2000;
      if (gridview_.size(0) < directsolvercap)
      {
        std::cout << "Using direct sparse solver for Darcy problem"
                  << std::endl;

        // assemble the residual
        auto residual = DOF(*gfs_, 0.0);
        gop.residual(*soldofs_, residual);

        // assemble the stiffness matrix
        auto fematrix = typename AssemblerTraits::JacobianMatrix(gop);
        gop.jacobian(*soldofs_, fematrix);

        // initialize the defect
        auto defect = DOF(*gfs_, 0.0);

        // choose direct sparse solver
        using Dune::PDELab::Backend::native;
        using SLV =
          typename Traits::Solver::DirectSparseSolvers<
            typename AssemblerTraits::NativeMatrix>::UMFPack;

        // solve the defect equation Av = Ax_0 - b
        auto statistics = Dune::InverseOperatorResult{};
        auto solver = SLV(native(fematrix));
        solver.apply(native(defect), native(residual), statistics);

        // apply defect correction x = x_0 - v (exact in this case)
        *soldofs_ -= defect;
      }
      else
      {
        std::cout << "Using iterative solver for Darcy problem" << std::endl;
        using SLVTraits =
          Traits::Solver::PDELabSolverTraits<DOF, AssemblerTraits>;

        // define solver, preconditioner, smoother combination
        using SBE = typename SLVTraits::CG_Solver_ILU0_Prec;
        auto ls = SBE(100, 0);

        // initialize actual solver
        using SLV = typename SLVTraits::template Solver<SBE>;
        auto solver = SLV(gop, ls, *soldofs_, reduction);

        // solve
        solver.apply();
      }

      solgvf_.reset(
        new GVF(gfs_, std::make_shared<const DOF>(*gfs_, *soldofs_)));
    }


  protected:

    const TargetGridView&           gridview_;
    const RFRealization&            rfs_     ;
    std::unique_ptr<DOF>            soldofs_ ;
    std::shared_ptr<const FEM>      fem_     ;
    std::shared_ptr<const GFS>      gfs_     ;
    std::shared_ptr<const GVF>      solgvf_  ;
  };


  template <class RandomField, class TargetGridView>
  class DarcyUQProblem
  {
    using GridTraits =
      Traits::GridTraits<Traits::Switches::GridLowerLeft::GENERIC>;

    using RF = typename RandomField::RealNumber;

    using RFRealization = typename RandomField::Realization;

    using DPTraits =
      Traits::PDELabTraits<TargetGridView, RF, RandomField::dimension,
                           RandomField::discretization>;

    using Data = LogNormalCoefficientData<TargetGridView, RFRealization, RF>;

    using AssemblerTraits = Traits::PDELabAssemblerTraits<Data, DPTraits>;

    using Quadrature = Dune::QuadratureRules<RF, RandomField::dimension>;


  public:

    using ModelData = Data;

    using MBE = typename AssemblerTraits::MatrixBackend;

    using QuantityOfInterest = RF;

    using QoIDomain = typename GridTraits::RectangularDomain;

    using GVF = typename DPTraits::GridViewFunction;

    using DOF = typename DPTraits::CoefficientVector;


    // setup complete UQ problem
    DarcyUQProblem(const TargetGridView& gridview,
                   const QoIDomain&      qoidomain,
                   const RFRealization&  rfs)
    : pdesolved_{ false     }
    , gridview_ { gridview  }
    , rfs_      { rfs       }
    , qoidomain_{ qoidomain }
    , fem_      { nullptr   }
    , gfs_      { nullptr   }
    , solgvf_   { nullptr   }
    {
      if constexpr (RandomField::discretization ==
                      Traits::Switches::Discretization::PQ1FEM
                    or
                    RandomField::discretization ==
                      Traits::Switches::Discretization::CIRCULANTEMBEDDING)
      {
        fem_.reset(new typename DPTraits::FiniteElementMap{});
      }
      if constexpr (RandomField::discretization ==
                      Traits::Switches::Discretization::CCFV)
      {
        fem_.reset(new typename DPTraits::FiniteElementMap(DPTraits::gt));
      }

      if (!fem_)
      {
        std::cerr << "WARNING, discretization method is neither piecewise "
                     "linear FEM, nor CCFV" << std::endl;

        fem_.reset(new typename DPTraits::FiniteElementMap{});
      }

      gfs_.reset(new typename DPTraits::GridFunctionSpace(gridview_, fem_));
    }


    void solve(double reduction=1e-10)
    {
      assert(!pdesolved_);

      // set up the model problem data
      auto md = ModelData(rfs_);

      // enforce the constraints
      auto cc = typename DPTraits::ConstraintsContainer{};
      auto bcadapter =
        typename AssemblerTraits::BoundaryConditionAdapter(gridview_, md);
      Dune::PDELab::constraints(bcadapter, *gfs_, cc, 0);

      // define the local operator for this specific Darcy problem
      auto lop = typename AssemblerTraits::LocalOperator(md);

      // initialize the assembler
      static_assert(DPTraits::order < 2 and
                    RandomField::dimension == 2);
      constexpr int maxnzs = 9;
      auto gop = typename AssemblerTraits::GridOperator(*gfs_, cc,
                                                        *gfs_, cc,
                                                        lop, MBE(maxnzs));

      // allocate the degrees of freedom vector and intialize to intial guess
      soldofs_.reset(new DOF(*gfs_, 0.0));

      // interpolate the boundary conditions
      auto dirbc = typename AssemblerTraits::DirichletAdapter(gridview_, md);
      Dune::PDELab::interpolate(dirbc, *gfs_, *soldofs_);

      // use direct sparse solver for small systems and iterative solver
      // for large ones
      constexpr int directsolvercap = 2000;
      if (gridview_.size(0) < directsolvercap)
      {
        // assemble the residual
        auto residual = DOF(*gfs_, 0.0);
        gop.residual(*soldofs_, residual);

        // assemble the stiffness matrix
        auto fematrix = typename AssemblerTraits::JacobianMatrix(gop);
        gop.jacobian(*soldofs_, fematrix);

        // initialize the defect
        auto defect = DOF(*gfs_, 0.0);

        // choose direct sparse solver
        using Dune::PDELab::Backend::native;
        using SLV =
          typename Traits::Solver::DirectSparseSolvers<
            typename AssemblerTraits::NativeMatrix>::UMFPack;

        // solve the defect equation Av = Ax_0 - b
        auto statistics = Dune::InverseOperatorResult{};
        auto solver = SLV(native(fematrix));
        solver.apply(native(defect), native(residual), statistics);

        // apply defect correction x = x_0 - v (exact in this case)
        *soldofs_ -= defect;
      }
      else
      {
        using SLVTraits =
          Traits::Solver::PDELabSolverTraits<DOF, AssemblerTraits>;

        // define solver, preconditioner, smoother combination
        using SBE = typename SLVTraits::CG_Solver_ILU0_Prec;
        auto ls = SBE(100, 0);

        // initialize actual solver
        using SLV = typename SLVTraits::template Solver<SBE>;
        auto solver = SLV(gop, ls, *soldofs_, reduction, 1e-20, 0);

        // solve
        solver.apply();
      }

      // store the solution as a GridViewFunction
      solgvf_.reset(
        new GVF(gfs_, std::make_shared<const DOF>(*gfs_, *soldofs_)));

      // mark the solution as computed
      pdesolved_ = true;
    }


    QuantityOfInterest quantity_of_interest() const
    {
      if (!pdesolved_)
        DUNE_THROW(Dune::InvalidStateException, "Trying to retrieve quantity "
                   "of interest before the PDE was solved.");

      // create the local function corresponding to the solution
      auto sollf = localFunction(*solgvf_);

      auto intgrl = static_cast<RF>(0.0);

      // accumulate the integrals over the elements in the integration domain
      for (const auto& element : elements(gridview_))
      {
        const auto geometry = element.geometry();

        if (inside_qoi_domain(geometry.center()))
        {
          // define a quadrature rule
          constexpr int qorder =
            GridTraits::dimension * DPTraits::order;
          const auto& quadrature = Quadrature::rule(geometry.type(), qorder);

          sollf.bind(element);

          for (const auto& qpoint : quadrature)
          {
            const auto qpos = qpoint.position();

            // evaluate the solution at the quadrature point. Technicality:
            // GridViewFunctions support vector output, so the value
            // must be a vector type
            const auto val = sollf(qpos);

            const auto absjacdet = geometry.integrationElement(qpos);

            // add contribution
            intgrl += qpoint.weight() * absjacdet * val[0];
          }
        }
      }

      // volume of the integration domain
      const auto idomvol = (qoidomain_[1][0] - qoidomain_[0][0])
                           * (qoidomain_[1][1] - qoidomain_[0][1]);

      return intgrl / idomvol;
    }


  protected:

    bool                  pdesolved_;
    const TargetGridView& gridview_ ;
    const RFRealization&  rfs_      ;
    QoIDomain             qoidomain_;

    std::unique_ptr<typename DPTraits::CoefficientVector>       soldofs_;
    std::shared_ptr<const typename DPTraits::FiniteElementMap>  fem_    ;
    std::shared_ptr<const typename DPTraits::GridFunctionSpace> gfs_    ;
    std::shared_ptr<const typename DPTraits::GridViewFunction>  solgvf_ ;


    // constructor used for mock objects in unit test
    DarcyUQProblem(const TargetGridView& gv, const QoIDomain& qoid)
    : pdesolved_{ true }
    , gridview_ { gv }
    , rfs_{}
    , qoidomain_{ qoid }
    , soldofs_  { nullptr }
    , fem_      { nullptr }
    , gfs_      { nullptr }
    , solgvf_   { nullptr }
    {
      if constexpr (RandomField::discretization ==
                      Traits::Switches::Discretization::PQ1FEM)
      {
        fem_.reset(new typename DPTraits::FiniteElementMap{});
      }
      if constexpr (RandomField::discretization ==
                      Traits::Switches::Discretization::CCFV)
      {
        fem_.reset(new typename DPTraits::FiniteElementMap(DPTraits::gt));
      }

      if (!fem_ and RandomField::discretization !=
                      Traits::Switches::Discretization::CIRCULANTEMBEDDING)
      {
        std::cerr << "WARNING, discretization method is neither piecewise "
                     "linear FEM, nor CCFV" << std::endl;
        fem_.reset(new typename DPTraits::FiniteElementMap{});
      }

      gfs_.reset(new typename DPTraits::GridFunctionSpace(gridview_, fem_));
    }


    bool inside_qoi_domain(const typename GridTraits::DomainVector& x) const
    {
      return ((qoidomain_[0][0] < x[0] and x[0] < qoidomain_[1][0])
              and
              (qoidomain_[0][1] < x[1] and x[1] < qoidomain_[1][1]));
    }
  };
} // namespace Adapter
} // namespace GRFS

#endif

