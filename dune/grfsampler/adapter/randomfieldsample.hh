#ifndef RANDOMFIELDSAMPLE_HH
#define RANDOMFIELDSAMPLE_HH

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/function/discretegridviewfunction.hh>

#include <dune/grfsampler/traits/adaptertraits.hh>
#include <dune/grfsampler/adapter/oversamplingdomain.hh>


namespace GRFS
{
namespace Adapter
{
  template <class OSGridView, class RFTraits>
  class PQ1RandomFieldSample
  {
  public:

    using RealNumber    = typename RFTraits::RealNumber;

    using SamplingGrid  = Adapter::SamplingGrid;

    using DomainVector  = typename SamplingGrid::DomainVector;

    using PQ1Basis      = typename RFTraits::template PQ1Basis<OSGridView>;

    using DofVector     = typename RFTraits::FlatDofVector;

    using GridViewFunction =
      Dune::Functions::DiscreteGlobalBasisFunction<PQ1Basis, DofVector>;

    using NTRE = typename GridViewFunction::NodeToRangeEntry;


    PQ1RandomFieldSample(const SamplingGrid&             grid,
                        std::shared_ptr<const PQ1Basis>  pq1basis,
                        std::shared_ptr<const DofVector> rfcoeffs)
    : grid_    { grid     }
    , pq1basis_{ pq1basis }
    , coeffs_  { rfcoeffs }
    , ntre_    { std::make_shared<const NTRE>(NTRE{})              }
    , realization_(pq1basis_, coeffs_, ntre_)
    {}


    DofVector degrees_of_freedom() const
    {
      return DofVector{ *coeffs_ };
    }


    template <class Element>
    RealNumber evaluate_on_osgrid(const Element& element,
                                  const DomainVector& x) const
    {
      auto localfunction = localFunction(realization_);
      localfunction.bind(element);

      return localfunction(x);
    }


    template <class TargetGridElement>
    RealNumber evaluate(const TargetGridElement& te,
                        const DomainVector& x) const
    {
        return evaluate_on_osgrid(grid_.oversampling_element(te), x);
    }


  protected:

    const SamplingGrid&              grid_       ;
    std::shared_ptr<const PQ1Basis>  pq1basis_   ;
    std::shared_ptr<const DofVector> coeffs_     ;
    std::shared_ptr<const NTRE>      ntre_       ;
    GridViewFunction                 realization_;
  };


  template <class RandomField, class RFTraits>
  class CirculantEmbeddingRandomFieldSample;
} // namespace Adapter
} // namespace GRFS

#endif

