#ifndef DARCYDATA_HH
#define DARCYDATA_HH

#include <memory>
#include <cmath>

#include <dune/pdelab/localoperator/convectiondiffusionparameter.hh>


namespace GRFS
{
namespace Adapter
{
  template<class GridView, class RandomFieldRealization, typename RealNumber>
  class LogNormalCoefficientData
  {
    using BCType = Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type;


  public:

    using Traits =
      Dune::PDELab::ConvectionDiffusionParameterTraits<GridView, RealNumber>;


    static constexpr bool permeabilityIsConstantPerCell()
    {
      return true;
    }


    LogNormalCoefficientData(const RandomFieldRealization& rfs)
      : rfs_{ rfs }
    {}


    // tensor diffusion coefficient
    typename Traits::PermTensorType
    A (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      auto ptval = std::exp(rfs_.evaluate(e, x));

      auto I = typename Traits::PermTensorType{};

      static_assert(Traits::dimDomain == 2);
      I[0][0] = ptval; I[0][1] =   0.0;
      I[1][0] =   0.0; I[1][1] = ptval;

      return I;
    }


    // velocity field
    typename Traits::RangeType
    b (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      typename Traits::RangeType v(0.0);
      return v;
    }


    // sink term
    typename Traits::RangeFieldType
    c (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return 0.0;
    }


    // source term
    typename Traits::RangeFieldType
    f (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return 1.0;
    }


    // boundary condition type function
    BCType
    bctype (const typename Traits::IntersectionType& is,
            const typename Traits::IntersectionDomainType& x) const
    {
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
    }


    // Dirichlet boundary condition value
    typename Traits::RangeFieldType
    g (const typename Traits::ElementType& e,
       const typename Traits::DomainType& x) const
    {
      return 0.0;
    }


    // Neumann boundary condition
    typename Traits::RangeFieldType
    j (const typename Traits::IntersectionType& is,
       const typename Traits::IntersectionDomainType& x) const
    {
      return 0.0;
    }


    // outflow boundary condition
    typename Traits::RangeFieldType
    o (const typename Traits::IntersectionType& is,
       const typename Traits::IntersectionDomainType& x) const
    {
      return 0.0;
    }


  private:

    const RandomFieldRealization& rfs_;
  };
} // namespace Adapter
} // namespace GRFS

#endif

