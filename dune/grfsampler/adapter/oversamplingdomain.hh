#ifndef OVERSAMPLINGDOMAIN_HH
#define OVERSAMPLINGDOMAIN_HH

#include <bitset>
#include <memory>

#include <dune/grfsampler/traits/gridtraits.hh>
#include <dune/grfsampler/traits/switches.hh>


namespace GRFS
{
namespace Adapter
{
  class SamplingGrid
  {
    using SGTraits =
      Traits::GridTraits<Traits::Switches::GridLowerLeft::GENERIC>;


  public:

    static constexpr auto geometrytype = SGTraits::geometrytype;


    using DomainField               = typename SGTraits::DomainField;

    using DomainVector              = typename SGTraits::DomainVector;

    using RectangularDomain         = typename SGTraits::RectangularDomain;

    using CellLayout                = typename SGTraits::CellLayout;

    using HostGrid                  = typename SGTraits::Grid;

    using MultiDomainGrid           = typename SGTraits::MultiDomainGrid;

    using TargetGrid                = typename MultiDomainGrid::SubDomainGrid;

    using OversamplingLevelGridView = typename MultiDomainGrid::LevelGridView;

    using OversamplingLeafGridView  = typename MultiDomainGrid::LeafGridView;

    using TargetLevelGridView       = typename TargetGrid::LevelGridView;

    using TargetLeafGridView        = typename TargetGrid::LeafGridView;


    SamplingGrid(const RectangularDomain& targetdomain,
                 const CellLayout&        tdomlayout,
                 const DomainField&       minlyrw)
      : hostdomain_{}
      , subdomain_{ targetdomain }
      , hostgrid_{ instantiate_host_grid(targetdomain, tdomlayout, minlyrw) }
      , multidomaingrid_(*hostgrid_, true)
        /*
         * mark the host elements that correspond to the desired subdomain and
         * add the corresponding elements to the subdomain grid
         */
    {
      if (minlyrw == 0.0)
      {
        std::cout << "WARNING, oversampling grid coincides with target grid"
                  << std::endl;
      }

      auto mdgv = multidomaingrid_.leafGridView();

      multidomaingrid_.startSubDomainMarking();
      constexpr int subdomainindex = 0;

      for (const auto& element : elements(mdgv))
      {
        const auto center = element.geometry().center();

        if (inside_domain(center, targetdomain))
          multidomaingrid_.addToSubDomain(subdomainindex, element);
      }

      multidomaingrid_.preUpdateSubDomains();
      multidomaingrid_.updateSubDomains();
      multidomaingrid_.postUpdateSubDomains();
    }


    const RectangularDomain& oversampling_domain() const
    {
      return hostdomain_;
    }


    const RectangularDomain& target_domain() const
    {
      return subdomain_;
    }


    std::size_t number_of_oversampling_cells(const int& level) const
    {
      return multidomaingrid_.size(level, 0);
    }


    std::size_t number_of_target_cells(const int& level) const
    {
      return multidomaingrid_.subDomain(0).size(level, 0);
    }


    CellLayout oversampling_level_size(const int& level) const
    {
      return hostgrid_->levelSize(level);
    }


    CellLayout target_level_size(const int& level) const
    {
      const auto sqrtn =
        std::sqrt(multidomaingrid_.subDomain(0).levelGridView(level).size(0));

      assert(std::ceil(sqrtn) == sqrtn);
      assert(oversampling_level_size(level)[0] ==
             oversampling_level_size(level)[1]);

      const auto nx = static_cast<int>(sqrtn);
      const auto ny = nx;

      return { {nx , ny} };
    }


    void global_refine(const int& refcount)
    {
      multidomaingrid_.globalRefine(refcount);
    }


    TargetLevelGridView target_level_grid_view(int lvl) const
    {
      return multidomaingrid_.subDomain(0).levelGridView(lvl);
    }


    TargetLeafGridView target_leaf_grid_view() const
    {
      return multidomaingrid_.subDomain(0).leafGridView();
    }


    OversamplingLevelGridView oversampling_level_grid_view(int lvl) const
    {
      return multidomaingrid_.levelGridView(lvl);
    }


    OversamplingLeafGridView oversampling_leaf_grid_view() const
    {
      return multidomaingrid_.leafGridView();
    }


    template <class SubDomainEntity>
    const auto& oversampling_element(const SubDomainEntity& telement) const
    {
      return multidomaingrid_.multiDomainEntity(telement);
    }


  protected:

    // ----------------------------- MEMBERS ----------------------------------

    RectangularDomain hostdomain_;
    RectangularDomain subdomain_;

    std::unique_ptr<HostGrid> hostgrid_;
    MultiDomainGrid           multidomaingrid_;


    // ----------------------------- METHODS ----------------------------------

    // side effect: stores the host domain extent in hostdomain_
    std::unique_ptr<HostGrid> instantiate_host_grid(
        const RectangularDomain& sd,
        const CellLayout& sdlayout,
        const DomainField mlw)
    {
      // desired mesh width in the subdomain
      const auto hx =
        (sd[1][0] - sd[0][0]) / static_cast<DomainField>(sdlayout[0]);
      const auto hy =
        (sd[1][1] - sd[0][1]) / static_cast<DomainField>(sdlayout[1]);

      // number of cells in the layer, for each dimension
      const auto nclx = int(std::ceil(mlw / hx));
      const auto ncly = int(std::ceil(mlw / hy));

      // number of cells per dimension in host grid
      const auto hostlayout = CellLayout{ 2 * nclx + sdlayout[0],
                                          2 * ncly + sdlayout[1] };

      // offsets of the host domain w.r.t. the target domain
      const auto offx = double(nclx) * hx;
      const auto offy = double(ncly) * hy;

      // corners of the host grid domain
      const auto hostll = DomainVector{ sd[0][0] - offx, sd[0][1] - offy };
      const auto hostur = DomainVector{ sd[1][0] + offx, sd[1][1] + offy };

      // store the domain extents
      hostdomain_ = RectangularDomain{ hostll, hostur };

      static_assert(SGTraits::dimension == 2);
      return std::make_unique<HostGrid>(hostll, hostur, hostlayout,
                                        std::bitset<2>("00") /* periodic */,
                                        0                    /* overlap  */);
    }


    bool inside_domain(const DomainVector& center,
                       const RectangularDomain& subdomain) const
    {
      return ((subdomain[0][0] < center[0]) and (center[0] < subdomain[1][0])
              and
              (subdomain[0][1] < center[1]) and (center[1] < subdomain[1][1]));
    }
  };
} // namespace Adapter
} // namespace GRFS

#endif

