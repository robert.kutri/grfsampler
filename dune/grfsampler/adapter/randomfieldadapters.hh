#ifndef RANDOMFIELDADAPTERS_HH
#define RANDOMFIELDADAPTERS_HH

#include <memory>
#include <utility>

#include <dune/functions/functionspacebases/interpolate.hh>

#include <dune/grfsampler/traits/adaptertraits.hh>


namespace Adapter
{
  template <class AdapterTraits>
  std::shared_ptr<typename AdapterTraits::RandomFieldSample>
  make_shared_ptr_to_random_field(
    std::shared_ptr<const typename AdapterTraits::FEBasis> febasis,
    std::shared_ptr<
      const typename AdapterTraits::RandomFieldCoefficient> sample)
  {
    auto ntreptr =
      std::shared_ptr<const typename AdapterTraits::NTRE>(
        new typename AdapterTraits::NTRE{});

    return std::shared_ptr<typename AdapterTraits::RandomFieldSample>(
      new typename AdapterTraits::RandomFieldSample(febasis, sample, ntreptr));
  }


  template <class GridView, class RandomField, class GlobalTraits>
  typename AdapterTraits<GridView, GlobalTraits>::RandomFieldCoefficient
  interpolate_random_field(
      const GridView& gv,
      const RandomField& rf,
      const typename GlobalTraits::template PQ1Basis<GridView> basis)
  {
    auto rffcn = [&rf=std::as_const(rf)](const auto& x)
                 {
                   auto fieldval =
                     typename AdapterTraits<GridView, GlobalTraits>::Scalar{};

                   rf.evaluate(x, fieldval);

                   return fieldval;
                 };

    auto coeff =
      typename AdapterTraits<GridView, GlobalTraits>::RandomFieldCoefficient{};
    Dune::Functions::interpolate(basis, coeff, rffcn);

    return coeff;
  }
} // namespace Adapter

#endif

