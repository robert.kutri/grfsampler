# GRFSampler

Sampling of Gaussian random fields using the
[stochastic partial differential equation approach](https://rss.onlinelibrary.wiley.com/doi/10.1111/j.1467-9868.2011.00777.x#).

# Requirements

- at least C++17 language standard
- Python 3 with numpy, scipy and matplotlib installations
