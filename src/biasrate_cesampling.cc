#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <random>
#include <fstream>
#include <iomanip>
#include <string>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/exceptions.hh>

#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/utility/gridhelper.hh>
#include <dune/grfsampler/adapter/darcyproblem.hh>
#include <dune/grfsampler/randomfield/circulantembeddingrandomfield.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>


using Statistics::unbiased_sample_variance;

using RandomField = GRFS::CirculantEmbeddingRandomField;

using R = typename RandomField::RealNumber;
using DomainVector = typename RandomField::GridTraits::Domain;

template <class GridView>
using UQProblem = GRFS::Adapter::DarcyUQProblem<RandomField, GridView>;

template <class GridView>
using QoIDomain = typename UQProblem<GridView>::QoIDomain;


constexpr char inifilename[] = "biasrate_ce.ini";


static constexpr unsigned int dim = 2;


template <class GridView>
QoIDomain<GridView> qoi_domain(const int& idhw, const int& cellsperdim,
                               const DomainVector& ur)
{
  if (idhw == 0)
    DUNE_THROW(Dune::Exception, "QoI domain of size zero.");
  if (cellsperdim % 2 == 1)
    DUNE_THROW(Dune::Exception, "Cells per dimension must be even number");

  const auto h = ur[0] / cellsperdim;

  const auto pd = static_cast<R>(cellsperdim / 2 - idhw);

  if (pd < 0)
    DUNE_THROW(Dune::Exception, "QoI domain larger than grid domain");

  return QoIDomain<GridView>{ DomainVector{ pd * h, pd * h },
                              DomainVector{ (pd + 2 * idhw) * h,
                                            (pd + 2 * idhw) * h } };
}


int main(int argc, char** argv)
{
  // initialize MPI helper in case of parallel execution
  const auto& mpihelper = Dune::MPIHelper::instance(argc, argv);
  const auto processorid = mpihelper.rank();

  // with the way the QoI integration domain is currently set up, only the
  // layout resulting from either 1, 2 or 4 processors yields correct results
  assert(mpihelper.size() == 1 or
         mpihelper.size() == 2 or
         mpihelper.size() == 4);

  if (processorid == 0)
  {
    std::cout << "\n\nStart sampling on " << mpihelper.size()
              << " processor(s)" << std::endl;
  }

  std::cout << "\nReading Setup from ini file" << std::endl;

  auto iniparser = Dune::ParameterTreeParser{};

  // read the main .ini file
  auto ptree = Dune::ParameterTree{};
  iniparser.readINITree(inifilename, ptree);
  iniparser.readOptions(argc, argv, ptree);

  const auto configfilename = ptree.get<std::string>("output.config") + ".csv";

  // initialize and refine the grid according to setup
  const auto gh = Utility::GridHelper<typename RandomField::GridTraits>(ptree);
  auto yaspgrid = Dune::YaspGrid<dim>(gh.L(), gh.N(), gh.B(), 0);

  const auto numrefinements = ptree.get<int>("grid.numrefinements");
  for (int r{0}; r < numrefinements; ++r)
    yaspgrid.globalRefine(1);


  // --------------------------------------------------------------------------
  //                            UQ COMPUTATIONS
  // --------------------------------------------------------------------------

  // use a non-deterministic RNG to generate the seeds for the realizations
  auto seedgenerator = std::random_device();

  // the rate estimation parameters
  using GV0 = decltype(yaspgrid.levelGridView(0));
  const auto idhw = ptree.get<int>("mc.qoidomainhalfwidth");
  const auto qoidomain  = qoi_domain<GV0>(idhw, gh.N()[0], gh.L());
  const auto samplesize = ptree.get<std::size_t>("mc.samplesize");
  const auto numlevels  = numrefinements + 1;

  // Modes of the distribution of the QoI we are interested in
  auto qoimeans = std::vector<R>{};
  auto qoivars  = std::vector<R>{};

  // initialize random field
  auto rf = RandomField(ptree);

  for (int lvl{0}; lvl < numlevels; ++lvl)
  {
    std::cout << "\nProcessor " << processorid << " sampling on level "
              << lvl << std::endl;

    // initialize the sample and its mean on this level (need both for
    // the computation of the standard deviation at the end)
    auto lvlsamplemean = R(0.0);
    auto lvlsample = std::vector<R>{};

    // define the grid view on which the UQ problem is to be solved
    const auto gv = yaspgrid.levelGridView(lvl);

    // obtain the realizations
    for (std::size_t n{0}; n < samplesize; ++n)
    {
      rf.generate_realization(seedgenerator());

      auto uqproblem =
        UQProblem<decltype(gv)>(gv, qoidomain, rf.get_realization());
      uqproblem.solve();

      const auto qoirealization = uqproblem.quantity_of_interest();
      lvlsample.push_back(qoirealization);
      lvlsamplemean += qoirealization;
    }

    lvlsamplemean /= static_cast<R>(samplesize);

    qoimeans.push_back(lvlsamplemean);
    qoivars.push_back(unbiased_sample_variance(lvlsample, lvlsamplemean));

    std::cout << "Processor " << processorid << " done on level " << lvl
              << std::endl;

    // if we are not on the finest level, refine for the next estimate
    if (lvl != yaspgrid.maxLevel())
      rf.refine();
  }

  // --------------------------------------------------------------------------

  std::cout << "\nWriting results from processor " << processorid <<
               "  to file.\n" << std::endl;

  // concatenate the rank of the process to the filename
  const auto meandatasignature = ptree.get<std::string>("output.meandata");
  const auto meandatafn =
    meandatasignature + "_r" + std::to_string(processorid) + ".csv";

  const auto vardatasignature = ptree.get<std::string>("output.vardata");
  const auto vardatafn =
    vardatasignature + "_r" + std::to_string(processorid) + ".csv";

  // only let processor 0 write the configuration file
  if (processorid == 0)
  {
    auto configfile = std::ofstream(configfilename);
    assert(configfile);

    const auto variance = ptree.get<double>("stochastic.variance");
    const auto corrlength = ptree.get<double>("stochastic.corrLength");
    const auto covstr = ptree.get<std::string>("stochastic.covariance");
    assert(covstr == "matern22");

    configfile << "# numprocessors, initcellsperdim, numrefinements, "
               << "samplesize, qoidomainhalfwidth, variance, corrlength, "
               << "smoothness" << std::endl;
    configfile << mpihelper.size() << ", " << yaspgrid.levelSize(0)[0] << ", "
               << numrefinements << ", " << samplesize << ", " << idhw
               << ", " << variance << ", " << corrlength << ", " << 1.0
               << std::endl;

    configfile << meandatafn << ", " << vardatafn << std::endl;

    for (int l{0}; l < numlevels; ++l)
    {
      configfile << ((l == 0) ? "" : ", ")
                 << yaspgrid.domainSize()[0] / yaspgrid.levelSize(l)[0];
    }
    configfile << std::endl;

    configfile.close();
  }


  auto meandatafile = std::ofstream(meandatafn);
  auto vardatafile = std::ofstream(vardatafn);

  assert(meandatafile);
  assert(vardatafile);

  constexpr int doubleprec = 15;
  for (int l{0}; l < numlevels; ++l)
  {
    meandatafile << std::setprecision(doubleprec) << std::scientific;
    vardatafile << std::setprecision(doubleprec) << std::scientific;

    if (l == 0)
    {
      meandatafile << qoimeans.at(l);
      vardatafile << qoivars.at(l);
    }
    else if (l == numlevels - 1)
    {
      meandatafile << ", " << qoimeans.at(l) << std::endl;
      vardatafile << ", " << qoivars.at(l) << std::endl;
    }
    else
    {
      meandatafile << ", " << qoimeans.at(l);
      vardatafile << ", " << qoivars.at(l);
    }
  }

  meandatafile.close();
  vardatafile.close();

  return 0;
}

