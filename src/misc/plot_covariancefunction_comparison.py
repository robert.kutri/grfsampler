import numpy as np
import matplotlib.pyplot as plt

# read csv files
covfcndata = np.loadtxt('covfcn.csv', delimiter=', ', skiprows=1)
materncovdata = np.loadtxt('materncov.csv', delimiter=', ', skiprows=1)

# exctract relevant quantities (omit r = 0 for now )
r = np.squeeze(covfcndata[:, 0])
covfcn = np.squeeze(covfcndata[:, 1])
covstddev = np.squeeze(covfcndata[:, 2])
materncov = np.squeeze(materncovdata[:, 1])

# plot covariance function and matern covariance function against distance
fig = plt.figure()

covfcnplt = fig.add_subplot(1, 1, 1)
covfcnplt.set_title("Covariance Functions")
covfcnplt.scatter(r, covfcn, label="RF covariance function")
covfcnplt.errorbar(r, covfcn, yerr=covstddev, alpha=0.2)
covfcnplt.plot(r, materncov, label="Matern covariance")
covfcnplt.grid(linestyle="--", linewidth=0.5, color='.25')
covfcnplt.set_xlabel("distance r")
covfcnplt.set_ylabel("covariance function c(x,y) = c(r), r = |x - y|")

covfcnplt.legend()

plt.show()

