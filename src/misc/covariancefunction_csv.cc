#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#undef HAVE_MPI

#include <cmath>
#include <map>
#include <vector>
#include <iterator>
#include <algorithm>
#include <fstream>

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/traits/globaltraits.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/assembly/triangularmatrix.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>
#include <dune/grfsampler/statistics/covariancefunction.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>
#include <dune/grfsampler/utility/test.hh>
#include <dune/grfsampler/utility/fuzzycompare.hh>

using GT = GRFS::GlobalTraits<GRFS::GridPosition::DEFAULT>;

// FIXME: Switch to exclusively using GlobalTraits instead of GlobalTypes


using GlobalTypes::RealNumber;

using ValueArray = std::vector<RealNumber>;
using DiscreteFunction = std::vector<RealNumber>;
using IndexSet = std::vector<std::size_t>;
using CellCenterArray = std::vector<GlobalTypes::DomainVector>;


template <class Map>
ValueArray sample_standard_deviation(const Map& covmapping,
                                     const DiscreteFunction& meanarray)
{
  auto stddevarray = ValueArray{};

  for (const auto& idxcovpair : covmapping)
  {
    auto samples = idxcovpair.second;
    auto mean = meanarray[idxcovpair.first];
    auto variance = RealNumber{ 0.0 };
    auto ssizem1 = static_cast<RealNumber>(samples.size());

    for (const RealNumber& covsample : samples)
    {
      if (!Utility::is_equal(covsample, mean))
        variance += (covsample - mean) * (covsample - mean) / ssizem1;
    }

    stddevarray.push_back(std::sqrt(variance));
  }

  return stddevarray;
}


ValueArray dof_to_vector(GlobalTypes::FlatDofVector&& dofvec)
{
  auto vec = ValueArray{};
  std::move(dofvec.begin(), dofvec.end(), std::back_inserter(vec));

  return vec;
}


// shamelessly copied from https://stackoverflow.com/questions/17074324/how-can
// -i-sort-two-vectors-in-the-same-way-with-criteria-that-uses-only-one-of
template <typename T, typename Compare>
std::vector<std::size_t> sort_permutation(
  const std::vector<T>& vec,
  const Compare& compare)
{
  std::vector<std::size_t> p(vec.size());
  std::iota(p.begin(), p.end(), 0);
  std::sort(p.begin(), p.end(),
            [&](std::size_t i, std::size_t j)
            {
              return compare(vec[i], vec[j]);
            });

  return p;
}


template <typename T>
std::vector<T> apply_permutation(
  const std::vector<T>& vec,
  const std::vector<std::size_t>& p)
{
  std::vector<T> sorted_vec(vec.size());
  std::transform(p.begin(), p.end(), sorted_vec.begin(),
                 [&](std::size_t i){ return vec[i]; });

  return sorted_vec;
}


int main(int argc, char** argv)
{
  const auto numcells = 250;
  const auto upperright = GlobalTypes::DomainVector{ 1.0, 1.0 };
  const auto cells = GlobalTypes::CellLayout{ numcells, numcells };

  auto grid = std::make_unique<GlobalTypes::Grid>(upperright, cells);
  auto gv = grid->levelGridView(grid->maxLevel());
  using GridView = decltype(gv);

  auto rfparam = Statistics::MaternParameters<double>{ 7.0, 0.1, 1.0 };
  auto randomfield = GRFS::PQ1GaussianRandomField<GridView, GT>(gv, rfparam);

  // sample random field values at the cell centers and omit the layer
  auto numsamples = 300;
  auto ccsamples = Statistics::Experiment<ValueArray >{};

  std::cout << "\nStart sampling " << numsamples << " samples." << std::endl;

  for (int n{0}; n < numsamples; ++n)
  {
    auto seed = static_cast<decltype(randomfield)::Seed>(n);
    auto sample = randomfield.generate_sample(seed);
    auto samplegridfcn =
      Dune::Functions::makeDiscreteGlobalBasisFunction<RealNumber>(
        randomfield.get_basis(), sample);

    ccsamples.push_back(
      dof_to_vector(Utility::compute_cell_center_values(samplegridfcn)));
  }

  std::cout << "\nDone sampling. Computing covariance function." << std::endl;

  // determine, which of the cell center values are inside the boundary layer
  auto omitlayerwidth = 4.0 * rfparam.corrlength;
  auto layeridx = Statistics::determine_layer_indices(gv, upperright,
                                                      omitlayerwidth);

  std::cout << "omitting layer of width " << omitlayerwidth << std::endl;

  // remove the cells, whose centers are in the boundary layer
  for (auto& sample : ccsamples)
    Statistics::remove_boundary_layer(sample, layeridx);

  std::cout << "computing distance matrix" << std::endl;

  // compute the distances of the cell centers in the grid view, omitting cells
  // within a certain layer along the boundary
  auto distmat = Statistics::compute_distance_matrix(gv, layeridx);

  std::cout << "computing sample covariance" << std::endl;

  // The correct sample mean is already asserted in the
  // unit_randomfield_moments.cc test. In order not to bias the result for
  // smaller sample sizes, I thus use the population mean, which is zero
  auto popmean = ValueArray(ccsamples.front().size(), 0.0);
  auto covmat = Statistics::compute_sample_covariance(ccsamples, popmean);

  std::cout << "computing stationary, isotropic covariance function"
            << std::endl;

  // array storing the unique occurring distance values
  auto rval = ValueArray{};

  // {int:vector<RealNumber>} map storing the covariance values for the
  // distance value at the corresponding index in rvals
  auto covmapping = std::map<int, ValueArray >{};

  // Classify the field values according to their distance
  Statistics::map_distance_to_covariance(covmapping, rval, covmat, distmat);

  // Compute the mean covariance as a function of r := || x - y ||_2
  auto covfcn = Statistics::compute_stationary_isotropic_covariance_function(
    covmapping);

  // this computation makes use of covmapping and thus needs to be performed
  // before sorting the arrays
  auto covstddev = sample_standard_deviation(covmapping, covfcn);

  std::cout << "\nComputation done, postprocessing and writing files" <<
  std::endl;

  // sort rval and covfcn corresponding to ascending distance values
  auto permutation = sort_permutation(rval,
                                      [](const RealNumber& r1,
                                         const RealNumber& r2)
                                      {
                                        return r1 < r2;
                                      });

  rval = apply_permutation(rval, permutation);
  covfcn = apply_permutation(covfcn, permutation);

  // compute the true matern covariance values for given rval
  auto materncov = Statistics::matern_covariance(rval, rfparam);

  // save the covariance function in csv format
  auto covfcnfile = std::ofstream{ "covfcn.csv" };

  assert(covfcnfile);
  covfcnfile << "distance, covariance, stddev of covariance estimate" << '\n';
  for (std::size_t r{0}; r < rval.size(); ++r)
    covfcnfile << rval[r] << ", " << covfcn[r] << ", " << covstddev[r] << '\n';
  covfcnfile << '\n';
  covfcnfile.close();

  auto materncovfile = std::ofstream{ "materncov.csv" };

  assert(materncovfile);
  materncovfile << "# distance, covariance" << '\n';
  for (std::size_t r{0}; r < rval.size(); ++r)
    materncovfile << rval[r] << ", " << materncov[r] << '\n';
  materncovfile << '\n';
  materncovfile.close();

  return 0;
}

