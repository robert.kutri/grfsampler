#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <random>
#include <string>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/exceptions.hh>

#include <dune/randomfield/randomfield.hh>

#include <dune/grfsampler/traits/globaltraits.hh>
#include <dune/grfsampler/traits/adaptertraits.hh>
#include <dune/grfsampler/utility/rateestimation.hh>
#include <dune/grfsampler/adapter/darcyproblem.hh>

using GT = GRFS::GlobalTraits<GRFS::GridPosition::DEFAULT>;

using RFT = Adapter::CETraits<GT>;

template <class GridView, class RandomField>
using UQProblem = Adapter::DarcyUQProblem<GridView, RandomField, GT>;


constexpr char inifilename[] = "biasrate.ini";


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  std::cout << "\n\nReading Setup from " << inifilename << std::endl;

  // set up the ini file parser
  auto initreeparser = Dune::ParameterTreeParser{};

  // read the setup options
  auto ptree = Dune::ParameterTree{};
  initreeparser.readINITree(inifilename , ptree);
  initreeparser.readOptions(argc, argv, ptree);

  // extract and store the options for the rate estimation
  const auto setup = Utility::RateEstimationSetup<GT>(ptree);

  std::cout << "Using " << setup.number_of_refinements()
            << " refinements of a " << setup.initial_cell_layout()[0] << "x"
            << setup.initial_cell_layout()[1] << " mesh and "
            << setup.number_of_samples() << " samples per level" << std::endl;

  std::cout << "Reading random field configuration from "
            << setup.random_field_ini_filename() << std::endl;

  // read the configuration file for the random field based on the filename
  // provided in the parameter tree
  auto rfconfig = Dune::ParameterTree{};
  initreeparser.readINITree(setup.random_field_ini_filename(), rfconfig);
  initreeparser.readOptions(argc, argv, rfconfig);

  // write configuration file for data analysis
  // First row contains the paramters of the rate estimation and the second row
  // contains the number of levels, followed by the corresponding filenames of
  // the files containing the realizations
  auto configfs = std::ofstream(setup.output_config_filename());
  assert(configfs);
  setup.write_csv_parameters(rfconfig, configfs);

  auto datafs = std::ofstream(setup.output_data_filename());


  // --------------------------------------------------------------------------
  //                          SIMULATIONS
  // --------------------------------------------------------------------------

  std::cout << "Setting up random field" << std::endl;

  // construct a random field sampler using circulant embedding
  auto randomfield = Dune::RandomField::RandomField<RFT>(rfconfig);
  using RandomField = decltype(randomfield);

  // use a non-deterministic PRNG to generate the seeds for the samples
  auto seedgenerator = std::random_device();

  std::cout << "Generating the grid and performing refinements" << std::endl;

  // allocate the grid
  auto grid =
    std::make_unique<typename GT::Grid>(setup.domain_extent(),
                                        setup.initial_cell_layout());

  // generate one grid level per refinement and store the mesh widths
  for (int l{0}; l < setup.number_of_refinements(); ++l)
  {
    configfs << ((l == 0) ? "" : ", ") << setup.mesh_width(grid->levelSize(l));

    constexpr int once{ 1 };
    grid->globalRefine(once);
  }

  configfs.close();

  std::cout << "\n- Performing computations -\n" << std::endl;

  // Compute the estimates of E(|Q_l-Q|) for each level l<L, assuming Q=Q_L.
  // As the grid views are lightweight, they are defined anew for each sample
  const auto N = setup.number_of_samples();
  for (int n{0}; n < N; ++n)
  {
    const auto numlevels = setup.number_of_refinements() + 1;

    // output progress to console
    std::cout << "\rLevel: " << numlevels << "/" << numlevels
              << " --- Sample: " << (n + 1) << "/" << N << std::flush;

    // use computations on the finest level as reference
    const auto refgv = grid->levelGridView(grid->maxLevel());
    using ReferenceGridView = decltype(refgv);

    // sample a random field
    randomfield.generate(seedgenerator());

    // use the randomfield as the logarithm of permeability in a Darcy problem
    auto refuqproblem =
      UQProblem<ReferenceGridView, RandomField>(
        refgv, setup.qoi_domain_corners(), randomfield);

    // solve the PDE
    refuqproblem.solve();

    // compute the quantity of interest and store is as the reference value
    const auto refqoi = refuqproblem.quantity_of_interest();

    // compute the quantity of interest estimates on all coarser levels
    const auto maxlvlidx = numlevels - 1;
    for (int lvlidx{0}; lvlidx < maxlvlidx; ++lvlidx)
    {
      // output progress to console
      std::cout << "\rLevel: " << (lvlidx + 1) << "/" << numlevels
                << " --- Sample: " << (n + 1) << "/" << N << std::flush;

      const auto gv = grid->levelGridView(lvlidx);

      auto uqproblem =
        UQProblem<decltype(gv), RandomField>(
          gv, setup.qoi_domain_corners(), randomfield);

      uqproblem.solve();

      // Compute the value of the realization of |Q_l - Q_L|
      const auto qoidiff = std::abs(uqproblem.quantity_of_interest() - refqoi);

      // write the realization to the data file
      if (lvlidx == 0)
        datafs << qoidiff;
      else if (lvlidx == maxlvlidx - 1)
        datafs << ", " << qoidiff << std::endl;
      else
        datafs << ", " << qoidiff;
    }
  }

  std::cout << std::endl << "DONE\n\n" << std::endl;

  return 0;
}

