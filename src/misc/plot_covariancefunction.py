import numpy as np
import matplotlib.pyplot as plt

# read file
filename = "covfcn4.csv"
data = np.loadtxt(filename, delimiter=', ', skiprows=1)

r = np.squeeze(data[:, 0])
covfcn = np.squeeze(data[:, 1])

fig = plt.figure()

cplt = fig.add_subplot(1, 1, 1)
cplt.set_title("Covariance Function for " + filename)
cplt.plot(r, covfcn)
cplt.scatter(r, covfcn, label="covariance function")
cplt.grid(linestyle="--", linewidth=0.5, color='.25')
cplt.set_xlabel("distance r")
cplt.set_ylabel("covariance function c(x, y) = c(r), r := |x - y|")
cplt.legend()

plt.show()

