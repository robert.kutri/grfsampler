import sys
import csv
import math

import numpy as np
import matplotlib.pyplot as plt

from scipy.stats import linregress


if len(sys.argv) == 1:
    sys.exit("Missing filename for configuration file")
if len(sys.argv) > 2:
    sys.exit("Too many arguments")

configfilename = sys.argv[1]

# use csv reader to read file line by line
configfile = open(configfilename, mode='r', newline='')

# omit comment line
next(csv.reader(configfile))

# read the config parameters
config = next(csv.reader(configfile))

# extract parameter values
numlevels  = int(config[0])
numsamples = int(config[1])
variance   = float(config[2])
corrlength = float(config[3])
smoothness = float(config[4])

# read the filename for the sample realizations
datafilename = next(csv.reader(configfile))[0].strip()

# read the mesh widths for the levels
meshwidth = [float(h) for h in next(csv.reader(configfile))]
biasestimate = []

data = np.loadtxt(datafilename, delimiter=', ')

# compute the MC estimate of E(Q_l - Q) on each level
for lvl in range(numlevels - 1):
    biasestimate += [np.mean(data[:, lvl])]

# estimate the rate
rate = linregress(np.log10(meshwidth), np.log10(biasestimate))

print("convergence rate estimate: ", rate[0])


# -----------------------------------------------------------------------------
#                                   PLOT
# -----------------------------------------------------------------------------

fig = plt.figure()

rp = fig.add_subplot(1, 1, 1)

# create the canvas
titlestr = str(numsamples) + " samples per estimate, variance: " \
        + str(variance) + ", corrlength: " + str(corrlength) + ", nu: " \
        + str(smoothness)
rp.set_title(titlestr)
rp.grid(linestyle="--", linewidth=0.5, color=".25")
rp.set_xlabel("mesh width h")
rp.set_ylabel("E(|Q - Q_h|)")

# add the data
rp.loglog(meshwidth, biasestimate, color='k')
rp.scatter(meshwidth, biasestimate, label='E(|Q - Q_h|)', color='k',
           marker='o')

# add powers of h for reference
hpow1 = meshwidth
hpow2 = np.power(meshwidth, 2.0)

rp.loglog(meshwidth, hpow1, label="h^1", linestyle="-", alpha=0.5)
rp.loglog(meshwidth, hpow2, label="h^2", linestyle="-", alpha=0.5)

boxstr = "rate estimate ~ " + '{:.2f}'.format(rate[0])
boxprops = dict(boxstyle='round', alpha=0.2)
rp.text(0.6, 0.1, boxstr, transform=rp.transAxes, fontsize=14,
        verticalalignment='bottom', bbox=boxprops)
rp.legend()

plt.show()

