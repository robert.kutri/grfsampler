#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <random>
#include <fstream>
#include <iomanip>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/exceptions.hh>

#include <dune/grfsampler/utility/rateestimation.hh>
#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/adapter/darcyproblem.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/randomfield/circulantembeddingrandomfield.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>

using Statistics::unbiased_sample_variance;

using SGrid = GRFS::Adapter::SamplingGrid;

using OSGridView  = typename SGrid::OversamplingLevelGridView;
using TGridView   = typename SGrid::TargetLevelGridView;

using RandomField    = GRFS::PQ1GaussianRandomField<OSGridView>;
using RefRandomField = GRFS::CirculantEmbeddingRandomField;

using R             = typename RandomField::RealNumber;
using RFRealization = typename RandomField::Realization;

using UQProblem    = GRFS::Adapter::DarcyUQProblem<RandomField, TGridView>;
using RefUQProblem = GRFS::Adapter::DarcyUQProblem<RefRandomField, TGridView>;


constexpr char inifilename[] = "biasrate_spde.ini";


int main(int argc, char** argv)
{
  // initialize MPI helper in case of parallel execution
  const auto& mpihelper = Dune::MPIHelper::instance(argc, argv);
  const auto processorid = mpihelper.rank();

  // with the way the QoI integration domain is currently set up, only the
  // layout resulting from either 1, 2 or 4 processors yields correct results
  assert(mpihelper.size() == 1 or
         mpihelper.size() == 2 or
         mpihelper.size() == 4);

  if (processorid == 0)
  {
    std::cout << "\n\nStart sampling on " << mpihelper.size()
              << " processor(s)" << std::endl;
  }

  std::cout << "\nReading Setup from ini file" << std::endl;

  auto iniparser = Dune::ParameterTreeParser{};

  // read the main .ini file
  auto ptree = Dune::ParameterTree{};
  iniparser.readINITree(inifilename, ptree);
  iniparser.readOptions(argc, argv, ptree);

  const auto mcsetup = Utility::RateEstimationSetup<R>(ptree);

  // extract the parameters of the matern field
  const auto maternparam =
    Utility::parse_matern_parameters<R>(ptree, "RANDOMFIELD");

  // The rectangular domain, on which the realizations are to be computed
  const auto targetdomain = mcsetup.target_domain();

  // parse cell layout on the coarsest level of the target grid
  const auto initlayout = mcsetup.initial_cell_layout();

  // parse minimal layer width for the oversampling domain
  const auto mlw = ptree.get<R>("GRID.layerfactor") * maternparam.corrlength;

  std::cout << "Setting up the grid on processor " << processorid
            << "  and refining it" << std::endl;

  // define the target grid embedded into an oversampling grid
  auto sgrid = SGrid(targetdomain, initlayout, mlw);

  // refine the grid according to the rate estimation setup
  for (int r{0}; r < mcsetup.number_of_refinements(); ++r)
    sgrid.global_refine(1);


  // --------------------------------------------------------------------------
  //                            UQ COMPUTATIONS
  // --------------------------------------------------------------------------

  // use a non-deterministic RNG to generate the seeds for the realizations
  auto seedgenerator = std::random_device();

  // the rate estimation parameters
  const auto samplesize = mcsetup.number_of_samples();
  const auto qoidomain  = mcsetup.qoi_domain();
  const auto numlevels  = mcsetup.number_of_refinements() + 1;

  // Modes of the distribution of the QoI we are interested in
  auto qoimeans = std::vector<R>{};
  auto qoivars  = std::vector<R>{};

  for (int lvl{0}; lvl < numlevels; ++lvl)
  {
    std::cout << "\nProcessor " << processorid << " sampling on level "
              << lvl << std::endl;

    const auto osgv = sgrid.oversampling_level_grid_view(lvl);
    auto rf = RandomField(sgrid, osgv, maternparam);

    // initialize the sample and its mean on this level (need both for
    // the computation of the standard deviation at the end)
    auto lvlsamplemean = R(0.0);
    auto lvlsample = std::vector<R>{};

    // define the grid view on which the UQ problem is to be solved
    const auto tgv = sgrid.target_level_grid_view(lvl);

    // obtain the realizations
    for (std::size_t n{0}; n < samplesize; ++n)
    {
      rf.generate_realization(seedgenerator());

      auto uqproblem = UQProblem(tgv, qoidomain, rf.get_realization());
      uqproblem.solve();

      const auto qoirealization = uqproblem.quantity_of_interest();
      lvlsample.push_back(qoirealization);
      lvlsamplemean += qoirealization;
    }

    lvlsamplemean /= static_cast<R>(samplesize);

    qoimeans.push_back(lvlsamplemean);
    qoivars.push_back(unbiased_sample_variance(lvlsample, lvlsamplemean));

    std::cout << "Processor " << processorid << " done on level " << lvl
              << std::endl;

    // if we are on the finest level, check whether to compute a reference
    // estimate using Circulant Embedding
    if (lvl == numlevels - 1 and mcsetup.use_ce_reference())
    {
      const auto refonrefined = ptree.get<bool>("MC.refineforreference");

      std::cout << "\nComputing reference estimate using Circulant Embedding ";
      std::cout << ((refonrefined) ? "on refinement of the finest grid."
                                   : "on the finest grid.")
                << std::endl;

      /* TODO:
       * It is tedious and suboptimal to first write an ini file and then
       * immediately read it. Try setting the parameters in a
       * Dune::ParameterTree directly
       */

      // set the necessary parameters for the configuration
      const auto extensionval = (maternparam.corrlength < 0.15) ? 1 : 2;
      const auto extensions = std::vector<int>{ extensionval, extensionval };
      const auto cells = sgrid.target_level_size(lvl);

      auto covstr = std::string{};
      if (maternparam.smoothness == 1.0 and RandomField::dimension == 2)
        covstr = "matern22";
      else
        DUNE_THROW(Dune::NotImplemented, "Higher smoothness or dimension does "
                   "not yet work properly");

      const auto ceininame = "biasrate_CE_tmp.ini";
      auto ceini = std::ofstream(ceininame);

      ceini << "[grid]\n";
      ceini << "extensions = "
            << extensions[0] << " " << extensions[1] << "\n";
      ceini << "cells = " << cells[0] << " " << cells[1] << "\n";
      ceini << "[stochastic]\n";
      ceini << "covariance = " << covstr << "\n";
      ceini << "variance = " << maternparam.variance << "\n";
      ceini << "corrLength = " << maternparam.corrlength << std::endl;

      ceini.close();

      // read the ini file we just wrote
      auto ceconfig = typename RefRandomField::Configuration{};
      iniparser.readINITree(ceininame, ceconfig);
      iniparser.readOptions(argc, argv, ceconfig);

      // initialize circulant embedding random field
      auto refrf = RefRandomField(ceconfig);

      // refine the circulant embedding grid once, if necessary
      if (refonrefined)
        refrf.refine();

      auto reflvlsamplemean = R(0.0);
      auto reflvlsample = std::vector<R>{};

      const auto rtgv = sgrid.target_level_grid_view(lvl);

      // obtain the realizations
      for (std::size_t n{0}; n < samplesize; ++n)
      {
        refrf.generate_realization(seedgenerator());

        auto refuqproblem =
          RefUQProblem(rtgv, qoidomain, refrf.get_realization());
        refuqproblem.solve();

        const auto qoirealization = refuqproblem.quantity_of_interest();
        reflvlsample.push_back(qoirealization);
        reflvlsamplemean += qoirealization;
      }

      reflvlsamplemean /= static_cast<R>(samplesize);

      qoimeans.push_back(reflvlsamplemean);
      qoivars.push_back(
        unbiased_sample_variance(reflvlsample, reflvlsamplemean));
    }
  }

  // --------------------------------------------------------------------------

  std::cout << "\nWriting results from processor " << processorid <<
               "  to file.\n" << std::endl;

  // only let processor 0 write the configuration file
  if (processorid == 0)
  {
    auto configfile = std::ofstream(mcsetup.output_config_filename());
    assert(configfile);
    mcsetup.write_csv_parameters(ptree, mpihelper, configfile);

    for (int l{0}; l < numlevels; ++l)
    {
      configfile << ((l == 0) ? "" : ", ")
                 << mcsetup.mesh_width(sgrid.target_level_size(l));
    }
    configfile << std::endl;

    configfile.close();
  }

  // concatenate the rank of the process to the filename
  const auto meandatafn =
    mcsetup.mean_data_signature() + "_r" + std::to_string(processorid)
      + ".csv";
  const auto vardatafn =
    mcsetup.variance_data_signature() + "_r" + std::to_string(processorid)
      + ".csv";
  auto meandatafile = std::ofstream(meandatafn);
  auto vardatafile = std::ofstream(vardatafn);
  assert(meandatafile);
  assert(vardatafile);

  constexpr int doubleprec = 15;
  for (int l{0}; l < numlevels; ++l)
  {
    meandatafile << std::setprecision(doubleprec) << std::scientific;
    vardatafile << std::setprecision(doubleprec) << std::scientific;

    if (l == 0)
    {
      meandatafile << qoimeans.at(l);
      vardatafile << qoivars.at(l);
    }
    else if (l == numlevels - 1)
    {
      if (mcsetup.use_ce_reference())
      {
        meandatafile << ", " << qoimeans.at(l) << ", " << qoimeans.at(l+1)
                     << std::endl;
        vardatafile << ", " << qoivars.at(l) << ", " << qoivars.at(l+1)
                    << std::endl;
      }
      else
      {
        meandatafile << ", " << qoimeans.at(l) << std::endl;
        vardatafile << ", " << qoivars.at(l) << std::endl;
      }
    }
    else
    {
      meandatafile << ", " << qoimeans.at(l);
      vardatafile << ", " << qoivars.at(l);
    }
  }

  meandatafile.close();
  vardatafile.close();

  return 0;
}

