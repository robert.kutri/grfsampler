#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <iterator>
#include <memory>
#include <random>
#include <cassert>
#include <cmath>
#include <fstream>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grfsampler/statistics/materncovariance.hh>
#include <dune/grfsampler/statistics/covariancefunction.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>
#include <dune/grfsampler/randomfield/circulantembeddingrandomfield.hh>
#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/utility/gridhelper.hh>
#include <dune/grfsampler/utility/test.hh>

using RandomField = GRFS::CirculantEmbeddingRandomField;

using R = typename RandomField::RealNumber;
using DomainVector = typename RandomField::GridTraits::Domain;


inline double numcells_to_mesh_width(const double& domainextent, const int& n)
{
  return domainextent / static_cast<double>(n);
}


constexpr char inifilename[] = "covariancefunction_ce.ini";

static constexpr unsigned int dim = 2;


int main(int argc, char** argv)
{
  // initialize fake MPI
  const auto& mpihelper = Dune::MPIHelper::instance(argc, argv);

  std::cout << "\n\nReading setup from " << inifilename << "\n\n" << std::endl;

  auto iniparser = Dune::ParameterTreeParser{};

  // read the main .ini file
  auto ptree = Dune::ParameterTree{};
  iniparser.readINITree(inifilename, ptree);
  iniparser.readOptions(argc, argv, ptree);

  // initialize and refine the grid according to setup
  const auto gh = Utility::GridHelper<typename RandomField::GridTraits>(ptree);
  auto yaspgrid = Dune::YaspGrid<dim>(gh.L(), gh.N(), gh.B(), 0);

  // refine the grid according to ini file specification
  const auto numrefinements = ptree.get<int>("grid.numrefinements");
  for (int r{0}; r < numrefinements; ++r)
    yaspgrid.globalRefine(1);

  // use a non-deterministic RNG to generate the seeds for the realizations
  auto seedgenerator = std::random_device();

  // estimate parameters
  const auto samplesize = ptree.get<std::size_t>("setup.samplesize");
  const auto binsize = ptree.get<R>("setup.binsize");
  const auto numlevels  = numrefinements + 1;

  // set up configuration file storing all relevant parameter and file names
  const auto configfilename = ptree.get<std::string>("output.config") + ".csv";
  auto configfile = std::ofstream(configfilename);
  assert(configfile);

  constexpr double dummyentry = 0.0;
  const auto variance = ptree.get<double>("stochastic.variance");
  const auto corrlength = ptree.get<double>("stochastic.corrLength");
  const auto covstr = ptree.get<std::string>("stochastic.covariance");
  assert(covstr == "matern22");

  configfile << "# numprocessors, initcellsperdim, numrefinements, "
	     << "samplesize, qoidomainhalfwidth, variance, corrlength, "
	     << "smoothness" << std::endl;
  configfile << mpihelper.size() << ", " << yaspgrid.levelSize(0)[0] << ", "
	     << numrefinements << ", " << samplesize << ", " << dummyentry
	     << ", " << variance << ", " << corrlength << ", " << 1.0
	     << std::endl;

  // write the data filenames into the configuration file
  constexpr char datafilesignature[] = "covfcn_lvl";
  for (int l{0}; l < numlevels; ++l)
  {
    configfile << datafilesignature << l << ".csv";
    if (l < numlevels - 1)
      configfile << ", ";
    else
      configfile << std::endl;
  }

  // write the mesh sizes into the configuration file (assuming same mesh size
  // in both directions)
  for (int l{0}; l < numlevels; ++l)
  {
    configfile << numcells_to_mesh_width(
                    yaspgrid.domainSize()[0], yaspgrid.levelSize(l)[0]);
    if (l < numlevels - 1)
      configfile << ", ";
    else
      configfile << std::endl;
  }

  configfile.close();

  // initialize random field
  auto rf = RandomField(ptree);

  // compute the covariance function for each level
  for (int lvl{0}; lvl < numlevels; ++lvl)
  {
    // sample the value of the random field at the cell centers
    auto ccsample = std::vector<std::vector<R> >{};

    // define the relevant grid view
    const auto gv = yaspgrid.levelGridView(lvl);
    using GridView = decltype(gv);

    std::cout << "Sampling " << samplesize << " realizations" << std::endl;

    for (std::size_t n{0}; n < samplesize; ++n)
    {
      ccsample.push_back(std::vector<R>{});
      auto& ccvals = ccsample.back();

      // generate a new realization and extract it from the random field
      rf.generate_realization(seedgenerator());
      const auto& realization = rf.get_realization();

      // probe the realization at the cell centers and store
      for (const auto& e : elements(gv))
        ccvals.push_back(realization.evaluate(e, e.geometry().center()));
    }

    std::cout << "Computing covariance matrix" << std::endl;

    // compute the covariance matrix for the sample
    const auto covmat = Statistics::sample_covariance_zero_mean(ccsample);

    std::cout << "Computing stationary, isotropic covariance function"
              << std::endl;

    // compute the distances of the cell centers from one another
    const auto distmat =
      Statistics::distance_matrix<GridView, DomainVector>(gv);

    // array storing the unique occurring distance values
    auto rval = std::vector<double>{};

    // {size_t:vector<double>} map storing the covariance values for the
    // distance value at the corresponding index in rvals
    auto covmapping = std::map<std::size_t, decltype(rval)>{};

    // Classify the field values according to their distance
    Statistics::map_distance_to_covariance(covmapping, rval, covmat, distmat,
                                           binsize);

    // Compute the mean covariance as a function of r := || x - y ||_2
    auto covfcn = Statistics::compute_stationary_isotropic_covariance_function(
	covmapping);

    std::cout << "Writing results to file" << std::endl;

    auto covfcnfile =
      std::ofstream{ datafilesignature + std::to_string(lvl) + ".csv" };
    assert(covfcnfile);

    for (std::size_t r{0}; r < rval.size(); ++r)
      covfcnfile << rval[r] << ", " << covfcn[r] << '\n';
    covfcnfile << std::endl;

    covfcnfile.close();

    std::cout << "DONE\n" << std::endl;

    // if we are not on the finest level, refine for the next computation
    if (lvl != yaspgrid.maxLevel())
      rf.refine();
  }

  return 0;
}

