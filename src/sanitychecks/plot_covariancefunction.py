import sys
import csv
import math

import numpy as np
import matplotlib.pyplot as plt

from scipy.special import gamma, kv
from scipy.stats import linregress


# CAREFUL! kappa = sqrt(2 * nu), not sqrt(8 * nu)
def matern_covariance(distance, sigmasq, rho, nu):

    if (abs(distance) < 1e-12):
        return sigmasq

    else:
        kappa = math.sqrt(2.0 * nu) / rho
        kr = kappa * distance
        modbessel = kv(nu, kr)
        factor = math.pow(2.0, 1.0 - nu) * sigmasq / gamma(nu)

        return factor * math.pow(kr, nu) * modbessel


# read the configuration file
if len(sys.argv) == 1:
    sys.exit("Missing configuration file")
if len(sys.argv) > 2:
    sys.exit("Too many arguments")

print("TODO: Why is the meshsize of the coarsest level used in the plot?"
      " Does it have to do with the order of the plot command called ?")

configfilename = sys.argv[1]

# use csv.reader to read the configuration file
configfile = open(configfilename, mode='r', newline='')

# omit comment line
next(csv.reader(configfile))

# read parameters
paramline = next(csv.reader(configfile))

numprocessors   = int(paramline[0])
initcellsperdim = int(paramline[1])
numrefinements  = int(paramline[2])
samplesize      = int(paramline[3])
# paramline[4] not needed here
variance        = float(paramline[5])
corrlength      = float(paramline[6])
smoothness      = float(paramline[7])
numlevels       = numrefinements + 1

# read the filenames of the files containing the actual data
datafilenames = next(csv.reader(configfile))

# read the mesh widths
meshwidth = next(csv.reader(configfile))
meshwidth = [float(meshwidth[i]) for i in range(len(meshwidth))]

configfile.close()

# prepare the canvas
fig = plt.figure()

# define the color map for the covariance function plot
numcolors = len(datafilenames)
colormap  = plt.get_cmap('OrRd')

covplt = fig.add_subplot(1, 2, 1)
rteplt = fig.add_subplot(1, 2, 2)

covplt.set_title("Covariance Functions")
covplt.grid(linestyle="--", linewidth=0.5, color='.25')
covplt.set_xlabel("distance r")
covplt.set_ylabel("covariance function c(x, y) = c(r), r := |x - y|")


rteplt.set_title("Error in Maximum Norm against Mesh Width")
rteplt.grid(linestyle="--", linewidth=0.5, color='.25')
rteplt.set_xlabel("mesh width h")
rteplt.set_ylabel("error e = max |c(r) - c_h(r)|")

fig.suptitle("variance: " + str(variance)
             + "  -  corrlength: " + str(corrlength)
             + "  -  nu: " + str(smoothness)
             + "  -  " + str(samplesize) +
             " realizations per estimate")

inftyerror = []
l2error = []

l = 0
for filename in reversed(datafilenames):

    # read data
    data = np.loadtxt(filename.strip(), delimiter=', ')
    dist = np.squeeze(data[:, 0])
    covfcn = np.squeeze(data[:, 1])

    # compute the matern field values at the distance values
    materncov = [matern_covariance(r, variance, corrlength, smoothness)
                    for r in dist]

    # determine the errors
    error = np.abs(materncov - covfcn)
    inftyerror += [np.amax(error)]
    l2error += [math.sqrt(np.trapz(y=error * error, x=dist))]

    # add dataset to plot of covariance functions
    if (l == 0):
        covplt.plot(dist, materncov, label="Matern covariance function",
                    linewidth=2.5, color='k')

    covplt.plot(dist, covfcn, label="h = " + "{:.2e}".format(meshwidth[l]),
                color=colormap((numcolors-l) / numcolors), alpha=0.6)

    l += 1

covplt.legend()

rteplt.loglog(meshwidth, inftyerror, color='k')
rteplt.scatter(meshwidth, inftyerror, label="infty error", color='k',
               marker='o')
rteplt.loglog(meshwidth, l2error, color='k')
rteplt.scatter(meshwidth, l2error, label='L2 error', color='k', marker='^')

# add powers of h as reference (scaling factors are for aesthetics)
hpowonehalf = np.power(meshwidth, 0.5)
hpowone = np.multiply(meshwidth, 3.0)
hpowthreehalves = 10.0 * np.power(meshwidth, 1.5)
hpowtwo = 20.0 * np.power(meshwidth, 2.0)

rteplt.loglog(meshwidth, hpowonehalf, label="h^{1/2}", linestyle="-",
              alpha=0.5)
rteplt.loglog(meshwidth, hpowone, label="h^{1}", linestyle="-", alpha=0.5)
rteplt.loglog(meshwidth, hpowthreehalves, label="h^{3/2}", linestyle="-",
              alpha=0.5)
rteplt.loglog(meshwidth, hpowtwo, label="h^{2}", linestyle="-", alpha=0.5)

inftyrate = linregress(np.log10(meshwidth), np.log10(inftyerror))[0]
l2rate = linregress(np.log10(meshwidth), np.log10(l2error))[0]

rateestimatesstr = "intfy rate ~ " + '{:.2f}'.format(inftyrate) + \
                   "\nL2 rate ~ " + '{:.2f}'.format(l2rate)

props = dict(boxstyle='round', alpha=0.2)
rteplt.text(0.6, 0.1, rateestimatesstr, transform=rteplt.transAxes,
            fontsize=14, verticalalignment='bottom', bbox=props)

rteplt.legend()

plt.show()

