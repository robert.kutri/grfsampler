#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <string>
#include <cassert>
#include <memory>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/pdelab/function/callableadapter.hh>

#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>


using SGrid = GRFS::Adapter::SamplingGrid;

using CellLayout = typename SGrid::CellLayout;
using OSGridView = typename SGrid::OversamplingLeafGridView;
using TGridView  = typename SGrid::TargetLeafGridView;
using Domain     = typename SGrid::RectangularDomain;

using RandomField = GRFS::PQ1GaussianRandomField<OSGridView>;

using R             = typename RandomField::RealNumber;
using RFRealization = typename RandomField::Realization;
using RFCoefficient = typename RFRealization::DofVector;

using Sample        = std::vector<RFCoefficient>;


constexpr char inifilename[] = "marginalvariance.ini";


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  std::cout << "Generating the setup" << std::endl;

  // define an ini file parser
  auto ptree = Dune::ParameterTree{};
  auto ptreeparser = Dune::ParameterTreeParser{};

  // parse ini file
  ptreeparser.readINITree(inifilename, ptree);
  ptreeparser.readOptions(argc, argv, ptree);

  // parse Matern parameters
  const auto param = Utility::parse_matern_parameters<R>(ptree, "RANDOMFIELD");

  // parse target domain
  const auto urx = ptree.get<R>("GRID.upperrightx", 0.0);
  const auto ury = ptree.get<R>("GRID.upperrighty", 0.0);
  const auto tdom = Domain{ { {0., 0.}, {urx, ury} } };

  // parse cell layout in target grid
  const auto nx = ptree.get<int>("GRID.ncellsx", 0);
  const auto ny = ptree.get<int>("GRID.ncellsy", 0);
  const auto layout = CellLayout{ nx, ny };

  // parse minimal layer width for the oversampling domain
  const auto mlw = ptree.get<R>("GRID.layerfactor") * param.corrlength;

  // define target grid embedded into an oversampling grid
  auto sgrid = SGrid(tdom, layout, mlw);

  std::cout << "Initializing random field" << std::endl;

  // set up the random field sampler
  const auto osgv = sgrid.oversampling_leaf_grid_view();
  auto rf = RandomField(sgrid, osgv, param);

  const auto numsamples = ptree.get<int>("MC.numsamples");
  assert(numsamples > 0);

  std::cout << "Start sampling " << numsamples << " realizations" << std::endl;

  // sample values of random field realizations. As we are using a PQ1 basis,
  // the values of the degrees of freedom directly correspond to the values
  // of the realization at the grid vertices
  auto sample = Sample{};
  const auto tenpercentnumsamples = int(0.1 * numsamples);
  auto percentcounter = 0;
  for (int n{0}; n < numsamples; ++n)
  {
    if (n % tenpercentnumsamples == 0)
    {
      std::cout << percentcounter << "%" << " done" << std::endl;
      percentcounter += 10;
    }

    // use loop counter as seed for a new random field realization
    const auto seed = static_cast<decltype(rf)::Seed>(n);
    rf.generate_realization(seed);

    // copy the degrees of freedom vector into the sample
    sample.push_back(rf.get_realization().degrees_of_freedom());
  }

  std::cout << "Computing marginal variance" << std::endl;

  // store the marginal variance as a random field sample, in order to be
  // able to use the same interface
  auto margvar =
    std::make_shared<const RFCoefficient>(
        Statistics::sample_variance_zero_mean(sample));
  auto wrappedmargvar = RFRealization(sgrid, rf.share_basis(), margvar);

  std::cout << "Writing results to file" << std::endl;

  const auto tgv = sgrid.target_leaf_grid_view();
  auto vtkwriter = Dune::VTKWriter<TGridView>(tgv, Dune::VTK::conforming);

  // make a grid function out of the marginal variance, in order to pass it
  // to the VTK writer
  auto gfcn = Dune::PDELab::makeGridFunctionFromCallable(tgv,
                [&wrappedmargvar](const auto& element, const auto& xlocal)
                {
                  return wrappedmargvar.evaluate(element, xlocal);
                });

  using GFAdapter = Dune::PDELab::VTKGridFunctionAdapter<decltype(gfcn)>;
  auto gfcnadapter = std::make_shared<GFAdapter>(gfcn, "marginal variance");
  vtkwriter.addVertexData(gfcnadapter);

  const auto filename = ptree.get<std::string>("OUTPUT.filename");
  vtkwriter.write(filename, Dune::VTK::appendedraw);

  std::cout << "File written.\n" << std::endl;

  return 0;
}

