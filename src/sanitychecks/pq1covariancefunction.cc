#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <iterator>
#include <memory>
#include <random>
#include <cassert>
#include <cmath>
#include <fstream>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>
#include <dune/grfsampler/statistics/covariancefunction.hh>
#include <dune/grfsampler/statistics/momentestimators.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/utility/rateestimation.hh>
#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/utility/test.hh>


using SGrid = GRFS::Adapter::SamplingGrid;

using OSGridView   = typename SGrid::OversamplingLevelGridView;
using TGridView    = typename SGrid::TargetLevelGridView;
using DomainVector = typename SGrid::DomainVector;
using Domain       = typename SGrid::RectangularDomain;
using CellLayout   = typename SGrid::CellLayout;

using RandomField = GRFS::PQ1GaussianRandomField<OSGridView>;

using R = typename RandomField::RealNumber;


inline double numcells_to_mesh_width(const double& domainextent, const int& n)
{
  return domainextent / static_cast<double>(n);
}


constexpr char inifilename[] = "covariancefunction.ini";


int main(int argc, char** argv)
{
  // initialize fake MPI
  const auto& mpihelper = Dune::MPIHelper::instance(argc, argv);

  std::cout << "\n\nReading setup from " << inifilename << "\n\n" << std::endl;

  auto iniparser = Dune::ParameterTreeParser{};

  // read the main .ini file
  auto ptree = Dune::ParameterTree{};
  iniparser.readINITree(inifilename, ptree);
  iniparser.readOptions(argc, argv, ptree);

  const auto setup = Utility::RateEstimationSetup<R>(ptree);

  // extract Matern Field parameters
  const auto maternparam =
    Utility::parse_matern_parameters<R>(ptree, "RANDOMFIELD");

  // grid parameters
  const auto targetdomain = setup.target_domain();
  const auto initlayout   = setup.initial_cell_layout();
  const auto minlyrwidth  = ptree.get<R>("GRID.layerfactor")
                              * maternparam.corrlength;

  // initialize sampling grid
  auto sgrid = SGrid(targetdomain, initlayout, minlyrwidth);

  // refine the grid according to ini file specification
  for (int r{0}; r < setup.number_of_refinements(); ++r)
    sgrid.global_refine(1);

  // use a non-deterministic RNG to generate the seeds for the realizations
  auto seedgenerator = std::random_device();

  // estimate parameters
  const auto samplesize = setup.number_of_samples();
  const auto numlevels  = setup.number_of_refinements() + 1;
  const auto binsize = ptree.get<R>("COVARIANCEFUNCTION.binsize");

  // set up configuration file storing all relevant parameter and file names
  auto configfile = std::ofstream(setup.output_config_filename());
  assert(configfile);
  setup.write_csv_parameters(ptree, mpihelper, configfile, false);

  // write the data filenames into the configuration file
  constexpr char datafilesignature[] = "covfcn_lvl";
  for (int l{0}; l < numlevels; ++l)
  {
    configfile << datafilesignature << l << ".csv";
    if (l < numlevels - 1)
      configfile << ", ";
    else
      configfile << std::endl;
  }

  // write the mesh sizes into the configuration file (assuming same mesh size
  // in both directions)
  for (int l{0}; l < numlevels; ++l)
  {
    configfile << numcells_to_mesh_width(
                    targetdomain[1][0], sgrid.oversampling_level_size(l)[0]);
    if (l < numlevels - 1)
      configfile << ", ";
    else
      configfile << std::endl;
  }

  configfile.close();

  // compute the covariance function for each level
  for (int lvl{0}; lvl < numlevels; ++lvl)
  {
    std::cout << "Setting up sampler on level " << lvl << std::endl;
    const auto ogv = sgrid.oversampling_level_grid_view(lvl);
    const auto tgv = sgrid.target_level_grid_view(lvl);

    // initialize random field on this level
    auto rf = RandomField(sgrid, ogv, maternparam);

    // sample the value of the random field at the cell centers
    auto ccsample = std::vector<std::vector<R> >{};

    std::cout << "Sampling " << samplesize << " realizations" << std::endl;

    for (std::size_t n{0}; n < samplesize; ++n)
    {
      ccsample.push_back(std::vector<R>{});
      auto& ccvals = ccsample.back();

      // generate a new realization and extract it from the random field
      rf.generate_realization(seedgenerator());
      const auto& realization = rf.get_realization();

      // probe the realization at the cell centers and store
      for (const auto& e : elements(tgv))
        ccvals.push_back(realization.evaluate(e, e.geometry().center()));
    }

    std::cout << "Computing covariance matrix" << std::endl;

    // compute the covariance matrix for the sample
    const auto covmat = Statistics::sample_covariance_zero_mean(ccsample);

    std::cout << "Computing stationary, isotropic covariance function"
              << std::endl;

    // compute the distances of the cell centers from one another
    const auto distmat =
      Statistics::distance_matrix<TGridView, DomainVector>(tgv);

    // array storing the unique occurring distance values
    auto rval = std::vector<double>{};

    // {size_t:vector<double>} map storing the covariance values for the
    // distance value at the corresponding index in rvals
    auto covmapping = std::map<std::size_t, decltype(rval)>{};

    // Classify the field values according to their distance
    Statistics::map_distance_to_covariance(covmapping, rval, covmat, distmat,
                                           binsize);

    // Compute the mean covariance as a function of r := || x - y ||_2
    auto covfcn = Statistics::compute_stationary_isotropic_covariance_function(
	covmapping);

    std::cout << "Writing results to file" << std::endl;

    auto covfcnfile =
      std::ofstream{ datafilesignature + std::to_string(lvl) + ".csv" };
    assert(covfcnfile);

    for (std::size_t r{0}; r < rval.size(); ++r)
      covfcnfile << rval[r] << ", " << covfcn[r] << '\n';
    covfcnfile << std::endl;

    covfcnfile.close();

    std::cout << "DONE\n" << std::endl;
  }

  return 0;
}

