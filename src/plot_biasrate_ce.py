import sys
import csv
import math

import numpy as np
import matplotlib.pyplot as plt

from scipy.stats import linregress


# the file containing the data should be passed as first argument
if len(sys.argv) == 1:
    sys.exit("Missing config files")
if len(sys.argv) > 2:
    sys.exit("Too many arguments")

configfilename = sys.argv[1]

# use csv reader to read the configuration file
configfile = open(configfilename, mode='r', newline='')

# omit comment line
next(csv.reader(configfile))

# read the parameters contained in the mean data file
paramline = next(csv.reader(configfile))

"""
the samplesize has to be the same for each sample considered
"""

numprocessors   = int(paramline[0])
initcellsperdim = int(paramline[1])
numrefinements  = int(paramline[2])
samplesize      = int(paramline[3])
qoidomhalfwidth = int(paramline[4])
variance        = float(paramline[5])
corrlength      = float(paramline[6])
smoothness      = float(paramline[7])
numlevels       = numrefinements + 1

datafiles = next(csv.reader(configfile))

# sample means and sample variances of the QoI sample on each level
meandata = np.loadtxt(datafiles[0].strip(), delimiter=', ')
vardata = np.loadtxt(datafiles[1].strip(), delimiter=', ')

meandata = np.reshape(meandata, ((-1, numlevels)))
vardata = np.reshape(vardata, ((-1, numlevels)))
assert meandata.shape == vardata.shape

# transform the sample variances to the variances of the mc estimators
# according to the central limit theorem
vardata = vardata / samplesize

# read the mesh widths for the levels
meshwidth = next(csv.reader(configfile))
meshwidth = np.array([float(h) for h in meshwidth])
meshwidth = meshwidth[:-1]

# take the finest level as reference and compute the bias estimates. This also
# transposes the remaining meandata array
biasmc = np.array([meandata[:, j] - meandata[:, -1]
                      for j in range(numlevels - 1)])
biasmc = np.abs(np.mean(biasmc, axis=1))

# transform the estimator variances accordingly
numsamples = meandata.shape[0]

vardata = [vardata[:,j] + vardata[:, -1] for j in range(numlevels - 1)]
biassd = [math.sqrt(np.sum(vardata[l])) / numsamples
              for l in range(numlevels - 1)]

# estimate the rate
rate = linregress(np.log10(meshwidth), np.log10(biasmc))


# -----------------------------------------------------------------------------
#                                   PLOT
# -----------------------------------------------------------------------------

fig = plt.figure()

rp = fig.add_subplot(1, 1, 1)

# create the canvas
numrlzstr = '{:,}'.format(numsamples * samplesize).replace(',', ' ')
titlestr = numrlzstr + " realizations per estimate, " \
           + "variance: " + str(variance) + ", corrlength: " \
           + str(corrlength) + ", nu: " + str(smoothness)
rp.set_title(titlestr)
rp.grid(linestyle="--", linewidth=0.5, color=".25")
rp.set_xlabel("mesh width h")
rp.set_ylabel("E[Q_l] - E[Q]")

# add the data
rp.loglog(meshwidth, biasmc, color='k')
rp.scatter(meshwidth, biasmc, label='E[Q_l] - E[Q]', color='k', marker='o')

# add errorbars
rp.errorbar(meshwidth, biasmc, yerr=biassd, capsize=4)

# add powers of h for reference
hpow20 = 2e-1 * np.power(meshwidth, 2.0)
hpow12 = 5e-4 * np.power(meshwidth, 0.5)

rp.loglog(meshwidth, hpow20, label="h^2", linestyle="--", alpha=0.5)
rp.loglog(meshwidth, hpow12, label="h^{1/2}", linestyle="--", alpha=0.5)

# add text box with rate estimate
boxstr = "rate estimate ~ " + '{:.2f}'.format(rate[0])
boxprops = dict(boxstyle='round', alpha=0.2)
rp.text(0.6, 0.1, boxstr, transform=rp.transAxes, fontsize=14,
        verticalalignment='bottom', bbox=boxprops)
rp.legend()

plt.show()

