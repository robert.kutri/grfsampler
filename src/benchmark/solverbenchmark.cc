#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>

#include <dune/common/timer.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/istl/ldl.hh>
#include <dune/istl/spqr.hh>
#include <dune/istl/superlu.hh>
#include <dune/istl/umfpack.hh>

#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/statistics/materncovariance.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>


using SGrid = GRFS::Adapter::SamplingGrid;

using Domain     = typename SGrid::RectangularDomain;
using CellLayout = typename SGrid::CellLayout;
using OSGridView = typename SGrid::OversamplingLevelGridView;

template <template<class> class Solver>
using RandomField = GRFS::PQ1GaussianRandomField<OSGridView, Solver>;

using R = double;
using Seed = unsigned int;


constexpr char inifilename[] = "solverbenchmark.ini";


int main(int argc, char** argv)
{
  // Instantiate fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // read ini file
  auto iniparser = Dune::ParameterTreeParser{};
  auto ptree = Dune::ParameterTree{};
  iniparser.readINITree(inifilename, ptree);
  iniparser.readOptions(argc, argv, ptree);
  
  // extract parameters
  const auto maternparam   = Statistics::MaternParameters<R>{ 1.0, 0.1, 1.0 };
  const auto numsolves     = ptree.get<int>("BENCHMARK.numsolves", 0);
  const auto numrefs       = ptree.get<int>("GRID.numrefinements", 0); 
  const auto initcellspd   = ptree.get<int>("GRID.initcellsperdim", 0);
  const auto targetdomain  = Domain{ { {0., 0.}, {1., 1.} } };
  const auto initlayout    = CellLayout{ initcellspd, initcellspd };
  const auto minlayerwidth = 1.0 * maternparam.corrlength;
  const auto numlevels     = numrefs + 1;

  // define sampling grid
  auto sgrid = SGrid(targetdomain, initlayout, minlayerwidth);

  for (int r{0}; r < numrefs; ++r)
    sgrid.global_refine(1);

  auto ildltiming    = std::vector<R>{};
  auto spqrtiming    = std::vector<R>{};
  auto superlutiming = std::vector<R>{};
  auto umfpacktiming = std::vector<R>{};

  for (int lvl{0}; lvl < numlevels; ++lvl)
  {
    const auto osgv = sgrid.oversampling_level_grid_view(lvl);
    const auto levelsize = sgrid.oversampling_level_size(lvl);


    std::cout << "\n---- Start ILDL benchmark ----" << std::endl;
    std::cout << "\nAssemble random field on " << levelsize[0] << "x" 
              << levelsize[1] << " grid" << std::endl;

    auto ildlrf = RandomField<Dune::LDL>(sgrid, osgv, maternparam);

    std::cout << "Start solves" << std::endl;

    auto watch = Dune::Timer{};

    for (int n{0}; n < numsolves; ++n)
      ildlrf.generate_realization(static_cast<Seed>(n));

    auto ildltime = watch.elapsed();

    std::cout << "\n Took: " << ildltime << "s, Average time per solve: "
              << ildltime / static_cast<R>(numsolves) << std::endl;

    ildltiming.push_back(ildltime / static_cast<R>(numsolves));

    std::cout << "\n\n---- Start SPQR benchmark ----" << std::endl;
    std::cout << "\nAssemble random field on " << levelsize[0] << "x" 
              << levelsize[1] << " grid" << std::endl;

    auto spqrrf = RandomField<Dune::SPQR>(sgrid, osgv, maternparam);

    std::cout << "Start solves" << std::endl;

    watch.reset(); 

    for (int n{0}; n < numsolves; ++n)
      spqrrf.generate_realization(static_cast<Seed>(n));

    auto spqrtime = watch.elapsed();

    std::cout << "\n Took: " << spqrtime << "s, Average time per solve: "
              << spqrtime / static_cast<R>(numsolves) << std::endl;

    spqrtiming.push_back(spqrtime / static_cast<R>(numsolves));

    std::cout << "\n\n---- Start SuperLU benchmark ----" << std::endl;
    std::cout << "\nAssemble random field on " << levelsize[0] << "x" 
              << levelsize[1] << " grid" << std::endl;

    auto superlurf = RandomField<Dune::SuperLU>(sgrid, osgv, maternparam);

    std::cout << "Start solves" << std::endl;

    watch.reset(); 

    for (int n{0}; n < numsolves; ++n)
      superlurf.generate_realization(static_cast<Seed>(n));

    auto superlutime = watch.elapsed();

    std::cout << "\n Took: " << superlutime << "s, Average time per solve: "
              << superlutime / static_cast<R>(numsolves) << std::endl;

    superlutiming.push_back(superlutime / static_cast<R>(numsolves));

    std::cout << "\n\n---- Start UMFPack benchmark ----" << std::endl;
    std::cout << "\nAssemble random field on " << levelsize[0] << "x" 
              << levelsize[1] << " grid" << std::endl;

    auto umfpackrf = RandomField<Dune::UMFPack>(sgrid, osgv, maternparam);

    std::cout << "Start solves" << std::endl;

    watch.reset(); 

    for (int n{0}; n < numsolves; ++n)
      umfpackrf.generate_realization(static_cast<Seed>(n));

    auto umfpacktime = watch.elapsed();

    std::cout << "\n Took: " << umfpacktime << "s, Average time per solve: "
              << umfpacktime / static_cast<R>(numsolves) << std::endl;

    umfpacktiming.push_back(umfpacktime / static_cast<R>(numsolves));
  }

  auto benchmarkfile = std::ofstream("solverbenchmark.csv");
  assert(benchmarkfile);

  for (int l{0}; l < numlevels; ++l)
  {
    benchmarkfile << std::pow(2.0, double(l)) * double(initcellspd) << ", "
                  << ildltiming.at(l) << ", " 
                  << spqrtiming.at(l) << ", "
                  << superlutiming.at(l) << ", "
                  << umfpacktiming.at(l) << std::endl;
  }

  return 0;
}

