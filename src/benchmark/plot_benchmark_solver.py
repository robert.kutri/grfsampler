import numpy as np
import matplotlib.pyplot as plt

benchmark = np.loadtxt("solverbenchmark.csv", delimiter=", ")

cellsperdim  = benchmark[:, 0]
ildltimes    = benchmark[:, 1]
spqrtimes    = benchmark[:, 2]
superlutimes = benchmark[:, 3]
umfpacktimes = benchmark[:, 4]

fig = plt.figure()

bmrk = fig.add_subplot(1, 1, 1)

bmrk.set_title("Average Time per Solve")
bmrk.grid(linestyle="--", linewidth=0.5, color=".25")
bmrk.set_xlabel("cells per dimension (2D)")
bmrk.set_ylabel("average time per solve")

bmrk.loglog(cellsperdim, ildltimes)
bmrk.loglog(cellsperdim, spqrtimes)
bmrk.loglog(cellsperdim, superlutimes)
bmrk.loglog(cellsperdim, umfpacktimes)

bmrk.scatter(cellsperdim, ildltimes, label="ILDL")
bmrk.scatter(cellsperdim, spqrtimes, label="SPQR")
bmrk.scatter(cellsperdim, superlutimes, label="SuperLU")
bmrk.scatter(cellsperdim, umfpacktimes, label="UMFPack")

bmrk.legend()

plt.show()

