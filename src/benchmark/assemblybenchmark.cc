#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#undef HAVE_MPI

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cassert>

#include <dune/common/timer.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/assembly/quadrature.hh>


int main (int argc, char** argv)
{
  const auto testkappa = 0.2;
  const auto numruns = 8;
  const auto numcellsfactor = 2;
  const auto numcellsinitial = 50;
  const auto upperright = GlobalTypes::DomainVector{ 1.0, 1.0 };

  auto tmpnumcells = numcellsinitial;
  auto testnumcells = std::vector<int>{ tmpnumcells };
  for (int n{1}; n < numruns; ++n)
  {
    tmpnumcells *= numcellsfactor;
    testnumcells.push_back(tmpnumcells);
  }

  auto numdofs = std::vector<int>{};
  auto cputime = std::vector<double>{};

  std::cout << "\nTiming " << numruns << " assemblies.\n\n" << std::endl;

  auto setupwatch = Dune::Timer{};
  auto assemblywatch = Dune::Timer{};

  for (const auto& numcells : testnumcells)
  {
    setupwatch.reset();

    std::cout << "Setting up " << numcells << " x " << numcells << " grid."
              << std::endl;

    const auto celllayout = GlobalTypes::CellLayout{ numcells, numcells };
    auto grid = std::make_unique<GlobalTypes::Grid>(upperright, celllayout);
    auto gv = grid->leafGridView();
    auto pq1basis = GlobalTypes::PQ1Basis<decltype(gv)>(gv);

    std::cout << "Setup took " << setupwatch.elapsed() << " seconds."
              << std::endl;

    std::cout << "Assembling matrices for " << pq1basis.size()
              << " degrees of freedom." << std::endl;

    assemblywatch.reset();

    auto assembly =
      GRFS::Assembly::assemble_for_pq1basis<decltype(gv)>(pq1basis, testkappa);

    auto assemblytime = assemblywatch.elapsed();

    std::cout << "Assembly took " << assemblytime << " seconds.\n\n"
              << std::endl;

    numdofs.push_back(pq1basis.size());
    cputime.push_back(assemblytime);
  }

  auto filename = std::stringstream{};
  filename << "bnchmrk_"
           << testkappa << "kap_"
           << numruns << "rns_"
           << numcellsfactor << "ncf_"
           << numcellsinitial << "nci.csv";

  auto benchmrkfile = std::ofstream{ filename.str() };
  assert(benchmrkfile);

  benchmrkfile << "# number of degrees of freedom, CPU time" << '\n';
  for (std::size_t nd{0}; nd < numdofs.size(); ++nd)
    benchmrkfile << std::fixed << numdofs[nd] << ", " << cputime[nd] << '\n';
  benchmrkfile << '\n';
  benchmrkfile.close();

  return 0;
}

