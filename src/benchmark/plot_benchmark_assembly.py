#!/usr/bin/python3

# usage: python3 plot_benchmark_assembly.py <benchmark_filename>

import sys
import numpy as np
import matplotlib.pyplot as plt

# read csv file
filename = str(sys.argv[1])
benchmarkdata = np.loadtxt(filename, delimiter=', ', skiprows=1);

# extract relevant data
numdofs = np.squeeze(benchmarkdata[:, 0])
cputime = np.squeeze(benchmarkdata[:, 1])

# plot 
fig = plt.figure()

bmplt = fig.add_subplot(1, 1, 1)

bmplt.set_title("Assembly Time as a Function of System Size")
bmplt.scatter(numdofs, cputime, label="benchmark samples")
bmplt.loglog(numdofs, cputime)
bmplt.set_xlabel("number of degrees of freedom")
bmplt.set_ylabel("time for assembly")



plt.show()

