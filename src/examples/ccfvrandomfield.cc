#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/pdelab/function/callableadapter.hh>
#include <dune/pdelab/common/vtkexport.hh>

#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/randomfield/ccfvrandomfield.hh>

using SGrid = GRFS::Adapter::SamplingGrid;

using Domain     = typename SGrid::RectangularDomain;
using CellLayout = typename SGrid::CellLayout;
using OSGridView = typename SGrid::OversamplingLeafGridView;
using TGridView  = typename SGrid::TargetLeafGridView;

using RandomField = GRFS::CCFVGaussianRandomField<OSGridView>;

using R             = typename RandomField::RealNumber;
using RFRealization = typename RandomField::Realization;


constexpr char inifilename[] = "ccfvrandomfield.ini";


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  std::cout << "Generating the setup" << std::endl;

  // define an ini file parser
  auto ptree = Dune::ParameterTree{};
  auto ptreeparser = Dune::ParameterTreeParser{};

  // parse ini file
  ptreeparser.readINITree(inifilename, ptree);
  ptreeparser.readOptions(argc, argv, ptree);

  // parse Matern parameters
  const auto param = Utility::parse_matern_parameters<R>(ptree, "RANDOMFIELD");

  // parse target domain
  const auto urx = ptree.get<R>("GRID.upperrightx", 0.0);
  const auto ury = ptree.get<R>("GRID.upperrighty", 0.0);
  const auto tdom = Domain{ { {0., 0.}, {urx, ury} } };

  // parse cell layout in target grid
  const auto nx = ptree.get<int>("GRID.ncellsx", 0);
  const auto ny = ptree.get<int>("GRID.ncellsy", 0);
  const auto layout = CellLayout{ nx, ny };

  // parse minimal layer width for the oversampling domain
  const auto mlw = ptree.get<R>("GRID.layerfactor") * param.corrlength;

  // define target grid embedded into an oversampling grid
  auto sgrid = SGrid(tdom, layout, mlw);

  std::cout << "Initializing random field" << std::endl;

  // set up the random field sampler
  const auto osgv = sgrid.oversampling_leaf_grid_view();
  auto randomfield = RandomField(sgrid, osgv, param);

  std::cout << "Sampling random field" << std::endl;

  // generate a sample
  auto seed = Utility::parse_seed_value("RANDOMFIELD", ptree);
  randomfield.generate_realization(seed);

  std::cout << "Writing output file" << std::endl;

  const auto tgv = sgrid.target_leaf_grid_view();
  auto vtkwriter = Dune::VTKWriter<TGridView>(tgv, Dune::VTK::conforming);

  const auto& realization = randomfield.get_realization();

  // PDELab machinery to convert the random field sample into a grid function
  auto gfcn = Dune::PDELab::makeGridFunctionFromCallable(tgv,
                [&realization](const auto& element, const auto& xlocal)
                {
                  return realization.evaluate(element, xlocal);
                }
              );

  using GFAdapter = Dune::PDELab::VTKGridFunctionAdapter<decltype(gfcn)>;
  auto gfcnadapter =
    std::make_shared<GFAdapter>(gfcn, "random_field_realization");
  vtkwriter.addCellData(gfcnadapter);

  const auto filename = ptree.get<std::string>("OUTPUT.filename");
  vtkwriter.write(filename, Dune::VTK::appendedraw);

  std::cout << "File written.\n" << std::endl;

  return 0;
}

