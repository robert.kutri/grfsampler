#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <memory>
#include <cmath>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/pdelab/function/callableadapter.hh>

#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/randomfield/pq1randomfield.hh>
#include <dune/grfsampler/adapter/oversamplingdomain.hh>
#include <dune/grfsampler/adapter/darcyproblem.hh>

using SGrid = GRFS::Adapter::SamplingGrid;

using Domain     = typename SGrid::RectangularDomain;
using CellLayout = typename SGrid::CellLayout;
using OSGridView = typename SGrid::OversamplingLeafGridView;
using TGridView  = typename SGrid::TargetLeafGridView;

using RandomField = GRFS::PQ1GaussianRandomField<OSGridView>;

using R             = typename RandomField::RealNumber;
using RFRealization = typename RandomField::Realization;

using DarcyProblem  = GRFS::Adapter::DarcyModelProblem<RandomField, TGridView>;


constexpr char inifilename[] = "darcymodelproblem.ini";


int main (int argc, char** argv)
{
  // initialize MPI
  Dune::MPIHelper::instance(argc, argv);

  std::cout << "Parsing the setup" << std::endl;

  // define an ini file parser
  auto ptree = Dune::ParameterTree{};
  auto ptreeparser = Dune::ParameterTreeParser{};

  // parse ini file
  ptreeparser.readINITree(inifilename, ptree);
  ptreeparser.readOptions(argc, argv, ptree);

  // parse Matern parameters
  const auto param = Utility::parse_matern_parameters<R>(ptree, "RANDOMFIELD");

  // parse target domain
  const auto urx = ptree.get<R>("GRID.upperrightx", 0.0);
  const auto ury = ptree.get<R>("GRID.upperrighty", 0.0);
  const auto tdom = Domain{ { {0., 0.}, {urx, ury} } };

  // parse cell layout in target grid
  const auto nx = ptree.get<int>("GRID.ncellsx", 0);
  const auto ny = ptree.get<int>("GRID.ncellsy", 0);
  const auto layout = CellLayout{ nx, ny };

  // parse minimal layer width for the oversampling domain
  const auto mlw = ptree.get<R>("GRID.layerfactor") * param.corrlength;

  const auto seed = Utility::parse_seed_value("RANDOMFIELD", ptree);

  const auto outputfilename = ptree.get<std::string>("OUTPUT.filename");

  std::cout << "Setting up grid and assembling sampler" << std::endl;

  // define the target grid embedded into an oversampling grid
  auto sgrid = SGrid(tdom, layout, mlw);

  // define the random field sampler
  const auto osgv = sgrid.oversampling_leaf_grid_view();
  auto randomfield = RandomField(sgrid, osgv, param);

  std::cout << "Sampling random field realization" << std::endl;

  randomfield.generate_realization(seed);

  std::cout << "Setting up Darcy problem" << std::endl;

  // set up the darcy problem
  const auto tgv = sgrid.target_leaf_grid_view();
  auto darcyproblem = DarcyProblem(tgv, randomfield.get_realization());

  std::cout << "Solving Darcy Problem" << std::endl;

  darcyproblem.solve();

  std::cout << "Writing output to file" << std::endl;

  auto vtkwriter = Dune::VTKWriter<TGridView>(tgv, Dune::VTK::conforming);

  // add random field realization to file
  const auto& rfrealization = randomfield.get_realization();
  auto rfgf = Dune::PDELab::makeGridFunctionFromCallable(tgv,
                [&rfrealization](const auto& element, const auto& xlocal)
                {
                  return rfrealization.evaluate(element, xlocal);
                }
              );

  using RFGFVTKF = Dune::PDELab::VTKGridFunctionAdapter<decltype(rfgf)>;
  auto rfvtk = std::make_shared<RFGFVTKF>(rfgf, "random field realization");
  vtkwriter.addVertexData(rfvtk);

  // add permeability to file
  auto modeldata =
    typename DarcyProblem::ModelData(randomfield.get_realization());
  auto pmgf = Dune::PDELab::makeGridFunctionFromCallable(tgv,
                [&modeldata](const auto& element, const auto& xlocal)
                {
                  return modeldata.A(element, xlocal)[0][0];
                }
              );

  using PMGFVTKF = Dune::PDELab::VTKGridFunctionAdapter<decltype(pmgf)>;
  auto pmvtk = std::make_shared<PMGFVTKF>(pmgf, "permeability");
  vtkwriter.addVertexData(pmvtk);

  // add solution to file
  const auto& solgvf = darcyproblem.get_solution();
  auto sollocalfunction = localFunction(solgvf);
  auto slgf = Dune::PDELab::makeGridFunctionFromCallable(tgv,
                [&sollocalfunction](const auto& element, const auto& xlocal)
                {
                  sollocalfunction.bind(element);
                  return sollocalfunction(xlocal);
                }
              );

  using SLGFVTKF = Dune::PDELab::VTKGridFunctionAdapter<decltype(slgf)>;
  auto slvtk = std::make_shared<SLGFVTKF>(slgf, "solution");
  vtkwriter.addVertexData(slvtk);

  vtkwriter.write(outputfilename, Dune::VTK::appendedraw);

  std::cout << "File written.\n" << std::endl;

  return 0;
}

