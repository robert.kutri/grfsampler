#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#undef HAVE_MPI

#include <memory>
#include <string>
#include <iostream>
#include <random>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/grfsampler/globaltypes.hh>
#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/assembly/quadrature.hh>
#include <dune/grfsampler/assembly/pq1whitenoise.hh>


//FIXME: switch to exclusively using GlobalTraits instead of GlobalTypes


using GlobalTypes::RealNumber;

template <class GridView>
using FEBasis = GlobalTypes::PQ1Basis<GridView>;


int main(int argc, char** argv)
{
  // define an ini file parser
  auto ptree = Dune::ParameterTree{};
  auto ptreeparser = Dune::ParameterTreeParser{};

  // parse ini file
  ptreeparser.readINITree("lagrangewhitenoise.ini", ptree);
  ptreeparser.readOptions(argc, argv, ptree);

  // define grid
  auto grid = Utility::set_up_grid_unique_ptr(ptree);
  auto gv = grid->levelGridView(grid->maxLevel());
  using GV = decltype(gv);

  // define a Finite Element basis
  auto febasis = std::make_shared<const FEBasis<GV> >(gv);

  // the kappa parameter must be passed to the assembly method, but is
  // irrelevant for the white noise itself
  auto kappa = RealNumber{ 1.0 };
  auto assembly = GRFS::Assembly::assemble_for_pq1basis(*febasis, kappa);
  auto whitenoise = GRFS::Assembly::PQ1WhiteNoise(
    febasis, assembly->release_factorization());

  // generate the sample
  auto seed = Utility::parse_seed_value("WHITENOISE", ptree);
  auto whitenoisesample = whitenoise.sample_white_noise(seed);

  std::cout << "Sampling done. Writing to file." << std::endl;

  auto samplegfcn =
    Dune::Functions::makeDiscreteGlobalBasisFunction<RealNumber>(
      *febasis, whitenoisesample);

  auto vtkwriter = Dune::SubsamplingVTKWriter<decltype(gv)>(
    gv, Dune::refinementLevels(0));

  auto filename = ptree.get<std::string>("OUTPUT.filename");
  auto fieldinfo = Dune::VTK::FieldInfo::Type::scalar;
  vtkwriter.addVertexData(samplegfcn,
                          Dune::VTK::FieldInfo(filename, fieldinfo, 1));
  vtkwriter.write(filename);

  std::cout << "File written.\n" << std::endl;

  return 0;
}

