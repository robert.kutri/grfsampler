#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/pdelab/function/callableadapter.hh>
#include <dune/pdelab/common/vtkexport.hh>

#include <dune/grfsampler/traits/gridtraits.hh>
#include <dune/grfsampler/utility/inifileparsing.hh>
#include <dune/grfsampler/assembly/fvwhitenoise.hh>


using R = double;

using GT =
  GRFS::Traits::GridTraits<GRFS::Traits::Switches::GridLowerLeft::DEFAULT>;

using GridView     = typename GT::Grid::LeafGridView;
using DomainVector = typename GT::DomainVector;

using FVWhiteNoise =
  GRFS::Assembly::FiniteVolumeWhiteNoise<GridView, DomainVector, R>;

using FVDofs = typename FVWhiteNoise::Realization::Dofs;


constexpr char inifilename[] = "fvwhitenoise.ini";


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  // define an ini file parser
  auto ptree = Dune::ParameterTree{};
  auto ptreeparser = Dune::ParameterTreeParser{};

  // parse ini file
  ptreeparser.readINITree(inifilename, ptree);
  ptreeparser.readOptions(argc, argv, ptree);

  // define grid
  auto grid = Utility::set_up_grid_unique_ptr(ptree);
  const auto gv = grid->leafGridView();

  // initialize white noise
  auto whitenoise = FVWhiteNoise(gv);

  // generate the realization
  auto seed = Utility::parse_seed_value("WHITENOISE", ptree);
  whitenoise.generate_realization(seed);
  const auto& rlz = whitenoise.get_realization();

  std::cout << "Sampling done. Writing to file." << std::endl;

  auto rlzgf = Dune::PDELab::makeGridFunctionFromCallable(gv,
                [&rlz](const auto& element, const auto& xlocal)
                {
                  return rlz.evaluate<decltype(element)>(element, xlocal);
                }
              );

  using VTKF = Dune::PDELab::VTKGridFunctionAdapter<decltype(rlzgf)>;
  auto rlzvtkf = std::make_shared<VTKF>(rlzgf, "FV white noise");


  auto filename = ptree.get<std::string>("OUTPUT.filename");

  auto vtkwriter = Dune::VTKWriter<GridView>(gv, Dune::VTK::conforming);
  vtkwriter.addCellData(rlzvtkf);
  vtkwriter.write(filename, Dune::VTK::appendedraw);

  std::cout << "File written.\n" << std::endl;

  return 0;
}

