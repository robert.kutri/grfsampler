#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dune/pdelab/function/callableadapter.hh>

#include <dune/grfsampler/randomfield/circulantembeddingrandomfield.hh>
#include <dune/grfsampler/utility/gridhelper.hh>
#include <dune/grfsampler/utility/inifileparsing.hh>


using RangeField = double;
using RandomField = GRFS::CirculantEmbeddingRandomField;

static constexpr unsigned int dim = 2;

constexpr char inifilename[] = "cerandomfield.ini";


int main(int argc, char** argv)
{
  // initialize fake MPI
  Dune::MPIHelper::instance(argc, argv);

  std::cout << "Generating the setup" << std::endl;

  // define the ini file parser
  auto ptree = Dune::ParameterTree{};
  auto ptreeparser = Dune::ParameterTreeParser{};

  // parse ini file
  ptreeparser.readINITree(inifilename, ptree);
  ptreeparser.readOptions(argc, argv, ptree);

  // parse the output filename
  const auto outputfilename = ptree.get<std::string>("output.filename");

  std::cout << "Initializing random field" << std::endl;

  // initialize the random field
  auto randomfield = RandomField(ptree);

  std::cout << "Generating the realization" << std::endl;

  // sample realization
  const auto seed = Utility::parse_seed_value("realization", ptree);
  randomfield.generate_realization(seed);

  std::cout << "Writing output" << std::endl;

  // initialize a YASPGrid and pass the realization on this grid to VTK writer
  const auto gh = Utility::GridHelper<typename RandomField::GridTraits>(ptree);
  const auto ygrid = Dune::YaspGrid<dim>(gh.L(), gh.N(), gh.B(), 0);
  randomfield.write_to_vtk(outputfilename, ygrid.leafGridView());

  return 0;
}

